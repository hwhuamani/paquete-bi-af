CREATE OR REPLACE Package  DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR  Is
  /************************************************************************************************
	NOMBRE:       DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR
    PROPOSITO:    paquete ETL para el BI de Activos Fijos
    REVISIONES:
	╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
	║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
	║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
	║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │   Paquete de carga de Tablones   ║
	║         │             │                    │              │   carga de dimenciones y         ║
	║         │             │                    │              │   tablas fact del datamart AF    ║
	╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝

  *************************************************************************************************/
	
	--declaracion de constantes:
	Kn_Ejec_Erro    Number(1) := 1;
	Kn_Ejec_Ok      Number(1) := 0;
	Kv_Ejec_Mens_Ok Varchar2(30) := 'EJECUTADO SATISFACTORIAMENTE';
	
	kv_compania_hermes  Varchar2(30) := 'ADMIN';
	kv_compania_smart  Varchar2(30) := 'SMART';
	kv_acepta_datos  Varchar2(30) := 'S';
	kv_activo Varchar2(30) := 'S';
	kv_uso_restringido  Varchar2(30) := 'N';
	kv_tipo_doc_fact  Varchar2(30) := 'FAC';
	kv_dep_fiscal  Varchar2(30) := 'F';
	kv_dep_corp  Varchar2(30) := 'C';
	kv_ref  Varchar2(30) := 'H';

	--declaracion de procedimientos:
	
	--############ sp carga tablones:
	PROCEDURE BIAF_CRGA_TBLN_CIA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TBLN_CTA_CTBL(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TBLN_TIP_CRTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TBLN_FAMI_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TBLN_SUCU(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TBLN_PROV(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TBLN_CTRO_CSTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_TIP_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TBLN_TIP_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_ACTI_FIJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_ACTI_MEJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_HIST_DEPR(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TRNS_HIST_REVA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TRNS_HIST_DEPR_REVA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_DEPR_CENT_CSTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TRNS_ACTI_CNTRO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TBLN_ACTI_MEJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_TRNS_USU_APROC(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_TBLN_FACT_ACTI(
				ld_periodo       In  Varchar2,
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	
	--JM: STG_ACTIVO_FIJO.DMPG_BIAF_ACTI_MEJO_RV Paquete para DM para el poblamiento de los tablones del DataMart
	
	Function DMFU_MONT_BAJA_ACFI( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                                pn_flag_mont In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.ind_transfer%Type 
                              ) 
                                Return Number;
                                                                                           
  ----- Activos con depreciaciones revaluadas
  Function DMFU_DEPR_REVA( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                           pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_mejora%Type,
                           pd_fec_depr In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.FECH_ACTIV%Type,
                           pv_tip_cont In STG_ACTIVO_FIJO.tbl_hist_depreciacion_reval.contabilidad%Type,
                           pv_tip_mone In DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.mone_fact%Type 
                         ) 
                           Return Number;
  ----- Revaluaciones
  Function DMFU_DEPR_REVA_BAJA( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.activo_fijo%Type,
                                pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.mejora%Type,
                                pd_fec_depr In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.fecha_dep_ini%Type,
                                pd_fec_reti In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.fecha_retiro%Type 
                              ) 
                                Return Number;

  Function DMFU_DEPR_TOTA_REVA_SOLF( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                                     pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_mejora%Type 
                                   ) 
                                     Return Number;
                                     
  ------ Poblar Tablón Revaluación (1)
  Procedure DMPR_POBL_TBLN_REVA( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
                               
  ------ Poblar Tablón del Mayor (2)
  Procedure DMPR_POBL_TBLN_MAYR( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
                               
  ------ Poblar Tablón Asiento (3)
  Procedure DMPR_POBL_TBLN_ASNT( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
                               
  ------ Poblar Tablón EEFF (4)
  Procedure DMPR_POBL_TBLN_EEFF( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
                               
  ------ Poblar Tablón EEFF Activos (5)
  Procedure DMPR_POBL_TBLN_EEFF_ACTI( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
	
	
	--############ SP carga dimenciones:
	PROCEDURE BIAF_CRGA_DIM_TIEMPO(
				pd_fech_ini      In  Varchar2,
				pd_fech_fin      In  Varchar2,
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_DIM_CIA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_DIM_CTA_CTBL(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_DIM_TIPO_CTRTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_DIM_FAMI_ACTIVO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_DIM_TIPO_ACTIVO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_DIM_PROVEEDOR(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_DIM_SUCU(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
				
	PROCEDURE BIAF_CRGA_DIM_CTRO_CSTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_DIM_ACTI_FIJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2);
	

	-----JM: Asientos Contable del Mayor
	Procedure DMPR_ASNTO_CNTBL( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type ); 
	
	--JM: DM_ACTIVO_FIJO.DMPG_BIAF_ACTI_REVA, Paquete para DM para el poblamiento de las tablas FAC del DataMart
	----- Dimension Compañia
  Function DMFU_ID_CIA( pv_ruc_cia In DM_ACTIVO_FIJO.biaf_cia.nro_ruc_cia%Type ) Return Number;    
  
  Function DMFU_TEXT_CIA( pv_ruc_cia In DM_ACTIVO_FIJO.biaf_cia.nro_ruc_cia%Type ) Return Varchar;
  
  ----- Dimension Asiento Contable  
  Function DMFU_ID_ASNTO( pv_asnto_cont In stg_activo_fijo.tbl_fact_estd_fina.asnto_cont%Type ) Return Number;
  
  ----- Dimension Centro de Costo  
  Function DMFU_ID_CENTRO_COSTO( pv_cntr_csto In DM_ACTIVO_FIJO.biaf_cntr_csto.cntr_csto%Type ) Return Number;
  
  ----- Dimension Activo Mejora  
  Function DMFU_ID_ACTI_MEJORA( pv_acti_fijo In DM_ACTIVO_FIJO.Biaf_Acti_Mejo.COD_ACTIVO%Type,
                                pv_acti_mejo In DM_ACTIVO_FIJO.Biaf_Acti_Mejo.COD_MEJORA%Type ) Return Number;
                                
  ----- Dimension Sucursal                                
  Function DMFU_ID_SUCURSAL( pv_codi_sucu In DM_ACTIVO_FIJO.biaf_sucu.COD_SUCU%Type ) Return Number;
  
  ----- Dimension Familia (Clasificacion del AF)  
  Function DMFU_ID_FAMI( pv_cod_fami In dm_activo_fijo.biaf_fami_acti.codi_fam%Type ) Return Number;  
  
  ----- Dimension Cuenta Contable  
  Function DMFU_ID_CTA_CTBLE( pv_cta_ctble In dm_activo_fijo.biaf_cnta_conta.cnta_cont%Type ) Return Number;  
	
	
	
	
	--######### SP Carga FACT
	PROCEDURE BIAF_CRGA_FACT_ACTIVO(
		ld_periodo       In  Varchar2,
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);
	
	PROCEDURE BIAF_CRGA_FACT_ACTIVO_HIST(
		ld_periodo       In  Varchar2,
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);
	
	
	--JM: Poblamiento de las tablas FAC
  ----- Revaluación
  Procedure DMPR_FACT_REVA( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                            pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );  
  ----- Revaluación Histórica                            
  Procedure DMPR_FACT_REVA_HIST( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                                 pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
  
  
    ----- Estados Financieros
  Procedure DMPR_FACT_FINA( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                            pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type );
                            
  ----- Estados Financieros Historicos                            
  Procedure DMPR_FACT_FINA_HIST( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                                 pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type ); 
  
	
	--###### llamadas ejecucion del ETL
	--carga inicial:
	PROCEDURE BIAF_CRGA_INIC(
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);	
	
	--carga periodica tablones
	PROCEDURE BIAF_CRGA_PERI_TBL(
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);	
	
	--carga periodica dimenciones:
	PROCEDURE BIAF_CRGA_PERI_DIM(
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);	
	
	--carga periodica fact y fact historica:
	PROCEDURE BIAF_CRGA_PERI_FACT(
		ld_periodo       In  Varchar2,
		pn_Erro          Out Number,
		pv_Mens          Out Varchar2);	
		
	--carga periodica ETL
	PROCEDURE BIAF_CRGA_PERI_ETL;	
	
End DMPG_BIAF_LOAD_DTMR;
/
Create Or Replace Package Body DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR Is
	
	--##############  Carga Stage Tablones ############### ---
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon compañia            ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_CIA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select conjunto,nombre,nit 
		from ERPADMIN.conjunto@EXACTUS; 
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS    		Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_CIA',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;

			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_CIA(
					raz_soci_cia,
					nro_ruc_cia,
					nom_cjto,
					fech_ulti_actu
				)values(
					d(y).nombre,
					d(y).nit,
					d(y).conjunto,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;
				
			End Loop;  
			--commit cada 1000 reg
			commit;
			EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_CIA' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_CIA;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon cuenta contable     ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_CTA_CTBL(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		Select Distinct trim(x.cuenta_contable) as cuenta_contable, trim(x.descripcion) as descripcion, x.cia
		From 
		(
			Select a.cuenta_contable, a.descripcion, kv_compania_hermes as cia  From admin.cuenta_contable@exactus a Where a.acepta_datos=kv_acepta_datos And a.uso_restringido=kv_uso_restringido
			Union All
			Select b.cuenta_contable,  b.descripcion, kv_compania_smart  as cia From smart.cuenta_contable@exactus b Where b.acepta_datos=kv_acepta_datos And b.uso_restringido=kv_uso_restringido
		) x;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_CNTA_CONTA',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				Insert Into STG_ACTIVO_FIJO.TBL_CNTA_CONTA (
					cnta_cont,
					desc_cnta_cont,
					cia,
					fech_ulti_actu
				)Values(
					d(y).cuenta_contable,
					d(y).descripcion,
					d(y).cia,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_CTA_CTBL' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_CTA_CTBL;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon tipo contrato       ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_TIP_CRTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		Select qq.codigo, qq.descripcion , kv_compania_smart as cia
		From SMART.valores_rubro_af@EXACTUS qq where qq.rubro = 1
		union all
		Select pp.codigo, pp.descripcion , kv_compania_hermes as cia
		From ADMIN.valores_rubro_af@EXACTUS pp where pp.rubro = 1;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_TIPO_CNTR',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_TIPO_CNTR(
					cod_tipo_ctrto,
					desc_tipo_ctrto,
					cia,
					fech_ulti_actu
				)values(
					d(y).codigo,
					d(y).descripcion,
					d(y).cia,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_TIP_CRTO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_TIP_CRTO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon familia activo      ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_FAMI_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select trim(pp.clasificacion_af) clasificacion_af, pp.descripcion  
		from ERPADMIN.clasificacion_af@EXACTUS pp;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_FAMI_ACTI',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_FAMI_ACTI(
					codi_fam,
					nom_rubr,
					clas_acti_fijo,
					fech_ulti_actu
				)values(
					d(y).clasificacion_af,
					d(y).descripcion,
					'TANGIBLE',
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_FAMI_ACTI' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_FAMI_ACTI;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon sucursal            ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_SUCU(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		Select cod_sucu_htb, des_sucu_htb, 
		(
			case when UPPER(substr(des_sucu_htb,0,3)) = 'SMA' then
			'SMART'
			else
			'ADMIN'
			end
		)
		as cia
			From general.tg_sucursal_htb@htbp
			Where des_sucu_htb Not Like '%XX%';
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_SUCU',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				Insert Into STG_ACTIVO_FIJO.TBL_SUCU(
					cod_sucu,
					des_sucu,
					cia,
					fech_ulti_actu					
				)Values(
					d(y).cod_sucu_htb,
					d(y).des_sucu_htb,
					d(y).cia,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_SUCU' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_SUCU;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon proveedor           ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_PROV(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		Select Distinct x.proveedor, x.nombre, x.cia
		From 
		(
			Select a.proveedor, a.nombre, kv_compania_hermes as cia From admin.proveedor@exactus a Where a.activo = kv_activo
			Union All
			Select b.proveedor, b.nombre, kv_compania_smart as cia From smart.proveedor@exactus b Where b.activo = kv_activo
		) x;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_PROV',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				Insert Into STG_ACTIVO_FIJO.TBL_PROV(
					cntro_ruc ,
					nom_prov,
					cia,
					fech_ulti_actu
				)Values(
					d(y).proveedor,
					d(y).nombre,
					d(y).cia,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_PROV' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_PROV;
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon centro de costo     ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_CTRO_CSTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		Select Distinct trim(x.centro_costo) as centro_costo, trim(x.descripcion) as descripcion, cia
		From (
			Select a.centro_costo, a.descripcion, kv_compania_hermes as cia From admin.centro_costo@exactus a 
			where (a.descripcion not like 'XXX NO%') and a.acepta_datos=kv_acepta_datos
			Union All
			Select a.centro_costo, a.descripcion, kv_compania_smart as cia From smart.centro_costo@exactus a 
			where (a.descripcion not like '%NO USAR%' and a.descripcion not like '%NO UTILIZAR%') and  a.acepta_datos=kv_acepta_datos
		 ) x ;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_CNTR_CSTO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				Insert Into STG_ACTIVO_FIJO.TBL_CNTR_CSTO(
					cntr_csto,
					des_cntr_csto,
					cia,
					fech_ulti_actu					
				)Values(
					d(y).centro_costo ,
					d(y).descripcion,
					d(y).cia,
					sysdate					
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_CTRO_CSTO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_CTRO_CSTO;
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ tipo activo                      ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_TIP_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select 
			kv_compania_hermes cia, ta.TIPO_ACTIVO, ta.DESCRIPCION, ta.TIPO_DEPR_FISCAL, 
			ta.TIPO_DEPR_COMP, ta.USA_DEPR_PORC, ta.SUMA_100_DEPR_PORC, 
			ta.TIPO_REVAL, ta.PORC_REVAL, ta.CTR_ACTIVO, ta.CTA_ACTIVO, ta.CTR_DEPR_ACUM_NORM, 
			ta.CTA_DEPR_ACUM_NORM, ta.CTR_REVALUACION, ta.CTA_REVALUACION, ta.CTR_DEPR_ACUM_REV, 
			ta.CTA_DEPR_ACUM_REV, ta.TIPO_INDICE_PRECIO, ta.CONSECUTIVO, ta.CTR_ACM_TIPO, 
			ta.CTA_ACM_TIPO, ta.CTR_RETIRO_ACT, ta.CTA_RETIRO_ACT, ta.CTR_RETIRO_DEPR,
			ta.CTA_RETIRO_DEPR, ta.PRIMEROS_MESES, ta.MESES_NO_DEPREC, ta.TIPO_CAMBIO, 
			ta.TIPO_PLAZO_VIDA_UTIL, ta.TIPO_PLAZO_VIDA_COMP, ta.TIPO_VALOR_RESC_LOCAL_F, 
			ta.TIPO_VALOR_RESC_DOLAR_F, ta.TIPO_VALOR_RESC_LOCAL_C, ta.TIPO_VALOR_RESC_DOLAR_C, 
			ta.NOTEEXISTSFLAG, ta.RECORDDATE, ta.ROWPOINTER, ta.CREATEDBY, ta.CREATEDATE, ta.UPDATEDBY,
			ta.CTA_REVALUACION_ACTIVO, ta.CTA_DEPR_ACUM_REVAL_ACTIVO, ta.CTA_DEPR_REVAL_ACTIVO, 
			ta.CTR_REVALUACION_ACTIVO, ta.CTR_DEPR_ACUM_REVAL_ACTIVO, ta.CTR_DEPR_REVAL_ACTIVO, 
			ta.CTR_REVAL_RETIRO, ta.CTA_REVAL_RETIRO, ta.TIPO_REVAL_C, ta.PORC_REVAL_C, ta.TIPO_INDEXACION,
			ta.MESES_NO_DEPREC_MEJORA, ta.TIPO, ta.CTR_REVAL_NEG, ta.CTA_REVAL_NEG 
		from admin.tipo_activo@EXACTUS ta
		union all
		select 
			kv_compania_smart cia,sta.TIPO_ACTIVO, DESCRIPCION, sta.TIPO_DEPR_FISCAL, 
			sta.TIPO_DEPR_COMP, sta.USA_DEPR_PORC, sta.SUMA_100_DEPR_PORC, 
			sta.TIPO_REVAL, sta.PORC_REVAL, sta.CTR_ACTIVO, sta.CTA_ACTIVO, sta.CTR_DEPR_ACUM_NORM, 
			sta.CTA_DEPR_ACUM_NORM, sta.CTR_REVALUACION, sta.CTA_REVALUACION, sta.CTR_DEPR_ACUM_REV, 
			sta.CTA_DEPR_ACUM_REV, sta.TIPO_INDICE_PRECIO, sta.CONSECUTIVO, sta.CTR_ACM_TIPO, 
			sta.CTA_ACM_TIPO, sta.CTR_RETIRO_ACT, sta.CTA_RETIRO_ACT, sta.CTR_RETIRO_DEPR,
			sta.CTA_RETIRO_DEPR, sta.PRIMEROS_MESES, sta.MESES_NO_DEPREC, sta.TIPO_CAMBIO, 
			sta.TIPO_PLAZO_VIDA_UTIL, sta.TIPO_PLAZO_VIDA_COMP, sta.TIPO_VALOR_RESC_LOCAL_F, 
			sta.TIPO_VALOR_RESC_DOLAR_F, sta.TIPO_VALOR_RESC_LOCAL_C, sta.TIPO_VALOR_RESC_DOLAR_C, 
			sta.NOTEEXISTSFLAG, sta.RECORDDATE, sta.ROWPOINTER, sta.CREATEDBY, sta.CREATEDATE, sta.UPDATEDBY,
			sta.CTA_REVALUACION_ACTIVO, sta.CTA_DEPR_ACUM_REVAL_ACTIVO, sta.CTA_DEPR_REVAL_ACTIVO, 
			sta.CTR_REVALUACION_ACTIVO, sta.CTR_DEPR_ACUM_REVAL_ACTIVO, sta.CTR_DEPR_REVAL_ACTIVO, 
			sta.CTR_REVAL_RETIRO, sta.CTA_REVAL_RETIRO, TIPO_REVAL_C, sta.PORC_REVAL_C, sta.TIPO_INDEXACION,
			sta.MESES_NO_DEPREC_MEJORA, sta.TIPO, sta.CTR_REVAL_NEG, sta.CTA_REVAL_NEG 
		from smart.tipo_activo@EXACTUS sta;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_TIPO_ACTIVO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_TIPO_ACTIVO (
					TIPO_ACTIVO, DESCRIPCION, TIPO_DEPR_FISCAL, TIPO_DEPR_COMP, USA_DEPR_PORC, SUMA_100_DEPR_PORC, 
					TIPO_REVAL, PORC_REVAL, CTR_ACTIVO, CTA_ACTIVO, CTR_DEPR_ACUM_NORM, 
					CTA_DEPR_ACUM_NORM, CTR_REVALUACION, CTA_REVALUACION, CTR_DEPR_ACUM_REV, 
					CTA_DEPR_ACUM_REV, TIPO_INDICE_PRECIO, CONSECUTIVO, CTR_ACM_TIPO, 
					CTA_ACM_TIPO, CTR_RETIRO_ACT, CTA_RETIRO_ACT, CTR_RETIRO_DEPR,
					CTA_RETIRO_DEPR, PRIMEROS_MESES, MESES_NO_DEPREC, TIPO_CAMBIO, 
					TIPO_PLAZO_VIDA_UTIL, TIPO_PLAZO_VIDA_COMP, TIPO_VALOR_RESC_LOCAL_F, 
					TIPO_VALOR_RESC_DOLAR_F, TIPO_VALOR_RESC_LOCAL_C, TIPO_VALOR_RESC_DOLAR_C, 
					NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, CREATEDBY, CREATEDATE, UPDATEDBY,
					CTA_REVALUACION_ACTIVO, CTA_DEPR_ACUM_REVAL_ACTIVO, CTA_DEPR_REVAL_ACTIVO, 
					CTR_REVALUACION_ACTIVO, CTR_DEPR_ACUM_REVAL_ACTIVO, CTR_DEPR_REVAL_ACTIVO, 
					CTR_REVAL_RETIRO, CTA_REVAL_RETIRO, TIPO_REVAL_C, PORC_REVAL_C, TIPO_INDEXACION,
					MESES_NO_DEPREC_MEJORA, TIPO, CTR_REVAL_NEG, CTA_REVAL_NEG,cia,
					fech_ulti_actu
				) values (
					d(y).TIPO_ACTIVO,
					d(y).DESCRIPCION,
					d(y).TIPO_DEPR_FISCAL,
					d(y).TIPO_DEPR_COMP,
					d(y).USA_DEPR_PORC,
					d(y).SUMA_100_DEPR_PORC,
					d(y).TIPO_REVAL,
					d(y).PORC_REVAL,
					d(y).CTR_ACTIVO,
					d(y).CTA_ACTIVO,
					d(y).CTR_DEPR_ACUM_NORM, 
					d(y).CTA_DEPR_ACUM_NORM,
					d(y).CTR_REVALUACION,
					d(y).CTA_REVALUACION,
					d(y).CTR_DEPR_ACUM_REV, 
					d(y).CTA_DEPR_ACUM_REV,
					d(y).TIPO_INDICE_PRECIO,
					d(y).CONSECUTIVO,
					d(y).CTR_ACM_TIPO, 
					d(y).CTA_ACM_TIPO,
					d(y).CTR_RETIRO_ACT,
					d(y).CTA_RETIRO_ACT,
					d(y).CTR_RETIRO_DEPR,
					d(y).CTA_RETIRO_DEPR,
					d(y).PRIMEROS_MESES,
					d(y).MESES_NO_DEPREC,
					d(y).TIPO_CAMBIO, 
					d(y).TIPO_PLAZO_VIDA_UTIL, 
					d(y).TIPO_PLAZO_VIDA_COMP, 
					d(y).TIPO_VALOR_RESC_LOCAL_F, 
					d(y).TIPO_VALOR_RESC_DOLAR_F, 
					d(y).TIPO_VALOR_RESC_LOCAL_C, 
					d(y).TIPO_VALOR_RESC_DOLAR_C, 
					d(y).NOTEEXISTSFLAG, 
					d(y).RECORDDATE, 
					d(y).ROWPOINTER, 
					d(y).CREATEDBY, 
					d(y).CREATEDATE, 
					d(y).UPDATEDBY,
					d(y).CTA_REVALUACION_ACTIVO, 
					d(y).CTA_DEPR_ACUM_REVAL_ACTIVO, 
					d(y).CTA_DEPR_REVAL_ACTIVO, 
					d(y).CTR_REVALUACION_ACTIVO, 
					d(y).CTR_DEPR_ACUM_REVAL_ACTIVO, 
					d(y).CTR_DEPR_REVAL_ACTIVO, 
					d(y).CTR_REVAL_RETIRO, 
					d(y).CTA_REVAL_RETIRO, 
					d(y).TIPO_REVAL_C, 
					d(y).PORC_REVAL_C, 
					d(y).TIPO_INDEXACION,
					d(y).MESES_NO_DEPREC_MEJORA, 
					d(y).TIPO, 
					d(y).CTR_REVAL_NEG, 
					d(y).CTA_REVAL_NEG,
					d(y).cia,
					sysdate
				);

				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_TIP_ACTI' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_TIP_ACTI;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon                     ║
		║         │             │                    │              │ tipo activo                      ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_TIP_ACTI(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select * from STG_ACTIVO_FIJO.TBL_TIPO_ACTIVO;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_TIP_ACTIVO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				Insert Into STG_ACTIVO_FIJO.TBL_TIP_ACTIVO(
					cod_tipo_acti,
					des_tipo_acti,
					tip_indi_prcio,
					tip_depr_corp,
					tip_depr_fiscal,
					tip_reval_corp,
					tip_reval_cont,
					cnta_cont,
					cia,
					fech_ulti_actu
				)Values(
					d(y).TIPO_ACTIVO ,
					d(y).DESCRIPCION, 
					d(y).TIPO_INDICE_PRECIO,
					d(y).TIPO_DEPR_COMP,
					d(y).TIPO_DEPR_FISCAL,
					d(y).TIPO_REVAL_C,
					d(y).TIPO_REVAL,
					d(y).CTA_ACTIVO,
					d(y).cia,
					sysdate
				); 
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_TIP_ACTI' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_TIP_ACTI;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ activo fijo                      ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_ACTI_FIJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select 
		kv_compania_hermes cia,
		af.ACTIVO_FIJO, af.UBICACION, af.TIPO_ACTIVO, af.DESCRIPCION, af.FECHA_ULT_MANT, af.FECHA_PROX_MANT, 
		af.RUBRO1_AF, RUBRO2_AF, af.RUBRO3_AF, af.RUBRO4_AF, af.RUBRO5_AF, af.RUBRO6_AF,
		af.RUBRO7_AF, af.RUBRO8_AF, af.RUBRO9_AF, af.RUBRO10_AF, af.AGRUPACION_AF, af.NIT, af.NOTEEXISTSFLAG,
		af.u_asientomy,af.u_tiortrans
		
		from admin.activo_fijo@EXACTUS af
		union all
		select 
		kv_compania_smart cia, 
		afs.ACTIVO_FIJO, afs.UBICACION, afs.TIPO_ACTIVO, afs.DESCRIPCION, afs.FECHA_ULT_MANT, afs.FECHA_PROX_MANT, 
		afs.RUBRO1_AF, RUBRO2_AF, afs.RUBRO3_AF, afs.RUBRO4_AF, afs.RUBRO5_AF, afs.RUBRO6_AF,
		afs.RUBRO7_AF, afs.RUBRO8_AF, afs.RUBRO9_AF, afs.RUBRO10_AF, afs.AGRUPACION_AF, afs.NIT, afs.NOTEEXISTSFLAG,
		afs.u_asientomy,afs.u_tiortrans
		from smart.activo_fijo@EXACTUS afs;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_ACTIVO_FIJO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_ACTIVO_FIJO (
					ACTIVO_FIJO, UBICACION, TIPO_ACTIVO, DESCRIPCION, FECHA_ULT_MANT, FECHA_PROX_MANT, 
					RUBRO1_AF, RUBRO2_AF, RUBRO3_AF, RUBRO4_AF, RUBRO5_AF, RUBRO6_AF,
					RUBRO7_AF, RUBRO8_AF, RUBRO9_AF, RUBRO10_AF, AGRUPACION_AF, NIT, NOTEEXISTSFLAG,
					u_asientomy,u_tiortrans,
					cia, fech_ulti_actu
				)
				values (
					d(y).ACTIVO_FIJO, d(y).UBICACION, d(y).TIPO_ACTIVO, d(y).DESCRIPCION, d(y).FECHA_ULT_MANT, d(y).FECHA_PROX_MANT, 
					d(y).RUBRO1_AF, d(y).RUBRO2_AF, d(y).RUBRO3_AF, d(y).RUBRO4_AF, d(y).RUBRO5_AF, d(y).RUBRO6_AF,
					d(y).RUBRO7_AF, d(y).RUBRO8_AF, d(y).RUBRO9_AF, d(y).RUBRO10_AF, d(y).AGRUPACION_AF, d(y).NIT, d(y).NOTEEXISTSFLAG,

					d(y).u_asientomy, d(y).u_tiortrans,

					d(y).cia,
					sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_ACTI_FIJO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_ACTI_FIJO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ activo mejora                    ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_ACTI_MEJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select kv_compania_hermes as cia,
		ACTIVO_FIJO, MEJORA, DESCRIPCION, PLAZO_VIDA_UTIL, 
		PLAZO_VIDA_COMP, FECHA_ULT_REVAL_F, FECHA_ULT_REVAL_C, 
		FECHA_ULT_DEPR_F, FECHA_ULT_DEPR_C, TIPO_DEPR_FISCAL, 
		TIPO_DEPR_COMP, TIPO_REVAL, TIPO_INDICE_PRECIO, 
		PORC_REVAL, COSTO_ORIG_LOCAL, COSTO_ORIG_DOLAR, 
		VALOR_RESC_LOCAL_F, VALOR_RESC_DOLAR_F, VALOR_RESC_LOCAL_C, 
		VALOR_RESC_DOLAR_C, DEP_ACU_HIST_LOC_F, DEP_ACU_HIST_DOL_F, 
		DEP_ACU_HIST_LOC_C, DEP_ACU_HIST_DOL_C, DEP_ACU_REVA_LOC_F, 
		DEP_ACU_REVA_DOL_F, DEP_ACU_REVA_LOC_C, DEP_ACU_REVA_DOL_C, 
		ACUM_REVAL_LOC_F, ACUM_REVAL_DOL_F, ACUM_REVAL_LOC_C, 
		ACUM_REVAL_DOL_C, FECHA_ADQUISICION, FECHA_ACTIVACION, 
		FECHA_RETIRO, USUARIO_RETIRO, FECHA_ULT_MOD, 
		USUARIO_ULT_MOD, ORDEN_COMPRA, PROVEEDOR, 
		CODIGO_BARRAS, NUMERO_SERIE, ASIENTO_INGRESO, 
		ASIENTO_RETIRO_F, ASIENTO_RETIRO_C, NOTAS, 
		MAX_VALORLIB_LOC_F, MAX_VALORLIB_DOL_F, MAX_VALORLIB_LOC_C, 
		MAX_VALORLIB_DOL_C, RESPONSABLE, SALDO_HIST_LOC_F, 
		SALDO_HIST_DOL_F, SALDO_HIST_LOC_C, SALDO_HIST_DOL_C, 
		INDEX_ACUM_LOC_F, INDEX_ACUM_DOL_F, INDEX_ACUM_LOC_C, 
		INDEX_ACUM_DOL_C, DEP_INDEX_LOC_F, DEP_INDEX_DOL_F, 
		DEP_INDEX_LOC_C, DEP_INDEX_DOL_C, DEP_ACUM_LOC_F, 
		DEP_ACUM_DOL_F, DEP_ACUM_LOC_C, DEP_ACUM_DOL_C, 
		DEP_HIST_LOC_F, DEP_HIST_DOL_F, DEP_HIST_LOC_C, 
		DEP_HIST_DOL_C, FECHA_ULT_INDEX_F, FECHA_ULT_INDEX_C, 
		DEP_ACUM_HIS_LOC_F, DEP_ACUM_HIS_DOL_F, DEP_ACUM_HIS_LOC_C, 
		DEP_ACUM_HIS_DOL_C, TIPO_ACTIVO, FECHA_RETIRO_C, 
		USUARIO_RETIRO_C, SUBTIPO_ACTIVO, AJUSTE, 
		FECHA_DEP_INI, DEP_INI_LOCAL, DEP_INI_DOLAR, 
		NOTAS_RETIRO, MOTIVO_RETIRO, TIPO_CAMBIO, 
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY, 
		FACTURA, RUBRO1_AF, RUBRO2_AF, 
		RUBRO3_AF, RUBRO4_AF, RUBRO5_AF, 
		RUBRO6_AF, RUBRO7_AF, RUBRO8_AF, 
		RUBRO9_AF, RUBRO10_AF, RUBRO11_AF, 
		RUBRO12_AF, RUBRO13_AF, RUBRO14_AF, 
		RUBRO15_AF, RUBRO16_AF, RUBRO17_AF, 
		RUBRO18_AF, RUBRO19_AF, RUBRO20_AF, 
		TIPO_REVAL_C, PORC_REVAL_C, TIPO_INDEXACION, 
		CLASIFICACION_AF, ES_OBRA_CURSO, UNSPSC, 
		CONTRATO_LEASING, NUMERO_CUOTAS_LEASING, MONTO_LEASING_LOCAL, 
		MONTO_LEASING_DOLAR, DEP_INI_LOCAL_C, DEP_INI_DOLAR_C, 
		ASIENTO_INGRESO_C, COSTO_ORIG_LOCAL_C, COSTO_ORIG_DOLAR_C, 
		FECHA_DEP_INI_C, CENTRO_COSTO_ALTA
		from admin.activo_mejora@EXACTUS
		union all
		select  kv_compania_smart as cia,
		ACTIVO_FIJO, MEJORA, DESCRIPCION, PLAZO_VIDA_UTIL, 
		PLAZO_VIDA_COMP, FECHA_ULT_REVAL_F, FECHA_ULT_REVAL_C, 
		FECHA_ULT_DEPR_F, FECHA_ULT_DEPR_C, TIPO_DEPR_FISCAL, 
		TIPO_DEPR_COMP, TIPO_REVAL, TIPO_INDICE_PRECIO, 
		PORC_REVAL, COSTO_ORIG_LOCAL, COSTO_ORIG_DOLAR, 
		VALOR_RESC_LOCAL_F, VALOR_RESC_DOLAR_F, VALOR_RESC_LOCAL_C, 
		VALOR_RESC_DOLAR_C, DEP_ACU_HIST_LOC_F, DEP_ACU_HIST_DOL_F, 
		DEP_ACU_HIST_LOC_C, DEP_ACU_HIST_DOL_C, DEP_ACU_REVA_LOC_F, 
		DEP_ACU_REVA_DOL_F, DEP_ACU_REVA_LOC_C, DEP_ACU_REVA_DOL_C, 
		ACUM_REVAL_LOC_F, ACUM_REVAL_DOL_F, ACUM_REVAL_LOC_C, 
		ACUM_REVAL_DOL_C, FECHA_ADQUISICION, FECHA_ACTIVACION, 
		FECHA_RETIRO, USUARIO_RETIRO, FECHA_ULT_MOD, 
		USUARIO_ULT_MOD, ORDEN_COMPRA, PROVEEDOR, 
		CODIGO_BARRAS, NUMERO_SERIE, ASIENTO_INGRESO, 
		ASIENTO_RETIRO_F, ASIENTO_RETIRO_C, NOTAS, 
		MAX_VALORLIB_LOC_F, MAX_VALORLIB_DOL_F, MAX_VALORLIB_LOC_C, 
		MAX_VALORLIB_DOL_C, RESPONSABLE, SALDO_HIST_LOC_F, 
		SALDO_HIST_DOL_F, SALDO_HIST_LOC_C, SALDO_HIST_DOL_C, 
		INDEX_ACUM_LOC_F, INDEX_ACUM_DOL_F, INDEX_ACUM_LOC_C, 
		INDEX_ACUM_DOL_C, DEP_INDEX_LOC_F, DEP_INDEX_DOL_F, 
		DEP_INDEX_LOC_C, DEP_INDEX_DOL_C, DEP_ACUM_LOC_F, 
		DEP_ACUM_DOL_F, DEP_ACUM_LOC_C, DEP_ACUM_DOL_C, 
		DEP_HIST_LOC_F, DEP_HIST_DOL_F, DEP_HIST_LOC_C, 
		DEP_HIST_DOL_C, FECHA_ULT_INDEX_F, FECHA_ULT_INDEX_C, 
		DEP_ACUM_HIS_LOC_F, DEP_ACUM_HIS_DOL_F, DEP_ACUM_HIS_LOC_C, 
		DEP_ACUM_HIS_DOL_C, TIPO_ACTIVO, FECHA_RETIRO_C, 
		USUARIO_RETIRO_C, SUBTIPO_ACTIVO, AJUSTE, 
		FECHA_DEP_INI, DEP_INI_LOCAL, DEP_INI_DOLAR, 
		NOTAS_RETIRO, MOTIVO_RETIRO, TIPO_CAMBIO, 
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY, 
		FACTURA, RUBRO1_AF, RUBRO2_AF, 
		RUBRO3_AF, RUBRO4_AF, RUBRO5_AF, 
		RUBRO6_AF, RUBRO7_AF, RUBRO8_AF, 
		RUBRO9_AF, RUBRO10_AF, RUBRO11_AF, 
		RUBRO12_AF, RUBRO13_AF, RUBRO14_AF, 
		RUBRO15_AF, RUBRO16_AF, RUBRO17_AF, 
		RUBRO18_AF, RUBRO19_AF, RUBRO20_AF, 
		TIPO_REVAL_C, PORC_REVAL_C, TIPO_INDEXACION, 
		CLASIFICACION_AF, ES_OBRA_CURSO, UNSPSC, 
		CONTRATO_LEASING, NUMERO_CUOTAS_LEASING, MONTO_LEASING_LOCAL, 
		MONTO_LEASING_DOLAR,
		
		/* --antes de magan
		DEP_INI_LOCAL_C, DEP_INI_DOLAR_C, 
		ASIENTO_INGRESO_C, COSTO_ORIG_LOCAL_C, COSTO_ORIG_DOLAR_C, 
		FECHA_DEP_INI_C, CENTRO_COSTO_ALTA
		*/
		--momentaneo: 
		0 as DEP_INI_LOCAL_C, 
		0 as DEP_INI_DOLAR_C, 
		'' as ASIENTO_INGRESO_C, 
		0 as COSTO_ORIG_LOCAL_C, 
		0 as COSTO_ORIG_DOLAR_C, 
		NULL as FECHA_DEP_INI_C, 
		'' as CENTRO_COSTO_ALTA
		
		from smart.activo_mejora@EXACTUS;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_ACTIVO_MEJORA',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA (
				ACTIVO_FIJO, MEJORA, DESCRIPCION, PLAZO_VIDA_UTIL, 
				PLAZO_VIDA_COMP, FECHA_ULT_REVAL_F, FECHA_ULT_REVAL_C, 
				FECHA_ULT_DEPR_F, FECHA_ULT_DEPR_C, TIPO_DEPR_FISCAL, 
				TIPO_DEPR_COMP, TIPO_REVAL, TIPO_INDICE_PRECIO, 
				PORC_REVAL, COSTO_ORIG_LOCAL, COSTO_ORIG_DOLAR, 
				VALOR_RESC_LOCAL_F, VALOR_RESC_DOLAR_F, VALOR_RESC_LOCAL_C, 
				VALOR_RESC_DOLAR_C, DEP_ACU_HIST_LOC_F, DEP_ACU_HIST_DOL_F, 
				DEP_ACU_HIST_LOC_C, DEP_ACU_HIST_DOL_C, DEP_ACU_REVA_LOC_F, 
				DEP_ACU_REVA_DOL_F, DEP_ACU_REVA_LOC_C, DEP_ACU_REVA_DOL_C, 
				ACUM_REVAL_LOC_F, ACUM_REVAL_DOL_F, ACUM_REVAL_LOC_C, 
				ACUM_REVAL_DOL_C, FECHA_ADQUISICION, FECHA_ACTIVACION, 
				FECHA_RETIRO, USUARIO_RETIRO, FECHA_ULT_MOD, 
				USUARIO_ULT_MOD, ORDEN_COMPRA, PROVEEDOR, 
				CODIGO_BARRAS, NUMERO_SERIE, ASIENTO_INGRESO, 
				ASIENTO_RETIRO_F, ASIENTO_RETIRO_C, NOTAS, 
				MAX_VALORLIB_LOC_F, MAX_VALORLIB_DOL_F, MAX_VALORLIB_LOC_C, 
				MAX_VALORLIB_DOL_C, RESPONSABLE, SALDO_HIST_LOC_F, 
				SALDO_HIST_DOL_F, SALDO_HIST_LOC_C, SALDO_HIST_DOL_C, 
				INDEX_ACUM_LOC_F, INDEX_ACUM_DOL_F, INDEX_ACUM_LOC_C, 
				INDEX_ACUM_DOL_C, DEP_INDEX_LOC_F, DEP_INDEX_DOL_F, 
				DEP_INDEX_LOC_C, DEP_INDEX_DOL_C, DEP_ACUM_LOC_F, 
				DEP_ACUM_DOL_F, DEP_ACUM_LOC_C, DEP_ACUM_DOL_C, 
				DEP_HIST_LOC_F, DEP_HIST_DOL_F, DEP_HIST_LOC_C, 
				DEP_HIST_DOL_C, FECHA_ULT_INDEX_F, FECHA_ULT_INDEX_C, 
				DEP_ACUM_HIS_LOC_F, DEP_ACUM_HIS_DOL_F, DEP_ACUM_HIS_LOC_C, 
				DEP_ACUM_HIS_DOL_C, TIPO_ACTIVO, FECHA_RETIRO_C, 
				USUARIO_RETIRO_C, SUBTIPO_ACTIVO, AJUSTE, 
				FECHA_DEP_INI, DEP_INI_LOCAL, DEP_INI_DOLAR, 
				NOTAS_RETIRO, MOTIVO_RETIRO, TIPO_CAMBIO, 
				NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
				CREATEDBY, CREATEDATE, UPDATEDBY, 
				FACTURA, RUBRO1_AF, RUBRO2_AF, 
				RUBRO3_AF, RUBRO4_AF, RUBRO5_AF, 
				RUBRO6_AF, RUBRO7_AF, RUBRO8_AF, 
				RUBRO9_AF, RUBRO10_AF, RUBRO11_AF, 
				RUBRO12_AF, RUBRO13_AF, RUBRO14_AF, 
				RUBRO15_AF, RUBRO16_AF, RUBRO17_AF, 
				RUBRO18_AF, RUBRO19_AF, RUBRO20_AF, 
				TIPO_REVAL_C, PORC_REVAL_C, TIPO_INDEXACION, 
				CLASIFICACION_AF, ES_OBRA_CURSO, UNSPSC, 
				CONTRATO_LEASING, NUMERO_CUOTAS_LEASING, MONTO_LEASING_LOCAL, 
				MONTO_LEASING_DOLAR, DEP_INI_LOCAL_C, DEP_INI_DOLAR_C, 
				ASIENTO_INGRESO_C, COSTO_ORIG_LOCAL_C, COSTO_ORIG_DOLAR_C, 
				FECHA_DEP_INI_C, CENTRO_COSTO_ALTA, cia, fech_ulti_actu
				) values (
				d(y).ACTIVO_FIJO, d(y).MEJORA, d(y).DESCRIPCION, d(y).PLAZO_VIDA_UTIL, 
				d(y).PLAZO_VIDA_COMP, d(y).FECHA_ULT_REVAL_F, d(y).FECHA_ULT_REVAL_C, 
				d(y).FECHA_ULT_DEPR_F, d(y).FECHA_ULT_DEPR_C, d(y).TIPO_DEPR_FISCAL, 
				d(y).TIPO_DEPR_COMP, d(y).TIPO_REVAL, d(y).TIPO_INDICE_PRECIO, 
				d(y).PORC_REVAL, d(y).COSTO_ORIG_LOCAL, d(y).COSTO_ORIG_DOLAR, 
				d(y).VALOR_RESC_LOCAL_F, d(y).VALOR_RESC_DOLAR_F, d(y).VALOR_RESC_LOCAL_C, 
				d(y).VALOR_RESC_DOLAR_C, d(y).DEP_ACU_HIST_LOC_F, d(y).DEP_ACU_HIST_DOL_F, 
				d(y).DEP_ACU_HIST_LOC_C, d(y).DEP_ACU_HIST_DOL_C, d(y).DEP_ACU_REVA_LOC_F, 
				d(y).DEP_ACU_REVA_DOL_F, d(y).DEP_ACU_REVA_LOC_C, d(y).DEP_ACU_REVA_DOL_C, 
				d(y).ACUM_REVAL_LOC_F, d(y).ACUM_REVAL_DOL_F, d(y).ACUM_REVAL_LOC_C, 
				d(y).ACUM_REVAL_DOL_C, d(y).FECHA_ADQUISICION, d(y).FECHA_ACTIVACION, 
				d(y).FECHA_RETIRO, d(y).USUARIO_RETIRO, d(y).FECHA_ULT_MOD, 
				d(y).USUARIO_ULT_MOD, d(y).ORDEN_COMPRA, d(y).PROVEEDOR, 
				d(y).CODIGO_BARRAS, d(y).NUMERO_SERIE, d(y).ASIENTO_INGRESO, 
				d(y).ASIENTO_RETIRO_F, d(y).ASIENTO_RETIRO_C, d(y).NOTAS, 
				d(y).MAX_VALORLIB_LOC_F, d(y).MAX_VALORLIB_DOL_F, d(y).MAX_VALORLIB_LOC_C, 
				d(y).MAX_VALORLIB_DOL_C, d(y).RESPONSABLE, d(y).SALDO_HIST_LOC_F, 
				d(y).SALDO_HIST_DOL_F, d(y).SALDO_HIST_LOC_C, d(y).SALDO_HIST_DOL_C, 
				d(y).INDEX_ACUM_LOC_F, d(y).INDEX_ACUM_DOL_F, d(y).INDEX_ACUM_LOC_C, 
				d(y).INDEX_ACUM_DOL_C, d(y).DEP_INDEX_LOC_F, d(y).DEP_INDEX_DOL_F, 
				d(y).DEP_INDEX_LOC_C, d(y).DEP_INDEX_DOL_C, d(y).DEP_ACUM_LOC_F, 
				d(y).DEP_ACUM_DOL_F, d(y).DEP_ACUM_LOC_C, d(y).DEP_ACUM_DOL_C, 
				d(y).DEP_HIST_LOC_F, d(y).DEP_HIST_DOL_F, d(y).DEP_HIST_LOC_C, 
				d(y).DEP_HIST_DOL_C, d(y).FECHA_ULT_INDEX_F, d(y).FECHA_ULT_INDEX_C, 
				d(y).DEP_ACUM_HIS_LOC_F, d(y).DEP_ACUM_HIS_DOL_F, d(y).DEP_ACUM_HIS_LOC_C, 
				d(y).DEP_ACUM_HIS_DOL_C, d(y).TIPO_ACTIVO, d(y).FECHA_RETIRO_C, 
				d(y).USUARIO_RETIRO_C, d(y).SUBTIPO_ACTIVO, d(y).AJUSTE, 
				d(y).FECHA_DEP_INI, d(y).DEP_INI_LOCAL, d(y).DEP_INI_DOLAR, 
				d(y).NOTAS_RETIRO, d(y).MOTIVO_RETIRO, d(y).TIPO_CAMBIO, 
				d(y).NOTEEXISTSFLAG, d(y).RECORDDATE, d(y).ROWPOINTER, 
				d(y).CREATEDBY, d(y).CREATEDATE, d(y).UPDATEDBY, 
				d(y).FACTURA, d(y).RUBRO1_AF, d(y).RUBRO2_AF, 
				d(y).RUBRO3_AF, d(y).RUBRO4_AF, d(y).RUBRO5_AF, 
				d(y).RUBRO6_AF, d(y).RUBRO7_AF, d(y).RUBRO8_AF, 
				d(y).RUBRO9_AF, d(y).RUBRO10_AF, d(y).RUBRO11_AF, 
				d(y).RUBRO12_AF, d(y).RUBRO13_AF, d(y).RUBRO14_AF, 
				d(y).RUBRO15_AF, d(y).RUBRO16_AF, d(y).RUBRO17_AF, 
				d(y).RUBRO18_AF, d(y).RUBRO19_AF, d(y).RUBRO20_AF, 
				d(y).TIPO_REVAL_C, d(y).PORC_REVAL_C, d(y).TIPO_INDEXACION, 
				d(y).CLASIFICACION_AF, d(y).ES_OBRA_CURSO, d(y).UNSPSC, 
				d(y).CONTRATO_LEASING, d(y).NUMERO_CUOTAS_LEASING, d(y).MONTO_LEASING_LOCAL, 
				d(y).MONTO_LEASING_DOLAR, d(y).DEP_INI_LOCAL_C, d(y).DEP_INI_DOLAR_C, 
				d(y).ASIENTO_INGRESO_C, d(y).COSTO_ORIG_LOCAL_C, d(y).COSTO_ORIG_DOLAR_C, 
				d(y).FECHA_DEP_INI_C, d(y).CENTRO_COSTO_ALTA, d(y).cia, sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_ACTI_MEJO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_ACTI_MEJO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ historico depreciacion           ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_HIST_DEPR(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select kv_compania_hermes as cia,
		HIST_DEPRECIACION, ACTIVO_FIJO, MEJORA, 
		FECHA, CONTABILIDAD, REFERENCIA, 
		AJUSTE, METODO, DEPR_LOCAL, 
		DEPR_DOLAR, ASIENTO, USUARIO, 
		FECHA_HORA, DEPR_HIST_LOCAL, DEPR_HIST_DOLAR, 
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY, 
		DIF_LOCAL, DIF_DOLAR, CENTRO_COSTO_AJUSTE

		from admin.hist_depreciacion@EXACTUS
		union all
		select kv_compania_smart as cia,
		HIST_DEPRECIACION, ACTIVO_FIJO, MEJORA, 
		FECHA, CONTABILIDAD, REFERENCIA, 
		AJUSTE, METODO, DEPR_LOCAL, 
		DEPR_DOLAR, ASIENTO, USUARIO, 
		FECHA_HORA, DEPR_HIST_LOCAL, DEPR_HIST_DOLAR, 
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY, 
		DIF_LOCAL, DIF_DOLAR, CENTRO_COSTO_AJUSTE
		from smart.hist_depreciacion@EXACTUS;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_HIST_DEPRECIACION',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION(
					HIST_DEPRECIACION, ACTIVO_FIJO, MEJORA, 
					FECHA, CONTABILIDAD, REFERENCIA, 
					AJUSTE, METODO, DEPR_LOCAL, 
					DEPR_DOLAR, ASIENTO, USUARIO, 
					FECHA_HORA, DEPR_HIST_LOCAL, DEPR_HIST_DOLAR, 
					NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
					CREATEDBY, CREATEDATE, UPDATEDBY, 
					DIF_LOCAL, DIF_DOLAR, CENTRO_COSTO_AJUSTE, cia,
					fech_ulti_actu
				)
				values (
					d(y).HIST_DEPRECIACION, d(y).ACTIVO_FIJO, d(y).MEJORA, 
					d(y).FECHA, d(y).CONTABILIDAD, d(y).REFERENCIA, 
					d(y).AJUSTE, d(y).METODO, d(y).DEPR_LOCAL, 
					d(y).DEPR_DOLAR, d(y).ASIENTO, d(y).USUARIO, 
					d(y).FECHA_HORA, d(y).DEPR_HIST_LOCAL, d(y).DEPR_HIST_DOLAR, 
					d(y).NOTEEXISTSFLAG, d(y).RECORDDATE, d(y).ROWPOINTER, 
					d(y).CREATEDBY, d(y).CREATEDATE, d(y).UPDATEDBY, 
					d(y).DIF_LOCAL, d(y).DIF_DOLAR, d(y).CENTRO_COSTO_AJUSTE,d(y).cia,
					sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_HIST_DEPR' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_HIST_DEPR;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ historico revaluacion            ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_HIST_REVA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select
		kv_compania_hermes as cia,
		hist_revaluacion      ,
		activo_fijo           ,
		mejora                ,
		fecha                 ,
		metodo               ,
		reval_valor           ,
		reval_valor_c        ,
		reval_depr_acum_f    ,
		reval_depr_acum_c     ,
		tipo_cambio           ,
		usuario               ,
		fecha_hora           ,
		asiento_fiscal        ,
		asiento_comp          ,
		noteexistsflag        ,
		recorddate            ,
		rowpointer            ,
		createdby             ,
		createdate            ,
		updatedby             ,
		reval_efectuada       ,
		valor_resc_f          ,
		valor_resc_c          ,
		fe_ult_depreciacion_f ,
		fe_ult_depreciacion_c ,
		saldo_valor_f         ,
		saldo_valor_c         ,
		vida_util_reval       ,
		comentario            ,
		metodo_c              ,
		consec_depr_f        ,
		consec_depr_c         ,
		fe_ult_depreciacion ,
		vida_util_reval_c     
			
		from  admin.ACTIVO_HIST_REVAL@EXACTUS pp

		union all

		select 
		kv_compania_smart as cia,
		hist_revaluacion      ,
		activo_fijo           ,
		mejora                ,
		fecha                 ,
		metodo               ,
		reval_valor           ,
		reval_valor_c        ,
		reval_depr_acum_f    ,
		reval_depr_acum_c     ,
		tipo_cambio           ,
		usuario               ,
		fecha_hora           ,
		asiento_fiscal        ,
		asiento_comp          ,
		noteexistsflag        ,
		recorddate            ,
		rowpointer           ,
		createdby             ,
		createdate            ,
		updatedby             ,
		reval_efectuada       ,
		valor_resc_f          ,
		valor_resc_c          ,
		fe_ult_depreciacion_f ,
		fe_ult_depreciacion_c ,
		saldo_valor_f         ,
		saldo_valor_c         ,
		vida_util_reval       ,
		comentario            ,
		metodo_c              ,
		consec_depr_f        ,
		consec_depr_c         ,
		fe_ult_depreciacion ,
		--vida_util_reval_c   magan 
		0 as vida_util_reval_c --momentaneo    
		from smart.ACTIVO_HIST_REVAL@EXACTUS;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_HIST_REVALUACION',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_HIST_REVALUACION(
				hist_revaluacion      ,
				activo_fijo           ,
				mejora                ,
				fecha                 ,
				metodo               ,
				reval_valor           ,
				reval_valor_c        ,
				reval_depr_acum_f    ,
				reval_depr_acum_c     ,
				tipo_cambio           ,
				usuario               ,
				fecha_hora           ,
				asiento_fiscal        ,
				asiento_comp          ,
				noteexistsflag        ,
				recorddate            ,
				rowpointer           ,
				createdby             ,
				createdate            ,
				updatedby             ,
				reval_efectuada       ,
				valor_resc_f          ,
				valor_resc_c          ,
				fe_ult_depreciacion_f ,
				fe_ult_depreciacion_c ,
				saldo_valor_f         ,
				saldo_valor_c         ,
				vida_util_reval       ,
				comentario            ,
				metodo_c              ,
				consec_depr_f        ,
				consec_depr_c         ,
				fe_ult_depreciacion ,
				vida_util_reval_c, 
				cia,
				fech_ulti_actu
				)
				values (
				d(y).hist_revaluacion      ,
				d(y).activo_fijo           ,
				d(y).mejora                ,
				d(y).fecha                 ,
				d(y).metodo               ,
				d(y).reval_valor           ,
				d(y).reval_valor_c        ,
				d(y).reval_depr_acum_f    ,
				d(y).reval_depr_acum_c     ,
				d(y).tipo_cambio           ,
				d(y).usuario               ,
				d(y).fecha_hora           ,
				d(y).asiento_fiscal        ,
				d(y).asiento_comp          ,
				d(y).noteexistsflag        ,
				d(y).recorddate            ,
				d(y).rowpointer           ,
				d(y).createdby             ,
				d(y).createdate            ,
				d(y).updatedby             ,
				d(y).reval_efectuada       ,
				d(y).valor_resc_f          ,
				d(y).valor_resc_c          ,
				d(y).fe_ult_depreciacion_f ,
				d(y).fe_ult_depreciacion_c ,
				d(y).saldo_valor_f         ,
				d(y).saldo_valor_c         ,
				d(y).vida_util_reval       ,
				d(y).comentario            ,
				d(y).metodo_c              ,
				d(y).consec_depr_f        ,
				d(y).consec_depr_c         ,
				d(y).fe_ult_depreciacion ,
				d(y).vida_util_reval_c, 
				d(y).cia,
				sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_HIST_REVA' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_HIST_REVA;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ historico depreciacion           ║
		║         │             │                    │              │ de la revaluacion                ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_HIST_DEPR_REVA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		select 
		kv_compania_hermes as cia,
		hist_depreciacion,
		hist_revaluacion ,
		reval_efectuada,
		activo_fijo,
		mejora,
		fecha,
		contabilidad,
		depr_reval_loc,
		depr_reval_dol,
		noteexistsflag,
		recorddate,
		rowpointer,
		createdby,
		updatedby,
		createdate from admin.hist_depreciacion_reval@EXACTUS
		union all
		select 
		kv_compania_smart as cia,
		hist_depreciacion,
		hist_revaluacion ,
		reval_efectuada,
		activo_fijo,
		mejora,
		fecha,
		contabilidad,
		depr_reval_loc,
		depr_reval_dol,
		noteexistsflag,
		recorddate,
		rowpointer,
		createdby,
		updatedby,
		createdate
		from smart.hist_depreciacion_reval@EXACTUS;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_HIST_DEPRECIACION_REVAL',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION_REVAL (
				hist_depreciacion,
				hist_revaluacion ,
				reval_efectuada,
				activo_fijo,
				mejora,
				fecha,
				contabilidad,
				depr_reval_loc,
				depr_reval_dol,
				noteexistsflag,
				recorddate,
				rowpointer,
				createdby,
				updatedby,
				createdate,
				cia,
				fech_ulti_actu
				)
				values (
				d(y).hist_depreciacion,
				d(y).hist_revaluacion ,
				d(y).reval_efectuada,
				d(y).activo_fijo,
				d(y).mejora,
				d(y).fecha,
				d(y).contabilidad,
				d(y).depr_reval_loc,
				d(y).depr_reval_dol,
				d(y).noteexistsflag,
				d(y).recorddate,
				d(y).rowpointer,
				d(y).createdby,
				d(y).updatedby,
				d(y).createdate,
				d(y).cia,
				sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_HIST_DEPR_REVA' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_HIST_DEPR_REVA;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ depreciacion centro de costo     ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_DEPR_CENT_CSTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select 
		kv_compania_hermes as cia,
		HIST_DEPRECIACION, CENTRO_COSTO, DEPR_LOCAL, 
		DEPR_DOLAR, NOTEEXISTSFLAG, RECORDDATE, 
		ROWPOINTER, CREATEDBY, CREATEDATE, UPDATEDBY
		from admin.depr_centro_costo@EXACTUS
		union all 
		select
		kv_compania_smart as cia,
		HIST_DEPRECIACION, CENTRO_COSTO, DEPR_LOCAL, 
		DEPR_DOLAR, NOTEEXISTSFLAG, RECORDDATE, 
		ROWPOINTER, CREATEDBY, CREATEDATE, UPDATEDBY
		from smart.depr_centro_costo@EXACTUS;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_DEPR_CENTRO_COSTO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_DEPR_CENTRO_COSTO (
					HIST_DEPRECIACION, CENTRO_COSTO, DEPR_LOCAL, 
					DEPR_DOLAR, NOTEEXISTSFLAG, RECORDDATE, 
					ROWPOINTER, CREATEDBY, CREATEDATE, UPDATEDBY,
					cia, fech_ulti_actu
				)
				values (
					d(y).HIST_DEPRECIACION, d(y).CENTRO_COSTO, d(y).DEPR_LOCAL, 
					d(y).DEPR_DOLAR, d(y).NOTEEXISTSFLAG, d(y).RECORDDATE, 
					d(y).ROWPOINTER, d(y).CREATEDBY, d(y).CREATEDATE, d(y).UPDATEDBY,
					d(y).cia,sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_DEPR_CENT_CSTO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_DEPR_CENT_CSTO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ activo centro                    ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_ACTI_CNTRO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select 
		kv_compania_hermes as cia,
		ACTIVO_FIJO, CENTRO_COSTO, PORCENTAJE,
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY
		from admin.activo_centro@EXACTUS
		union all
		select
		kv_compania_smart as cia,
		ACTIVO_FIJO, CENTRO_COSTO, PORCENTAJE,
		NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
		CREATEDBY, CREATEDATE, UPDATEDBY
		from smart.activo_centro@EXACTUS;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_ACTIVO_CENTRO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_ACTIVO_CENTRO (
					ACTIVO_FIJO, CENTRO_COSTO, PORCENTAJE,
					NOTEEXISTSFLAG, RECORDDATE, ROWPOINTER, 
					CREATEDBY, CREATEDATE, UPDATEDBY, cia,fech_ulti_actu
				)
				values (
					d(y).ACTIVO_FIJO, d(y).CENTRO_COSTO, d(y).PORCENTAJE,
					d(y).NOTEEXISTSFLAG, d(y).RECORDDATE, d(y).ROWPOINTER, 
					d(y).CREATEDBY, d(y).CREATEDATE, d(y).UPDATEDBY, d(y).cia, sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_ACTI_CNTRO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_ACTI_CNTRO;
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon activo mejora       ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_ACTI_MEJO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select
            x.activo_fijo cod_activo,
			x.mejora      cod_mejora,
			
            (
				case when
					(				
					Select
						count(y.activo_fijo)
					From
						(select sam.activo_fijo from STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA sam 
						) y
					Where
					y.activo_fijo = x.activo_fijo
					) >= 2
				then 1
				else 0
				end
            ) as ind_mejora,
			
            x.descripcion       as nom_acti,
            x.ubicacion         as ubic_acti,
            x.nit               as nit,
            x.plazo_vida_util      plaz_vida_util_cont,
            x.plazo_vida_comp      plaz_vida_util_corp,
            x.fecha_adquisicion    fech_adqi,
            x.fecha_retiro         fech_retir_cont,
            x.fecha_retiro_c       fech_retir_corp,
            x.fecha_activacion     fech_activ,
			
			(
			case
			when
			(
				Select
						trunc(z.fecha_retiro)
				From
					(select sam.fecha_retiro, sam.mejora, sam.activo_fijo from STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA sam  where (sam.fecha_ult_reval_f is null and sam.fecha_ult_reval_c is null)
					) z
					
				 Where
				z.activo_fijo = x.activo_fijo
				And z.mejora  = x.mejora
			)
			is null
			then 'ALTA'
			else 'BAJA'
			end
			) as est_acti,
			
            x.tipo_depr_fiscal as tipo_depr_cont,
            x.tipo_depr_comp   as tipo_depr_corp,
            x.tipo_reval       as tipo_reval_cont,
            x.tipo_reval_c     as tipo_reval_corp,
            x.proveedor        as proveedor,
			
			--familia
            x.clasificacion_af as familia, 
            
			x.tipo_activo as cod_tipo_acti,
			
			--tipo contrato
            x.rubro11_af  as cod_tipo_ctrto,
             
            x.asiento_ingreso   as asnt_ingr_cont,
            x.asiento_ingreso_c as asnt_ingr_corp,
            x.asiento_retiro_f  as asnt_retir_cont,
            x.asiento_retiro_c  as asnt_retir_corp,
            x.orden_compra      as nro_ord_comp,
			(
				Select  rtrim(xmlagg(xmlelement(usuario,usuario||',')).extract('//text()'), ',')
				From 
				(   select pp.usuario, pp.orden_compra from STG_ACTIVO_FIJO.TBL_USUARIOS_APROB_OC pp
				) w
				Where w.orden_compra = x.orden_compra
			)
			as empl_apro_ord_comp,
            x.responsable as resp_acti,
            x.factura     as nro_fact,
			
			(
				select distinct f.moneda
				from 
				(
					select  dc.moneda, dc.documento, dc.proveedor  from ADMIN.documentos_cp@exactus dc 
					where  dc.tipo = kv_tipo_doc_fact and dc.monto > 0  
					union all
					select  dcs.moneda, dcs.documento, dcs.proveedor  from smart.documentos_cp@exactus dcs
					where  dcs.tipo = kv_tipo_doc_fact and dcs.monto > 0  
				)  f
				where 
				f.documento = x.factura and f.proveedor = x.proveedor
				
			) as mone_fact,
			
            x.contrato_leasing      as nro_ctrto_lsng,
            x.numero_cuotas_leasing as nro_cuot_lsng,
			
			x.mon_lsng_local,
			x.mon_lsng_dolar,
			
            (

				select g.fecha_documento
				from (
						(Select distinct
							trunc(d.fecha_documento) as fecha_documento
						From
						ADMIN.documentos_cp@exactus  d
						, STG_ACTIVO_FIJO.TBL_PROV           p
						, ADMIN.subtipo_doc_cp@exactus s
						Where
						trim(d.proveedor) = trim(p.cntro_ruc)
						And d.tipo        = s.tipo
						And d.subtipo     = s.subtipo 
            And d.tipo = kv_tipo_doc_fact   
            And d.monto > 0
						And d.proveedor   = x.proveedor
						And d.documento   = x.factura
						) union all
						(
						Select distinct
							trunc(h.fecha_documento) as fecha_documento
						From
						smart.documentos_cp@exactus  h
						, STG_ACTIVO_FIJO.TBL_PROV           i
						, smart.subtipo_doc_cp@exactus j
						Where
						trim(h.proveedor) = trim(i.cntro_ruc)
						And h.tipo        = j.tipo
						And h.subtipo     = j.subtipo  
            And h.tipo = 'FAC'  
            And h.monto > 0
						And h.proveedor   = x.proveedor
						And h.documento   = x.factura
						) 
					)  g
							
            ) as fech_docu_fact,
            (
				Select
				k.fecha
				From
				(
					select d.fecha, d.prestamo from admin.prestamo@exactus d
					union all
					select m.fecha, m.prestamo  from  smart.prestamo@exactus m
				) k
				Where
				k.prestamo = x.contrato_leasing
            ) as fech_inic_ctrto_lsng,
			
			(
				Select
					q.fecha_vence
				From
				(
					select s.fecha_vence, s.prestamo from admin.prestamo@exactus s
					union all
					select r.fecha_vence, r.prestamo from smart.prestamo@exactus r
				) q	
				Where
				q.prestamo = x.contrato_leasing
            ) as fech_fin_ctrto_lsng, 
			
			(
				Select
					u.estado --CP01 CP02  salen dos: TIPO_OBLIGACION, PRESTAMO = 000000000006, 000000000004
				from
					(
					select d.estado, d.prestamo from admin.prestamo@exactus d
					union all
					select t.estado, t.prestamo from smart.prestamo@exactus t
					) u
				Where
					u.prestamo = x.contrato_leasing
            ) as esta_ctrto,
			
			(
				case
				when
				(
					select
					cb.usuario_aprobacion
					from
					(
					select  dcf.usuario_aprobacion, dcf.fecha_aprobacion, dcf.documento, dcf.proveedor 
          from admin.documentos_cp@exactus dcf 
          where  dcf.tipo = kv_tipo_doc_fact and dcf.monto > 0  
					union all
					select ab.usuario_aprobacion,ab.fecha_aprobacion, ab.documento, ab.proveedor 
          from smart.documentos_cp@exactus ab
          where  ab.tipo = kv_tipo_doc_fact and ab.monto > 0  
					) cb
					where
					cb.usuario_aprobacion   is not null
					and cb.fecha_aprobacion is not null
					and cb.documento                  = x.factura
					and cb.proveedor                  = x.proveedor	
				)
				is not null
				then 'PAGADO'
				else 'PENDIENTE'
				end
			)  as esta_docu_fact,
            x.costo_orig_local   as cost_orig_local_cont,
            x.costo_orig_dolar   as cost_orig_dolar_cont,
            x.costo_orig_local_c as cost_orig_local_corp,
            x.costo_orig_dolar_c as cost_orig_dolar_corp,
			--costos de baja
			(
				case when  (x.Fecha_Activacion is not null  AND  x.Fecha_Retiro is not null)  then
					x.Costo_Orig_Local
				else
					0
				end
			) as cost_orig_baja_local_cont,
			
			(
				case when  (x.Fecha_Activacion is not null  AND  x.Fecha_Retiro is not null)  then
					x.costo_orig_dolar
				else
					0
				end
			) as cost_orig_baja_dolar_cont,
			
			(
				case when  (x.Fecha_Activacion is not null  AND  x.Fecha_Retiro is not null)  then
					x.costo_orig_local_c
				else
					0
				end
			) as cost_orig_baja_local_corp,
			
			(
				case when  (x.Fecha_Activacion is not null  AND  x.Fecha_Retiro is not null)  then
					x.costo_orig_dolar_c
				else
					0
				end
			) as cost_orig_baja_dolar_corp,
			
			x.valor_resc_local_f as valo_resc_local_fiscal,
            x.valor_resc_dolar_f as valo_resc_dolar_fiscal,
            x.valor_resc_local_c as valo_resc_local_corp,
            x.valor_resc_dolar_c as valo_resc_dolar_corp,
            (				
				select
				fg.subtotal
				from
				(
				select dc1.subtotal, dc1.documento, dc1.proveedor 
        from  admin.documentos_cp@exactus dc1 
        where  dc1.tipo = kv_tipo_doc_fact and dc1.monto > 0  
				union all
				select abc.subtotal, abc.documento, abc.proveedor 
        from smart.documentos_cp@exactus abc 
        where abc.tipo = kv_tipo_doc_fact and abc.monto > 0  
				) fg
				where
				fg.documento     = x.factura
				and fg.proveedor = x.proveedor
					
            ) as mon_fact_sin_igv,
            (
				select
				jk.monto
				from
				(
					select dc2.monto, dc2.documento, dc2.proveedor 
          from admin.documentos_cp@exactus dc2  
          where  dc2.tipo = kv_tipo_doc_fact and dc2.monto > 0  
					union all
					select ac.monto, ac.documento, ac.proveedor 
          from  smart.documentos_cp@exactus ac 
          where  ac.tipo = kv_tipo_doc_fact and ac.monto > 0  
				) jk
				where
				jk.documento     = x.factura
				and jk.proveedor = x.proveedor
            ) as mon_tota_fact,
            0  as ind_transfer,
            x.u_tiortrans as tipo_origen_transfer, --codigo familia codi_fam FK
			x.u_asientomy as asnt_myor,
            x.rubro9_af as cntr_csto_gstor,
			x.cia
        from
		(	
			select 
				am.ACTIVO_FIJO, af.TIPO_ACTIVO,
				af.UBICACION, af.DESCRIPCION, 
				af.FECHA_ULT_MANT, af.FECHA_PROX_MANT, 
				af.AGRUPACION_AF, af.RUBRO1_AF, af.RUBRO2_AF, 
				af.RUBRO3_AF, af.RUBRO4_AF, af.RUBRO5_AF, af.RUBRO6_AF, 
				
				af.u_asientomy, af.u_tiortrans,
				
				af.RUBRO7_AF, af.RUBRO8_AF, am.RUBRO9_AF, af.RUBRO10_AF, 
				af.NIT, am.rubro11_af, am.RUBRO12_AF, am.RUBRO13_AF, af.RUBRO14_AF, 
				af.RUBRO15_AF, af.RUBRO16_AF, af.RUBRO17_AF, af.RUBRO18_AF, af.RUBRO19_AF,
				af.RUBRO20_AF,
				am.valor_resc_dolar_c, am.valor_resc_local_c, am.valor_resc_local_f, am.valor_resc_dolar_f,
				am.costo_orig_local, am.costo_orig_dolar, am.costo_orig_local_c, am.costo_orig_dolar_c,
				am.fecha_retiro, am.fecha_activacion, am.fecha_adquisicion,
				am.numero_cuotas_leasing, am.contrato_leasing, am.MONTO_LEASING_LOCAL as mon_lsng_local, am.MONTO_LEASING_DOLAR as mon_lsng_dolar,
				am.factura,am.responsable, am.orden_compra,
				am.asiento_retiro_c, am.asiento_ingreso, am.asiento_retiro_f, am.asiento_ingreso_c,
				am.clasificacion_af, am.proveedor,
				am.tipo_reval_c,am.tipo_reval,
				am.tipo_depr_fiscal,am.tipo_depr_comp, am.tipo_indexacion,
				am.fecha_retiro_c,
				am.mejora, am.plazo_vida_util,am.plazo_vida_comp,
				am.cia
				
			from STG_ACTIVO_FIJO.TBL_ACTIVO_FIJO   AF,
				STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA AM
			where af.activo_fijo = am.activo_fijo
			and af.cia = am.cia

         )  x;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_ACTI_MEJO',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_ACTI_MEJO
					( cod_activo
						  , cod_mejora
						  , ind_mejora
						  , nom_acti
						  , ubic_acti
						  , nit
						  , plaz_vida_util_cont
						  , plaz_vida_util_corp
						  , fech_adqi
						  , fech_retir_cont
						  , fech_retir_corp
						  , fech_activ
						  , est_acti
						  , tipo_depr_cont
						  , tipo_depr_corp
						  , tipo_reval_cont
						  , tipo_reval_corp
						  , proveedor--codigos no ID
						  , familia
						  , cod_tipo_acti
						  , cod_tipo_ctrto
						  , asnt_ingr_cont
						  , asnt_ingr_corp
						  , asnt_retir_cont
						  , asnt_retir_corp
						  , nro_ord_comp
						  , empl_apro_ord_comp
						  , resp_acti
						  , nro_fact
						  , mone_fact
						  , nro_ctrto_lsng
						  , nro_cuot_lsng
						  , mon_lsng_local
						  , mon_lsng_dolar
						  , fech_docu_fact
						  , fech_inic_ctrto_lsng
						  , fech_fin_ctrto_lsng
						  , esta_ctrto
						  , esta_docu_fact
						  , cost_orig_local_cont
						  , cost_orig_dolar_cont
						  , cost_orig_local_corp
						  , cost_orig_dolar_corp
						  , cost_orig_baja_local_cont
						  , cost_orig_baja_dolar_cont
						  , cost_orig_baja_local_corp
						  , cost_orig_baja_dolar_corp
						  , valo_resc_local_fiscal
						  , valo_resc_dolar_fiscal
						  , valo_resc_local_corp
						  , valo_resc_dolar_corp
						  , mon_fact_sin_igv
						  , mon_tota_fact
						  , ind_transfer
						  , tipo_origen_transfer
						  , asnt_myor
						  , cntr_csto_gstor,
						  cia,
						  fech_ulti_actu
				)values( 
						d(y).cod_activo
					  , d(y).cod_mejora
					  , d(y).ind_mejora
					  , d(y).nom_acti
					  , d(y).ubic_acti
					  , d(y).nit
					  , d(y).plaz_vida_util_cont
					  , d(y).plaz_vida_util_corp
					  , d(y).fech_adqi
					  , d(y).fech_retir_cont
					  , d(y).fech_retir_corp
					  , d(y).fech_activ
					  , d(y).est_acti
					  , d(y).tipo_depr_cont
					  , d(y).tipo_depr_corp
					  , d(y).tipo_reval_cont
					  , d(y).tipo_reval_corp
					  , d(y).proveedor --codigos no ID
					  , d(y).familia
					  , d(y).cod_tipo_acti
					  , d(y).cod_tipo_ctrto
					  , d(y).asnt_ingr_cont
					  , d(y).asnt_ingr_corp
					  , d(y).asnt_retir_cont
					  , d(y).asnt_retir_corp
					  , d(y).nro_ord_comp
					  , d(y).empl_apro_ord_comp
					  , d(y).resp_acti
					  , d(y).nro_fact
					  , d(y).mone_fact
					  , d(y).nro_ctrto_lsng
					  , d(y).nro_cuot_lsng
					  , d(y).mon_lsng_local
					  , d(y).mon_lsng_dolar
					  , d(y).fech_docu_fact
					  , d(y).fech_inic_ctrto_lsng
					  , d(y).fech_fin_ctrto_lsng
					  , d(y).esta_ctrto
					  , d(y).esta_docu_fact
					  , d(y).cost_orig_local_cont
					  , d(y).cost_orig_dolar_cont
					  , d(y).cost_orig_local_corp
					  , d(y).cost_orig_dolar_corp
					  , d(y).cost_orig_baja_local_cont
					  , d(y).cost_orig_baja_dolar_cont
					  , d(y).cost_orig_baja_local_corp
					  , d(y).cost_orig_baja_dolar_corp
					  , d(y).valo_resc_local_fiscal
					  , d(y).valo_resc_dolar_fiscal
					  , d(y).valo_resc_local_corp
					  , d(y).valo_resc_dolar_corp
					  , d(y).mon_fact_sin_igv
					  , d(y).mon_tota_fact
					  , d(y).ind_transfer
					  , d(y).tipo_origen_transfer
					  , d(y).asnt_myor
					  , d(y).cntr_csto_gstor
					  , d(y).cia
					  , sysdate
					 );
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_ACTI_MEJO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_ACTI_MEJO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon copia exactus       ║
		║         │             │                    │              │ usuarios aprobacion de la OC     ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TRNS_USU_APROC(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select 
		kv_compania_hermes as cia,
		ORDEN_COMPRA,
		USUARIO,
		FECHA_APROB,
		NOTEEXISTSFLAG,
		RECORDDATE,
		ROWPOINTER,
		CREATEDBY,
		UPDATEDBY,
		CREATEDATE 
		From admin.usuarios_aprob_oc@exactus
		union all
		select
		kv_compania_smart as cia,
		ORDEN_COMPRA,
		USUARIO,
		FECHA_APROB,
		NOTEEXISTSFLAG,
		RECORDDATE,
		ROWPOINTER,
		CREATEDBY,
		UPDATEDBY,
		CREATEDATE 
		From smart.usuarios_aprob_oc@exactus;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_USUARIOS_APROB_OC',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_USUARIOS_APROB_OC (
				ORDEN_COMPRA,
				USUARIO,
				FECHA_APROB,
				NOTEEXISTSFLAG,
				RECORDDATE,
				ROWPOINTER,
				CREATEDBY,
				UPDATEDBY,
				CREATEDATE,
				cia,
				fech_ulti_actu)
				values (
				d(y).ORDEN_COMPRA,
				d(y).USUARIO,
				d(y).FECHA_APROB,
				d(y).NOTEEXISTSFLAG,
				d(y).RECORDDATE,
				d(y).ROWPOINTER,
				d(y).CREATEDBY,
				d(y).UPDATEDBY,
				d(y).CREATEDATE,
				d(y).cia,
				sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TRNS_USU_APROC' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TRNS_USU_APROC;
	
	/**
		->>- @param IN:
			-ld_periodo : periodo de proceso, ejemplo :  '201703'
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Tablon fact activo         ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_TBLN_FACT_ACTI(
				ld_periodo       In  Varchar2,
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select 

		distinct ld_periodo as peri_mes_id,
		(select pp.nro_ruc_cia
			from STG_ACTIVO_FIJO.TBL_CIA pp
			where pp.nom_cjto = (case
				 when am.cia = kv_compania_smart then
				  'SMART'
				 else
				  'ADMIN'
			   end)
		) as nro_ruc_cia,
		am.cod_activo,
		am.cod_mejora,
		cc.centro_costo as cntr_csto,
		to_number(substr(cc.centro_costo, 1, 2)) as cod_sucu,

		--costos de adquisicion fiscal y corporativo
		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			(am.cost_orig_local_cont *  nvl(cc.porcentaje,100) / 100)
		else
			NULL
		end) as cost_orig_local_cont,

		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			(am.cost_orig_dolar_cont *  nvl(cc.porcentaje,100) / 100)
		else
			NULL
		end) as cost_orig_dolar_cont,

		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			(am.cost_orig_local_corp *  nvl(cc.porcentaje,100) / 100)
		else
			NULL
		end) as cost_orig_local_corp,

		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			(am.cost_orig_dolar_corp *  nvl(cc.porcentaje,100) / 100)
		 else
			NULL
		end) as cost_orig_dolar_corp,

		--cosots de baja
		(case when ltrim(TO_CHAR(trunc(am.fech_retir_cont), 'YYYYMM'), '0') = ld_periodo then
			am.cost_orig_baja_local_cont
		else
			0.0
		end) as cost_orig_baja_local_cont,

		(case when ltrim(TO_CHAR(trunc(am.fech_retir_cont), 'YYYYMM'), '0') = ld_periodo then
			am.cost_orig_baja_dolar_cont
		else
			0.0
		end) as cost_orig_baja_dolar_cont,

		(case when ltrim(TO_CHAR(trunc(am.fech_retir_corp), 'YYYYMM'), '0') = ld_periodo then
			am.cost_orig_baja_local_corp
		else
			0.0
		end) as cost_orig_baja_local_corp,

		(case when ltrim(TO_CHAR(trunc(am.fech_retir_corp), 'YYYYMM'), '0') = ld_periodo then
			am.cost_orig_baja_dolar_corp
		else
			0.0
		end) as cost_orig_baja_dolar_corp,

		--depreciaciones fiscal y corporativa

		(select avg(distinct nvl(dep.depr_local,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = 'F'
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = 'H' --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_local_cont,

		(select avg(distinct nvl(dep.depr_dolar,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = 'F'
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = 'H' --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_dolar_cont,

		(select avg(distinct nvl(dep.depr_local,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = 'C'
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = 'H' --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_local_corp,

		(select avg(distinct nvl(dep.depr_dolar,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = 'C'
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = 'H' --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_dolar_corp,

		(select distinct trunc(dep.fecha)
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = 'H' --depreciaciones no revaluacion
		   and am.cia = dep.cia) fech_depre_acti,

		nvl(cc.porcentaje,100) as pctj_cntr_cost, --100 o 0?

		--monto factura
		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			am.mon_fact_sin_igv
		else
			NULL
		end) as mon_fact_sin_igv,

		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			am.mon_tota_fact
		else
			NULL
		end) as mon_tota_fact

		from STG_ACTIVO_FIJO.TBL_ACTI_MEJO     am  left join 
		STG_ACTIVO_FIJO.TBL_ACTIVO_CENTRO cc on ( am.cod_activo = cc.activo_fijo and am.cia = cc.cia ) left join 
		STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dp on (am.cod_activo = dp.activo_fijo and am.cod_mejora = dp.mejora) 

		where

		am.cia = dp.cia and dp.referencia  = 'H'
		and ltrim(TO_CHAR(trunc(dp.fecha),'YYYYMM'),'0') =  ld_periodo
		--solo activos vigentes, se da prioridad a la baja contable o fiscal
		and to_date(to_char(nvl(trunc(am.fech_retir_cont),to_date('12/12/2050','dd/MM/YYYY')),'YYYYMM')  ,'YYYYMM')  >  to_date(ld_periodo, 'YYYYMM')
		--and to_date(to_char(nvl(am.fech_retir_corp,to_date('12/12/2050','dd/MM/YYYY')),'YYYYMM'),'YYYYMM')  >  to_date(ld_periodo, 'YYYYMM')

		--------  
		--am.cod_activo = '51058'
		--and am.cod_mejora = '51058'


		union all 
		--activos que no tienen centro de costo y/o depreciacion
		select  
		distinct
		ld_periodo as peri_mes_id,
		(select pp.nro_ruc_cia
		  from STG_ACTIVO_FIJO.TBL_CIA pp
		 where pp.nom_cjto = (case
				 when am.cia = kv_compania_smart then
				  'SMART'
				 else
				  'ADMIN'
			   end)) as nro_ruc_cia,
		am.cod_activo,
		am.cod_mejora,
		cc.centro_costo as cntr_csto,
		to_number(substr(cc.centro_costo, 1, 2)) as cod_sucu,

		--costos de adquisicion fiscal y corporativo
		(case
		 when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
		  (am.cost_orig_local_cont *  nvl(cc.porcentaje,100) / 100)
		 else
		  NULL
		end) as cost_orig_local_cont,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
		  (am.cost_orig_dolar_cont *  nvl(cc.porcentaje,100) / 100)
		 else
		  NULL
		end) as cost_orig_dolar_cont,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
		  (am.cost_orig_local_corp *  nvl(cc.porcentaje,100) / 100)
		 else
		  NULL
		end) as cost_orig_local_corp,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
		  (am.cost_orig_dolar_corp *  nvl(cc.porcentaje,100) / 100)
		 else
		  NULL
		end) as cost_orig_dolar_corp,

		--cosots de baja
		(case
		 when ltrim(TO_CHAR(trunc(am.fech_retir_cont), 'YYYYMM'), '0') = ld_periodo then
		  am.cost_orig_baja_local_cont
		 else
		  0.0
		end) as cost_orig_baja_local_cont,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_retir_cont), 'YYYYMM'), '0') = ld_periodo then
		  am.cost_orig_baja_dolar_cont
		 else
		  0.0
		end) as cost_orig_baja_dolar_cont,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_retir_corp), 'YYYYMM'), '0') = ld_periodo then
		  am.cost_orig_baja_local_corp
		 else
		  0.0
		end) as cost_orig_baja_local_corp,

		(case
		 when ltrim(TO_CHAR(trunc(am.fech_retir_corp), 'YYYYMM'), '0') = ld_periodo then
		  am.cost_orig_baja_dolar_corp
		 else
		  0.0
		end) as cost_orig_baja_dolar_corp,

		--depreciaciones fiscal y corporativa

		(select avg(distinct nvl(dep.depr_local,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = kv_dep_fiscal
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = kv_ref --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_local_cont,

		(select avg(distinct nvl(dep.depr_dolar,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = kv_dep_fiscal
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = kv_ref --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_dolar_cont,

		(select avg(distinct nvl(dep.depr_local,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = kv_dep_corp
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = kv_ref --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_local_corp,

		(select avg(distinct nvl(dep.depr_dolar,0))
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and dep.contabilidad = kv_dep_corp
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = kv_ref --depreciaciones no revaluacion
		   and am.cia = dep.cia) depr_dolar_corp,

		(select distinct trunc(dep.fecha)
		  from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dep
		 where dep.activo_fijo = am.cod_activo
		   and dep.mejora = am.cod_mejora
		   and to_char(trunc(dep.fecha), 'YYYYMM') = ld_periodo
		   and dep.referencia = kv_ref --depreciaciones no revaluacion
		   and am.cia = dep.cia) fech_depre_acti,

		nvl(cc.porcentaje,100) as pctj_cntr_cost, --100 o 0?

		--monto factura
		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			am.mon_fact_sin_igv
		else
			NULL
		end) as mon_fact_sin_igv,

		(case when ltrim(TO_CHAR(trunc(am.fech_activ), 'YYYYMM'), '0') = ld_periodo then
			am.mon_tota_fact
		else
			NULL
		end) as mon_tota_fact


		from STG_ACTIVO_FIJO.TBL_ACTI_MEJO     am  left join 
		STG_ACTIVO_FIJO.TBL_ACTIVO_CENTRO cc on ( am.cod_activo = cc.activo_fijo and am.cia = cc.cia ) left join 
		STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION dp on (am.cod_activo = dp.activo_fijo and am.cod_mejora = dp.mejora)

		where
		ltrim(TO_CHAR(trunc(am.fech_activ),'YYYYMM'),'0') =  ld_periodo
		--solo activos vigentes, se da prioridad a la baja contable o fiscal
		and to_date(to_char(nvl(trunc(am.fech_retir_cont),to_date('12/12/2050','dd/MM/YYYY')),'YYYYMM')  ,'YYYYMM')  >  to_date(ld_periodo, 'YYYYMM')

		and (dp.activo_fijo is null  and dp.mejora is null
		or cc.activo_fijo is null)

		--------  
		--and am.cod_activo = '52433'
		--and am.cod_mejora = '52433'
		;

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_registros_INS   	Number := 0;   
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);


		--limite de registros a tratar en bloque
		limite				Number := 1000;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--truncando la tabla
		STG_ACTIVO_FIJO.STPG_BIAF_UTIL_STG.BIAF_TRUNC_TBLN('TBL_FACT_ACTI',Pn_Erro,Pv_Mens); 

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
			Fetch D_DETALLE Bulk Collect Into d Limit limite;
			
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				--inserta
				insert into STG_ACTIVO_FIJO.TBL_FACT_ACTI (
				peri_mes_id,
				nro_ruc_cia,
				cod_activo,
				cod_mejora,
				cntr_csto,

				cod_sucu,

				cost_orig_local_cont,
				cost_orig_dolar_cont,
				cost_orig_local_corp,
				cost_orig_dolar_corp,

				cost_orig_baja_local_cont,
				cost_orig_baja_dolar_cont,
				cost_orig_baja_local_corp,
				cost_orig_baja_dolar_corp,



				--valo_resc_local_fiscal NUMBER(28,8),
				--valo_resc_dolar_fiscal NUMBER(28,8),
				--valo_resc_local_corp NUMBER(28,8),
				--valo_resc_dolar_corp NUMBER(28,8),

				depr_local_cont,
				depr_local_corp,
				depr_dolar_cont,
				depr_dolar_corp,


				fech_depre_acti,
				pctj_cntr_cost,
				mon_fact_sin_igv,
				mon_tota_fact,
				fech_ulti_actu

				)
				values (
				d(y).peri_mes_id,
				d(y).nro_ruc_cia,
				d(y).cod_activo,
				d(y).cod_mejora,
				d(y).cntr_csto,

				d(y).cod_sucu,

				d(y).cost_orig_local_cont,
				d(y).cost_orig_dolar_cont,
				d(y).cost_orig_local_corp,
				d(y).cost_orig_dolar_corp,

				d(y).cost_orig_baja_local_cont,
				d(y).cost_orig_baja_dolar_cont,
				d(y).cost_orig_baja_local_corp,
				d(y).cost_orig_baja_dolar_corp,

				d(y).depr_local_cont,
				d(y).depr_local_corp,
				d(y).depr_dolar_cont,
				d(y).depr_dolar_corp,


				d(y).fech_depre_acti,
				d(y).pctj_cntr_cost,
				d(y).mon_fact_sin_igv,
				d(y).mon_tota_fact,
				sysdate
				);
				
				--incrementando contadores:
				ln_registros_INS := ln_registros_INS + 1;  
				
			End Loop;    
		
			--commit cada 1000 reg
			Commit;

			Exit When D_DETALLE%NotFound;
		End Loop;

		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_TBLN_FACT_ACTI' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_TBLN_FACT_ACTI;
	
	
	--JM:
	
	/*********************************************************************************************************
  NOMBRE:      DMFU_MONT_BAJA_ACFI
  PROPOSITO:   Función que determina el Monto del activo en Estado de baja 
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi        --> Código de AF
  RETORNO:     Retorna variable number: 
               Si fecha_activacion es diferente a NULL y fecha_retiro es diferente a NULL, retorna indicador 'BAJA', por lo tanto
               retorna el monto original del activo dado de baja.
/*********************************************************************************************************/
 Function DMFU_MONT_BAJA_ACFI( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                               pn_flag_mont In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.ind_transfer%Type )
  Return Number As
    lm_monto_orig_baja  Number:=0;
    lm_orig_acti_sole_f Number:=0;
    lm_orig_acti_dola_f Number:=0;
    lm_orig_acti_sole_c Number:=0;
    lm_orig_acti_dola_c Number:=0;
    ld_fec_acti         Date:=Null;
    ld_fec_reti         Date:=Null;
   Begin
      Select trunc(am.fech_activ), trunc(am.fech_retir_cont), am.cost_orig_local_cont, am.cost_orig_dolar_cont, am.cost_orig_local_corp, am.cost_orig_dolar_corp
        Into ld_fec_acti, ld_fec_reti, lm_orig_acti_sole_f, lm_orig_acti_dola_f, lm_orig_acti_sole_c, lm_orig_acti_dola_c
        From STG_ACTIVO_FIJO.TBL_ACTI_MEJO am 
       Where am.cod_activo = pv_cod_acfi
         And am.cod_activo = am.cod_mejora;
       If ld_fec_reti Is Not Null Then  ---'BAJA';
          If pn_flag_mont = 1 Then 
                lm_orig_acti_sole_f:=lm_orig_acti_sole_f;
          ElsIf pn_flag_mont = 2 Then
                lm_orig_acti_dola_f:=lm_orig_acti_dola_f;
          ElsIf pn_flag_mont = 3 Then
                lm_orig_acti_sole_c:=lm_orig_acti_sole_c; 
          ElsIf pn_flag_mont = 4 Then
                lm_orig_acti_dola_c:=lm_orig_acti_dola_c;
          End If;
       End If;
     Return lm_monto_orig_baja;
 End DMFU_MONT_BAJA_ACFI; 

 
/*********************************************************************************************************
  NOMBRE:      DMFU_DEPR_REVA
  PROPOSITO:   Función que determina el monto de depreciacion de revaluacion del activo (Soles) fiscal
               (Depreciacion con revaluación) 
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
               pv_cod_mejo --> Codigo de Mejora 
               pd_fec_depr --> Fecha de la depreciacion
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciado revaluado en soles fiscal
/*********************************************************************************************************/
 Function DMFU_DEPR_REVA( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                          pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_mejora%Type,
                          pd_fec_depr In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.FECH_ACTIV%Type,
                          pv_tip_cont In STG_ACTIVO_FIJO.tbl_hist_depreciacion_reval.contabilidad%Type,
                          pv_tip_mone In DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.mone_fact%Type )
  Return Number As
    ln_soles Number:=0;
    ln_dolares Number:=0;
  Begin               
      Select am.depr_reval_loc, am.depr_reval_dol 
        Into ln_soles, ln_dolares
        From STG_ACTIVO_FIJO.tbl_hist_depreciacion_reval am 
       Where am.activo_fijo = pv_cod_acfi
         And am.mejora      = pv_cod_mejo
         And to_char(trunc(am.fecha),'mm/yyyy')= to_char(pd_fec_depr,'mm/yyyy')
         And am.contabilidad= pv_tip_cont;
         
         If ln_soles is null Then
            ln_soles := 0;
         ElsIf ln_dolares is null Then 
            ln_dolares:=0;
         End If;
         
         If pv_tip_mone = 'S/.' Then
            Return ln_soles; 
         Else
            Return ln_dolares;
         End If;   
         
      Exception       
        When Others Then
         If pv_tip_mone = 'S/.' Then
            Return ln_soles; 
         Else
            Return ln_dolares;
         End If;
 End DMFU_DEPR_REVA; 
 
 
/*********************************************************************************************************
  NOMBRE:      DMFU_DEPR_REVA_BAJA
  PROPOSITO:   Función que determina el monto de depreciacion de revaluacion del activo en baja
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
               pv_cod_mejo --> Codigo de Mejora 
               pd_fec_depr --> Fecha de la depreciacion
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciado revaluado en soles fiscal
/*********************************************************************************************************/
 Function DMFU_DEPR_REVA_BAJA( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.activo_fijo%Type,
                               pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.mejora%Type,
                               pd_fec_depr In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.fecha_dep_ini%Type,
                               pd_fec_reti In STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA.fecha_retiro%Type )
  Return Number As
    ln_soles_f Number:=0;
  Begin
    If pd_fec_reti != Null Then
       Begin
        Select am.depr_reval_loc
          Into ln_soles_f
          From STG_ACTIVO_FIJO.tbl_hist_depreciacion_reval am 
         Where am.activo_fijo = pv_cod_acfi
           And am.mejora      = pv_cod_mejo
           And trunc(am.fecha)= pd_fec_depr
           And am.contabilidad= 'F';
           If ln_soles_f is null or ln_soles_f  = 0 Then
              ln_soles_f  := 0;
           End If;
         Return ln_soles_f  ; 
       Exception       
          When Others Then
             Return ln_soles_f  ; 
       End;
    Else
      Return ln_soles_f  ; 
    End If;
 End DMFU_DEPR_REVA_BAJA;  


/*********************************************************************************************************
  NOMBRE:      DMFU_DEPR_TOTA_REVA_SOLF
  PROPOSITO:   Función que determina el monto de depreciacion total de revaluacion del activo (Soles) fiscal
               (Historico de Revaluacion Total de la Depreciacion) 
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
               pv_cod_mejo --> Codigo de Mejora 
  RETORNO:     Retorna variable Number: 
               el monto de depreciación total de revaluación en soles del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciacion total de revaluacion en soles fiscal 
/*********************************************************************************************************/
 Function DMFU_DEPR_TOTA_REVA_SOLF( pv_cod_acfi In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_activo%Type,
                                    pv_cod_mejo In STG_ACTIVO_FIJO.TBL_ACTI_MEJO.cod_mejora%Type )
  Return Number As
    ln_soles_f Number:=0;
  Begin               
      Select Sum(am.depr_local)
        Into ln_soles_f
        From STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION am 
       Where am.activo_fijo = pv_cod_acfi
         And am.mejora      = pv_cod_mejo
         And am.referencia  = 'R'
         And am.contabilidad= 'F';
        If ln_soles_f is null or ln_soles_f = 0 Then
            ln_soles_f := 0;
         End If;
       Return ln_soles_f ; 
      Exception       
        When Others Then
           Return ln_soles_f ;
 End DMFU_DEPR_TOTA_REVA_SOLF;


/*********************************************************************************************************
  NOMBRE:      DMPR_POBL_TBLN_REVA
  PROPOSITO:   Procedimiento para poblar el tablón de Revaluación
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_POBL_TBLN_REVA( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
  
  Cursor C_TBLON_ACTI_REVA Is
        Select to_char(trunc(a.fecha), 'YYYYMM') as peri_mes_id,  
               ( select a.nro_ruc_cia From STG_ACTIVO_FIJO.tbl_cia a Where a.nom_cjto = b.cia ) as nro_ruc_cia,  
               a.activo_fijo   as cod_activo, 
               a.mejora        as cod_mejora, 
               
               decode( c.porcentaje, 100, a.reval_valor * 1, a.reval_valor * nvl(c.porcentaje,1) )    as cost_reval_local_cont, 
               0               as cost_reval_dolar_cont, --- No existe el dato REVAL_VALOR_DOLAR_FISCAL en TBL_HIST_REVALUACION   
               
               decode( c.porcentaje, 100, a.reval_valor_c * 1, a.reval_valor_c * nvl(c.porcentaje,1) ) as cost_reval_local_corp,
               0               as cost_reval_dolar_corp, --- No existe el dato REVAL_VALOR_DOLAR_CORP en TBL_HIST_REVALUACION

               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_MONT_BAJA_ACFI( 
                     a.activo_fijo, 1 
                     )         as cost_retir_reval,
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_REVA( 
                     a.activo_fijo,
                     a.mejora, 
                     a.fecha,
                     'F',
                     'S/.' )   as  depr_reval_local_cont, 
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_REVA( 
                     a.activo_fijo,
                     a.mejora, 
                     a.fecha,
                     'F',
                     'US$' )   as  depr_reval_dolar_cont, 
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_REVA( 
                     a.activo_fijo,
                     a.mejora, 
                     a.fecha,
                     'C',
                     'S/.' )   as  depr_reval_local_corp, 
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_REVA( 
                     a.activo_fijo,
                     a.mejora, 
                     a.fecha,
                     'C',
                     'US$' )   as  depr_reval_dolar_corp,                     
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_REVA_BAJA( 
                     a.activo_fijo,
                     a.mejora, 
                     a.fecha,
                     b.fecha_retiro ) as depr_reval_retiro,
               a.fecha           as fech_depre_reval, 
               DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_DEPR_TOTA_REVA_SOLF( 
                     a.activo_fijo,
                     a.mejora )  as depr_tota_reval,                                                                                       
               decode(substr(c.centro_costo,1,2), null,'00',substr(c.centro_costo,1,2)) as cod_sucu,
               decode(c.centro_costo, null,'00.00.00',c.centro_costo)                   as cntr_csto
    from STG_ACTIVO_FIJO.TBL_HIST_REVALUACION a, STG_ACTIVO_FIJO.TBL_ACTIVO_MEJORA  b, STG_ACTIVO_FIJO.TBL_ACTIVO_CENTRO c  
    where a.activo_fijo = b.activo_fijo
    and a.mejora = b.mejora       
    and a.activo_fijo = c.activo_fijo(+) 
    and a.activo_fijo = a.mejora;

   Type TBL_C_TBLON_ACTI_REVA Is Table Of C_TBLON_ACTI_REVA%RowType;
    d TBL_C_TBLON_ACTI_REVA;      
   
   nTotReg_DPO        Number := 0;
 	 ln_registros_INS   Number := 0; 
    
   v_Fecha_sys        Varchar2(10);
   v_Hora_sys         Varchar2(10); 

  
Begin
  
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA TABLON REVALUACION');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  Execute Immediate 'Truncate Table STG_ACTIVO_FIJO.TBL_FACT_REVAL' ;
  
  Open C_TBLON_ACTI_REVA;
  Loop 
    Fetch C_TBLON_ACTI_REVA Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop
        
          nTotReg_DPO := nTotReg_DPO + 1;
          Begin
              Insert Into STG_ACTIVO_FIJO.TBL_FACT_REVAL (
                        peri_mes_id,  
                        nro_ruc_cia,
                        cod_activo,
                        cod_mejora,
                        cost_reval_local_cont ,
                        cost_reval_dolar_cont ,
                        cost_reval_local_corp ,
                        cost_reval_dolar_corp ,
                        cost_retir_reval, 
                        depr_reval_local_cont , 
                        depr_reval_dolar_cont , 
                        depr_reval_local_corp , 
                        depr_reval_dolar_corp ,                     
                        depr_reval_retiro ,    
                        fech_depre_reval, 
                        depr_tota_reval,                                                                                       
                        cod_sucu,
                        cntr_csto,
                        FECH_ULTI_ACTU             
              ) Values (
                        d(y).peri_mes_id,  
                        d(y).nro_ruc_cia,
                        d(y).cod_activo,
                        d(y).cod_mejora,
                        d(y).cost_reval_local_cont ,
                        d(y).cost_reval_dolar_cont ,
                        d(y).cost_reval_local_corp ,
                        d(y).cost_reval_dolar_corp ,
                        d(y).cost_retir_reval, 
                        d(y).depr_reval_local_cont , 
                        d(y).depr_reval_dolar_cont , 
                        d(y).depr_reval_local_corp , 
                        d(y).depr_reval_dolar_corp ,                     
                        d(y).depr_reval_retiro ,
                        d(y).fech_depre_reval, 
                        d(y).depr_tota_reval,          
                        d(y).cod_sucu,
                        d(y).cntr_csto,
                        sysdate
               );
          
           ln_registros_INS := ln_registros_INS + 1;
           pn_Erro:=0;          
        Exception
          When Others then
            dbms_output.put_line('Error Insert STG_ACTIVO_FIJO.TBL_FACT_REVAL: '||sqlerrm||' - '||sqlcode||' Activo-Mejora: '||d(y).cod_activo||'-'||d(y).cod_mejora || ' Periodo: '||d(y).peri_mes_id);
            pn_Erro:=1;
        End;  
    End Loop;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_ACTI_REVA%NotFound;
  End Loop;
  Close C_TBLON_ACTI_REVA;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales  : '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Inser : '||ln_registros_INS);    

End DMPR_POBL_TBLN_REVA;  


/*********************************************************************************************************
  NOMBRE:      DMPR_POBL_TBLN_MAYR
  PROPOSITO:   Procedimiento para poblar el tablón del Mayor
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_POBL_TBLN_MAYR( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  Cursor C_TBLON_ACTI_MAYOR Is              
   --- Las cuentas 33.3.1.1.099 y 33.8% corresponden a Transferencias y Unidades por Recibir
   --- Las cuentas 33.9% corresponden a Obras en Curso 
   --- La cuenta 34.9.1.1.999 corresponde a Proyectos
   Select x.asiento            ,
          x.consecutivo        ,
          x.centro_costo       ,
          x.cuenta_contable    ,
          x.fecha              ,
          x.tipo_asiento       ,
          x.fuente             ,
          x.referencia         ,
          x.origen             ,
          x.debito_local       ,
          x.credito_local      ,
          x.debito_dolar       ,
          x.credito_dolar      ,
          x.contabilidad       ,
          x.clase_asiento      ,
          x.estado_consolida   ,
          x.asiento_consolida  ,
          x.debito_unidades    ,
          x.credito_unidades   ,
          x.estado_cons_fisc   ,
          x.asnt_cons_fisc     ,
          x.estado_cons_corp   ,
          x.asnt_cons_corp     ,
          x.nit                ,
          x.cargado_obra_curso ,
          x.tipo_cambio        ,
          x.base_local         ,
          x.base_dolar         ,
          x.proyecto           ,
          x.fase               ,
          x.documento_global   ,
          x.u_flujo_efectivo   ,
          x.u_patrimonio_neto  ,
          x.cia
     From (
       Select asiento            ,
              consecutivo        ,
              centro_costo       ,
              cuenta_contable    ,
              fecha              ,
              tipo_asiento       ,
              fuente             ,
              referencia         ,
              origen             ,
              debito_local       ,
              credito_local      ,
              debito_dolar       ,
              credito_dolar      ,
              contabilidad       ,
              clase_asiento      ,
              estado_consolida   ,
              asiento_consolida  ,
              debito_unidades    ,
              credito_unidades   ,
              estado_cons_fisc   ,
              asnt_cons_fisc     ,
              estado_cons_corp   ,
              asnt_cons_corp     ,
              nit                ,
              cargado_obra_curso ,
              tipo_cambio        ,
              base_local         ,
              base_dolar         ,
              proyecto           ,
              fase               ,
              documento_global   ,
              u_flujo_efectivo   ,
              u_patrimonio_neto  ,
              'ADMIN' as cia
         From admin.mayor@exactus a
     Union all
       Select asiento            ,
              consecutivo        ,
              centro_costo       ,
              cuenta_contable    ,
              fecha              ,
              tipo_asiento       ,
              fuente             ,
              referencia         ,
              origen             ,
              debito_local       ,
              credito_local      ,
              debito_dolar       ,
              credito_dolar      ,
              contabilidad       ,
              clase_asiento      ,
              '' as estado_consolida   ,
              '' as asiento_consolida  ,
              debito_unidades    ,
              credito_unidades   ,
              estado_cons_fisc   ,
              asnt_cons_fisc     ,
              estado_cons_corp   ,
              asnt_cons_corp     ,
              nit                ,
              cargado_obra_curso ,
              tipo_cambio        ,
              base_local         ,
              base_dolar         ,
              proyecto           ,
              fase               ,
              documento_global   ,
              u_flujo_efectivo   ,
              u_patrimonio_neto  ,
              'SMART' as cia
         From smart.mayor@exactus a ) x
    Where x.origen in ( 'CP', 'AF' )
      And (  x.cuenta_contable = '33.3.1.1.099' 
          or x.cuenta_contable like '33.8%' 
          or x.cuenta_contable like '33.9%' 
          or x.cuenta_contable = '34.9.1.1.999'
           );

   Type TBL_C_TBLON_ACTI_MAYOR Is Table Of C_TBLON_ACTI_MAYOR%RowType;
    d TBL_C_TBLON_ACTI_MAYOR;  

   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 

   v_Fecha_sys        Varchar2(10);
   v_Hora_sys         Varchar2(10); 

Begin

  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA TABLON MAYOR');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  Execute Immediate 'Truncate Table STG_ACTIVO_FIJO.TBL_MAYOR' ; 
  
  Open C_TBLON_ACTI_MAYOR;
  Loop 
    Fetch C_TBLON_ACTI_MAYOR Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop        
        nTotReg_DPO := nTotReg_DPO + 1;   
        Begin
        Insert Into STG_ACTIVO_FIJO.TBL_MAYOR (
                   asiento            ,
                   consecutivo        ,
                   centro_costo       ,
                   cuenta_contable    ,
                   fecha              ,
                   tipo_asiento       ,
                   fuente             ,
                   referencia         ,
                   origen             ,
                   debito_local       ,
                   credito_local      ,
                   debito_dolar       ,
                   credito_dolar      ,
                   contabilidad       ,
                   clase_asiento      ,
                   estado_consolida   ,
                   asiento_consolida  ,
                   debito_unidades    ,
                   credito_unidades   ,
                   estado_cons_fisc   ,
                   asnt_cons_fisc     ,
                   estado_cons_corp   ,
                   asnt_cons_corp     ,
                   nit                ,
                   cargado_obra_curso ,
                   tipo_cambio        ,
                   base_local         ,
                   base_dolar         ,
                   proyecto           ,
                   fase               ,
                   documento_global   ,
                   u_flujo_efectivo   ,
                   u_patrimonio_neto  ,
                   cia
        ) Values (
                  d(y).asiento            ,
                  d(y).consecutivo        ,
                  d(y).centro_costo       ,
                  d(y).cuenta_contable    ,
                  d(y).fecha              ,
                  d(y).tipo_asiento       ,
                  d(y).fuente             ,
                  d(y).referencia         ,
                  d(y).origen             ,
                  d(y).debito_local       ,
                  d(y).credito_local      ,
                  d(y).debito_dolar       ,
                  d(y).credito_dolar      ,
                  d(y).contabilidad       ,
                  d(y).clase_asiento      ,
                  d(y).estado_consolida   ,
                  d(y).asiento_consolida  ,
                  d(y).debito_unidades    ,
                  d(y).credito_unidades   ,
                  d(y).estado_cons_fisc   ,
                  d(y).asnt_cons_fisc     ,
                  d(y).estado_cons_corp   ,
                  d(y).asnt_cons_corp     ,
                  d(y).nit                ,
                  d(y).cargado_obra_curso ,
                  d(y).tipo_cambio        ,
                  d(y).base_local         ,
                  d(y).base_dolar         ,
                  d(y).proyecto           ,
                  d(y).fase               ,
                  d(y).documento_global   ,
                  d(y).u_flujo_efectivo   ,
                  d(y).u_patrimonio_neto  ,
                  d(y).cia
         );                                
         
         ln_registros_INS := ln_registros_INS + 1; 
         pn_Erro:=0;
         Exception
          When Others then
            dbms_output.put_line('Error Insert STG_ACTIVO_FIJO.TBL_MAYOR: '||sqlerrm||' - '||sqlcode||' Asiento: '||d(y).asiento || ' Consecutivo: '||d(y).consecutivo);
            pn_Erro:=1;
        End;   
    End Loop;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_ACTI_MAYOR%NotFound;
  End Loop;
  Close C_TBLON_ACTI_MAYOR;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales: '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Inser  : '||ln_registros_INS);

End DMPR_POBL_TBLN_MAYR; 


/*********************************************************************************************************
  NOMBRE:      DMPR_POBL_TBLN_ASNT
  PROPOSITO:   Procedimiento para poblar el tablón asiento contable
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  -
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_POBL_TBLN_ASNT( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  Cursor C_TBLON_ASIENTO_MY Is              
   --- Las cuentas 33.3.1.1.099 y 33.8% corresponden a Transferencias y Unidades por Recibir
   --- Las cuentas 33.9% corresponden a Obras en Curso 
   --- La cuenta 34.9.1.1.999 corresponde a Proyectos
   Select x.asiento            ,
          x.consecutivo        ,
          x.centro_costo       ,
          x.cuenta_contable    ,
          x.fecha              ,
          x.tipo_asiento       ,
          x.fuente             ,
          x.referencia         ,
          x.origen             ,
          x.debito_local       ,
          x.credito_local      ,
          x.debito_dolar       ,
          x.credito_dolar      ,
          x.contabilidad       ,
          x.clase_asiento      ,
          x.estado_consolida   ,
          x.asiento_consolida  ,
          x.debito_unidades    ,
          x.credito_unidades   ,
          x.estado_cons_fisc   ,
          x.asnt_cons_fisc     ,
          x.estado_cons_corp   ,
          x.asnt_cons_corp     ,
          x.nit                ,
          x.cargado_obra_curso ,
          x.tipo_cambio        ,
          x.base_local         ,
          x.base_dolar         ,
          x.proyecto           ,
          x.fase               ,
          x.documento_global   ,
          x.u_flujo_efectivo   ,
          x.u_patrimonio_neto  ,
          x.cia
     From (
       Select asiento            ,
              consecutivo        ,
              centro_costo       ,
              cuenta_contable    ,
              fecha              ,
              tipo_asiento       ,
              fuente             ,
              referencia         ,
              origen             ,
              debito_local       ,
              credito_local      ,
              debito_dolar       ,
              credito_dolar      ,
              contabilidad       ,
              clase_asiento      ,
              estado_consolida   ,
              asiento_consolida  ,
              debito_unidades    ,
              credito_unidades   ,
              estado_cons_fisc   ,
              asnt_cons_fisc     ,
              estado_cons_corp   ,
              asnt_cons_corp     ,
              nit                ,
              cargado_obra_curso ,
              tipo_cambio        ,
              base_local         ,
              base_dolar         ,
              proyecto           ,
              fase               ,
              documento_global   ,
              u_flujo_efectivo   ,
              u_patrimonio_neto  ,
              'ADMIN' as cia
         From admin.mayor@exactus a
     Union all
       Select asiento            ,
              consecutivo        ,
              centro_costo       ,
              cuenta_contable    ,
              fecha              ,
              tipo_asiento       ,
              fuente             ,
              referencia         ,
              origen             ,
              debito_local       ,
              credito_local      ,
              debito_dolar       ,
              credito_dolar      ,
              contabilidad       ,
              clase_asiento      ,
              '' as estado_consolida   ,
              '' as asiento_consolida  ,
              debito_unidades    ,
              credito_unidades   ,
              estado_cons_fisc   ,
              asnt_cons_fisc     ,
              estado_cons_corp   ,
              asnt_cons_corp     ,
              nit                ,
              cargado_obra_curso ,
              tipo_cambio        ,
              base_local         ,
              base_dolar         ,
              proyecto           ,
              fase               ,
              documento_global   ,
              u_flujo_efectivo   ,
              u_patrimonio_neto  ,
              'SMART' as cia
         From smart.mayor@exactus a ) x
    Where x.origen in ( 'CP', 'AF' )
      And (  x.cuenta_contable = '33.3.1.1.099' 
          or x.cuenta_contable like '33.8%' 
          or x.cuenta_contable like '33.9%' 
          or x.cuenta_contable = '34.9.1.1.999'
           );

   Type TBL_C_TBLON_ASIENTO_MY Is Table Of C_TBLON_ASIENTO_MY%RowType;
    d TBL_C_TBLON_ASIENTO_MY;  

   nTotReg_DPO        Number := 0;
   ln_registros_INS  Number := 0; 

   v_Fecha_sys        Varchar2(10);
   v_Hora_sys         Varchar2(10); 

Begin
  
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA TABLON ASIENTO MAYOR');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  Execute Immediate 'Truncate Table STG_ACTIVO_FIJO.TBL_ASNTO_CNTBL' ; 
  
  Open C_TBLON_ASIENTO_MY;
  Loop 
    Fetch C_TBLON_ASIENTO_MY Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop        
          nTotReg_DPO := nTotReg_DPO + 1;
          Begin
          Insert Into STG_ACTIVO_FIJO.TBL_ASNTO_CNTBL
          (
            asnto_cont, fech_asnto, tipo_asnto, clase_asnto, cont_asnto, fnte_asnto, orgn_asnto, fech_ulti_actu  
          )
          Values
          (
           d(y).asiento, d(y).fecha, d(y).tipo_asiento, d(y).clase_asiento, d(y).contabilidad, d(y).fuente, d(y).origen, sysdate 
          );         
         
         ln_registros_INS := ln_registros_INS + 1;  
         pn_Erro:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert STG_ACTIVO_FIJO.TBL_ASNTO_CNTBL: '||sqlerrm||' - '||sqlcode||' Asiento: '||d(y).asiento || ' Periodo: '||d(y).fecha);
            pn_Erro:=1;
        End;  
    End Loop;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_ASIENTO_MY%NotFound;
  End Loop;
  Close C_TBLON_ASIENTO_MY;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales: '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Inser  : '||ln_registros_INS);

End DMPR_POBL_TBLN_ASNT;

/*********************************************************************************************************
  NOMBRE:      DMPR_POBL_TBLN_EEFF
  PROPOSITO:   Procedimiento para poblar el tablón estado financiero
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_POBL_TBLN_EEFF( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
-- ld_desde           Date;
-- ld_hasta           Date; 
 
 Cursor C_TBLON_ACTI_ESTD_FINA Is              
   --- Las cuentas 33.3.1.1.099 y 33.8% corresponden a Unidades por Recibir
   --- Las cuentas 33.9% corresponden a Obras en Curso 
   --- La cuenta 34.9.1.1.999 corresponde a Proyectos       
       Select distinct y.cnta_cont, y.codi_fam, y.peri_mes_id, y.nro_ruc_cia, y.cost_adicn, y.cost_transfer, y.asiento
       From (
        Select a.cuenta_contable as cnta_cont, 
              decode(a.cuenta_contable,'33.3.1.1.099','UNIDADES POR RECIBIR',
                                       '34.9.1.1.999','PROYECTOS') as codi_fam, 
              to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
              (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
              decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn, 
              0 as cost_transfer, 
              a.fecha, 
              a.asiento
         From STG_ACTIVO_FIJO.TBL_MAYOR a
        Where a.origen = 'CP'
          And (a.cuenta_contable = '33.3.1.1.099' or a.cuenta_contable = '34.9.1.1.999')
        UNION ALL
        Select a.cuenta_contable as cnta_cont, 
                      'UNIDADES POR RECIBIR' as codi_fam, 
                      to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
                      (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
                      decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn, 
                      0 as cost_transfer, 
                      a.fecha, 
                      a.asiento
                 From STG_ACTIVO_FIJO.TBL_MAYOR a
                Where a.origen = 'CP'
                  And (a.cuenta_contable like '33.8%')
        UNION ALL
        Select a.cuenta_contable as cnta_cont, 
                      'OBRAS EN CURSO' as codi_fam, 
                      to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
                      (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
                      decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn,          
                      0 as cost_transfer, 
                      a.fecha, 
                      a.asiento
                 From STG_ACTIVO_FIJO.TBL_MAYOR a
                Where a.origen = 'CP'
                  And (a.cuenta_contable like '33.9%') ) y;
--       Where y.fecha Between ld_desde And ld_hasta;

   Type TBL_C_TBLON_ACTI_ESTD_FINA Is Table Of C_TBLON_ACTI_ESTD_FINA%RowType;
    d TBL_C_TBLON_ACTI_ESTD_FINA;  

   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_registros_INS2  Number := 0;
   ln_registros_UPD   Number := 0;

   v_Fecha_sys        Varchar2(10);
   v_Hora_sys         Varchar2(10);    
  
Begin
  --inicializando el periodo
--  ld_desde := to_date('01'||'/'||substr(pv_per_mens,5,2)||'/'||substr(pv_per_mens,1,4));
--  ld_hasta := last_day(ld_desde);    
    
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA TABLON ESTD FINANCIEROS');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  Execute Immediate 'Truncate Table STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA' ;
  
  Open C_TBLON_ACTI_ESTD_FINA;
  Loop 
    Fetch C_TBLON_ACTI_ESTD_FINA Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop        
        nTotReg_DPO := nTotReg_DPO + 1;  
        Begin
        Insert Into STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA (
                cnta_cont,  
                codi_fam,
                peri_mes_id,
                nro_ruc_cia,
                cost_adicn,
                cost_transfer,
                asnto_cont,
                fech_ulti_actu  
          ) Values (
                d(y).cnta_cont,  
                d(y).codi_fam,
                d(y).peri_mes_id,
                d(y).nro_ruc_cia,
                d(y).cost_adicn,
                d(y).cost_transfer,
                d(y).asiento,
                sysdate
          ); 
        ln_registros_INS := ln_registros_INS + 1; 
        pn_Erro:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA: '||sqlerrm||' - '||sqlcode||' CtaCtble: '||d(y).cnta_cont || ' Familia: '||d(y).codi_fam);
            pn_Erro:=1;
        End;   
    End Loop;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_ACTI_ESTD_FINA%NotFound;
  End Loop;
  Close C_TBLON_ACTI_ESTD_FINA;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales: '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Ins  : '||ln_registros_INS);

End DMPR_POBL_TBLN_EEFF;


/*********************************************************************************************************
  NOMBRE:      DMPR_POBL_TBLN_EEFF_ACTI
  PROPOSITO:   Procedimiento para poblar el tablón estado financiero activos
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_POBL_TBLN_EEFF_ACTI( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is      

  Cursor C_TBLON_ACTI_ESTD_FINA Is              
   --- Las cuentas 33.3.1.1.099 y 33.8% corresponden a Unidades por Recibir
   --- Las cuentas 33.9% corresponden a Obras en Curso 
   --- La cuenta 34.9.1.1.999 corresponde a Proyectos       
       Select distinct y.cnta_cont, y.codi_fam, y.peri_mes_id, y.nro_ruc_cia, y.cost_adicn, y.cost_transfer, y.asiento
       From (
        Select a.cuenta_contable as cnta_cont, 
              decode(a.cuenta_contable,'33.3.1.1.099','UNIDADES POR RECIBIR',
                                       '34.9.1.1.999','PROYECTOS') as codi_fam, 
              to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
              (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
              decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn, 
              0 as cost_transfer, 
              a.fecha, 
              a.asiento
         From STG_ACTIVO_FIJO.TBL_MAYOR a
        Where a.origen = 'CP'
          And (a.cuenta_contable = '33.3.1.1.099' or a.cuenta_contable = '34.9.1.1.999')
        UNION ALL
        Select a.cuenta_contable as cnta_cont, 
                      'UNIDADES POR RECIBIR' as codi_fam, 
                      to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
                      (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
                      decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn, 
                      0 as cost_transfer, 
                      a.fecha, 
                      a.asiento
                 From STG_ACTIVO_FIJO.TBL_MAYOR a
                Where a.origen = 'CP'
                  And (a.cuenta_contable like '33.8%')
        UNION ALL
        Select a.cuenta_contable as cnta_cont, 
                      'OBRAS EN CURSO' as codi_fam, 
                      to_char(a.fecha,'yyyy')||to_char(a.fecha,'mm') as peri_mes_id,
                      (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
                      decode(substr(a.asiento,1,2),'CP',a.debito_local,a.credito_local) as cost_adicn,          
                      0 as cost_transfer, 
                      a.fecha, 
                      a.asiento
                 From STG_ACTIVO_FIJO.TBL_MAYOR a
                Where a.origen = 'CP'
                  And (a.cuenta_contable like '33.9%') ) y;

   Type TBL_C_TBLON_ACTI_ESTD_FINA Is Table Of C_TBLON_ACTI_ESTD_FINA%RowType;
    d TBL_C_TBLON_ACTI_ESTD_FINA;  

   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_registros_INS2  Number := 0;
   ln_registros_UPD   Number := 0; 
   ln_cant_reg        Number := 0; 

   v_Fecha_sys        Varchar2(10);
   v_Hora_sys         Varchar2(10);   
  
Begin
    
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON EEFF AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA TABLON ESTD FINAN -AF');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  Open C_TBLON_ACTI_ESTD_FINA;
  Loop 
    Fetch C_TBLON_ACTI_ESTD_FINA Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop        
        nTotReg_DPO := nTotReg_DPO + 1;  
        
         For c_cc In ( Select (Select cuenta_contable From STG_ACTIVO_FIJO.TBL_MAYOR x Where x.asiento =  a.asnt_myor) as cnta_cont, 
                               familia as codi_fam,
                               to_char(a.fech_activ,'yyyy')||to_char(a.fech_activ,'mm')   as peri_mes_id,  
                               (select z.nro_ruc_cia from STG_ACTIVO_FIJO.tbl_cia z where z.nom_cjto = a.cia) as nro_ruc_cia, 
                               0                      as cost_adicn,
                               a.cost_orig_local_cont as cost_transfer, 
                               a.fech_activ as fecha,
                               a.asnt_myor
                         From STG_ACTIVO_FIJO.tbl_acti_mejo a
                        Where a.asnt_myor = d(y).asiento
                          and a.familia = d(y).codi_fam                          
                       ) Loop
                
                 Select count(cnta_cont) 
                 Into ln_cant_reg
                 from STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA fe
                  Where fe.cnta_cont  = d(y).cnta_cont
                    and fe.asnto_cont = d(y).asiento
                    and fe.codi_fam   = d(y).codi_fam ;
                 
                 If ln_cant_reg = 0 Then        
                   Begin
                    Insert Into STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA (
                              cnta_cont,  
                              codi_fam,
                              peri_mes_id,
                              nro_ruc_cia,
                              cost_adicn,
                              cost_transfer,
                              asnto_cont,
                              fech_ulti_actu  
                        ) Values (
                              c_cc.cnta_cont,  
                              c_cc.codi_fam,
                              c_cc.peri_mes_id,
                              c_cc.nro_ruc_cia,
                              c_cc.cost_adicn,
                              c_cc.cost_transfer,
                              c_cc.asnt_myor,
                              sysdate
                        ); 
                     ln_registros_INS := ln_registros_INS + 1;                
                     pn_Erro:=0;
                  Exception
                    When Others then
                      dbms_output.put_line('Error Insert STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA: '||sqlerrm||' - '||sqlcode||' Cnta.Contable: '||c_cc.cnta_cont||' Familia: '||c_cc.codi_fam||' Asiento: '||c_cc.asnt_myor);
                      pn_Erro:=1;
                  End;  
                Else
                   Begin
                    Update STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA 
                       Set cost_transfer = c_cc.cost_transfer,
                           fech_ulti_actu = sysdate
                     Where cnta_cont = c_cc.cnta_cont
                       And codi_fam  = c_cc.codi_fam
                       And asnto_cont= c_cc.asnt_myor;                    
                         
                     ln_registros_UPD := ln_registros_UPD + 1;  
                     pn_Erro:=0;
                  Exception
                    When Others then
                      dbms_output.put_line('Error Update STG_ACTIVO_FIJO.TBL_FACT_ESTD_FINA: '||sqlerrm||' - '||sqlcode||' Cnta.Contable: '||c_cc.cnta_cont||' Familia: '||c_cc.codi_fam||' Asiento: '||c_cc.asnt_myor);
                      pn_Erro:=1;
                  End;  
                End If;
         End Loop;              
    End Loop;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_ACTI_ESTD_FINA%NotFound;
  End Loop;
  Close C_TBLON_ACTI_ESTD_FINA;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales: '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Ins  : '||ln_registros_INS);
  dbms_output.put_line('TOT.REG. UPD  : '||ln_registros_UPD);

End DMPR_POBL_TBLN_EEFF_ACTI;

	
	
	
	
	---#####################  Carga Dimenciones Datamart    ########################
		
	/**
		->>- @param IN:
			-pd_fech_ini : fecha inicial, ejemplo '01/01/1985'
			-pd_fech_fin : fecha final, ejemplo '30/12/2050'
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion tiempo           ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_DIM_TIEMPO(
				pd_fech_ini      In  Varchar2,
				pd_fech_fin      In  Varchar2,
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		

		--declarando variables auxiliares:
		v_Fecha_sys        	Varchar2(10);
		v_Hora_sys         	Varchar2(10);
		contadorAnio Number(6) := 0;
		contadorMes Number(6) := 0;
		Pd_fecha_inicio date;
		Pd_fecha_fin date;
		Pn_existe Number(1) := 0;
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--inicializacion de fechas:
		Pd_fecha_inicio := to_date(pd_fech_ini,'dd/MM/yyyy');
		Pd_fecha_fin := to_date(pd_fech_fin,'dd/MM/yyyy');
	
		--iterando sobre los años
		FOR contadorAnio IN  to_number(TO_CHAR(Pd_fecha_inicio,'YYYY')) .. to_number(TO_CHAR(Pd_fecha_fin,'YYYY'))
		LOOP 
	  
			--iterando sobre los meses
			FOR contadorMes IN  to_number(extract(month from Pd_fecha_inicio)) .. to_number(extract(month from Pd_fecha_fin))
			LOOP
			
				select count(1)  
				into Pn_existe  
				from DM_ACTIVO_FIJO.BIAF_TMPO_PERI where  peri_mes_id = contadorAnio || contadorMes;
				
				if  Pn_existe = 0 then
					
					insert into DM_ACTIVO_FIJO.BIAF_TMPO_PERI(
					peri_mes_id,
					anio,
					trime,
					quarter,
					mes_nro,
					mes_letra,
					fech_ulti_actu
					) 
					select 
						contadorAnio || 
						(
						case when contadorMes < 10 then 
							'0'||contadorMes
						else
							to_char(contadorMes)
						end  
						)
						as periodo_mes_id,
						contadorAnio as anio,
						ceil(to_number(contadorMes/3)) AS trime,
						ceil(to_number(contadorMes/4)) AS quarter, 
						contadorMes as mes_nro,
						trim(to_char(to_date(contadorMes||'/'||contadorAnio,'MM/yyyy'), 'MONTH')) as mes_letra,
						--TO_CHAR(SYSDATE-contador,'W') as nro_sema_mes,
						--to_char(sysdate-contador, 'DD') as nro_dia_mes,
						--to_char(sysdate-contador, 'DAY') as dia_letr, 
						sysdate
					from  dual;
					
				else
					update DM_ACTIVO_FIJO.BIAF_TMPO_PERI set
					anio = contadorAnio,
					trime = ceil(to_number(contadorMes/3)),
					quarter = ceil(to_number(contadorMes/4)),
					mes_nro = contadorMes,
					mes_letra = to_char(to_date(contadorMes||'/'||contadorAnio,'MM/yyyy'), 'MONTH'),
					fech_ulti_actu = sysdate
					where 
					peri_mes_id = contadorAnio||contadorMes;
				end if;
				
				commit;
				
			END LOOP;
		END LOOP;
		
		DBMS_OUTPUT.PUT_LINE('################  *-FIN-*');  
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_TIEMPO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_TIEMPO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion compañia         ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_DIM_CIA(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select  raz_soci_cia,
                nro_ruc_cia,
				nom_cjto
		from STG_ACTIVO_FIJO.TBL_CIA; 

		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_total_registros        	Number := 0;
		ln_registros_UPD	    	Number := 0;
		ln_registros_INS   			Number := 0;   
		ln_existe             		Number := 0;
		v_Fecha_sys        			Varchar2(10);
		v_Hora_sys         			Varchar2(10);
		pn_cid 						Number(28) := 0;

		--limite de registros a tratar en bloque
		limite				Number := 1000;
		
		
		
				
	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;

			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
				
				select count(nom_cjto) 
					into ln_existe 
				from DM_ACTIVO_FIJO.BIAF_CIA
				where nom_cjto = d(y).nom_cjto;
				
				
				if ln_existe > 0 then
					--actualiza
					update DM_ACTIVO_FIJO.BIAF_CIA set
					raz_soci_cia = d(y).raz_soci_cia,
					nro_ruc_cia = d(y).nro_ruc_cia,
					fech_ulti_actu = sysdate
					where 
					nom_cjto = d(y).nom_cjto;
					
					ln_registros_UPD := ln_registros_UPD + 1;
				else
					--inserta
					--max id
					select nvl(max(cia_id)+1,1) 
					into pn_cid  
					from DM_ACTIVO_FIJO.BIAF_CIA;
					
					
					insert into DM_ACTIVO_FIJO.BIAF_CIA(
					cia_id,
					raz_soci_cia,
					nro_ruc_cia,
					nom_cjto,
					fech_ulti_actu
					)values(
						pn_cid,
						d(y).raz_soci_cia,
						d(y).nro_ruc_cia,
						d(y).nom_cjto,
						sysdate
					); 
					
					ln_registros_INS := ln_registros_INS + 1;
				
				end if;
				
				ln_total_registros := ln_total_registros + 1;
				
			End Loop;  
			--commit cada 1000 reg
			commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;

			
		--registro CERO:	
		insert into DM_ACTIVO_FIJO.BIAF_CIA(
			cia_id,
			raz_soci_cia,
			nro_ruc_cia,
			nom_cjto,
			fech_ulti_actu
		)select
			0,
			'ND',
			'ND',
			'ND',
			sysdate
			from  dual where  not exists(
			select raz_soci_cia from DM_ACTIVO_FIJO.BIAF_CIA where cia_id = 0
			);

		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		commit;


		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_CIA' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_CIA;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion cuenta contable  ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_DIM_CTA_CTBL(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		
		--declarando el cursor:
		Cursor D_DETALLE Is
		
		select * from STG_ACTIVO_FIJO.TBL_CNTA_CONTA;
		
		--declarando un tipo rowtype 
		Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

		--declarando una variable del tipo anterior.
		d  TBL_D_DETALLE;  

		--declarando variables auxiliares:
		ln_total_registros        	Number := 0;
		ln_registros_UPD	    	Number := 0;
		ln_registros_INS   			Number := 0;   
		ln_existe             		Number := 0;
		v_Fecha_sys        			Varchar2(10);
		v_Hora_sys         			Varchar2(10);
		pn_cid 						Number(28) := 0;

		--limite de registros a tratar en bloque
		limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;

			--iterando sobre los datos del cursor
			For y In 1 .. d.count Loop
				
				Select Count(cnta_cont)
					Into ln_existe
				From DM_ACTIVO_FIJO.BIAF_CNTA_CONTA
				Where cnta_cont  = d(y).cnta_cont;
		 
				If ln_existe > 0 Then
					--actualiza
					Update DM_ACTIVO_FIJO.BIAF_CNTA_CONTA
						Set desc_cnta_cont = d(y).desc_cnta_cont, 
							nom_cia = d(y).cia,
							fech_ulti_actu = Sysdate
					Where cnta_cont = d(y).cnta_cont;

					ln_registros_UPD := ln_registros_UPD + 1;
					
				Else
					--inserta
					--max id
					select nvl(max(cnta_id)+1,1) 
					into pn_cid  
					from DM_ACTIVO_FIJO.BIAF_CNTA_CONTA;

					Insert Into DM_ACTIVO_FIJO.BIAF_CNTA_CONTA(
					cnta_id,
					cnta_cont,
					desc_cnta_cont,
					nom_cia,			
					fech_ulti_actu
					)Values(
					pn_cid,
					d(y).cnta_cont,
					d(y).desc_cnta_cont,
					d(y).cia,
					Sysdate
					);
					
					ln_registros_INS := ln_registros_INS + 1; 
				   
				End if;
				
				ln_total_registros := ln_total_registros + 1;
			End Loop;    
			
			--commit cada 1000
			Commit;

		Exit When D_DETALLE%NotFound;
		End Loop;

		--Registro Cero
		Insert Into DM_ACTIVO_FIJO.BIAF_CNTA_CONTA
			(
			cnta_id ,        
			cnta_cont , 
			desc_cnta_cont,
			nom_cia,
			fech_ulti_actu
			)
		Select
			0,
			'ND',
			'ND',
			'ND',
			sysdate
			From  dual 
			Where  Not Exists
			(Select desc_cnta_cont From DM_ACTIVO_FIJO.BIAF_CNTA_CONTA Where cnta_id = 0);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;       
		Commit;

		--cerrando el cursor
		Close D_DETALLE;  
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_CTA_CTBL' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_CTA_CTBL;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion tipo contrato    ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_DIM_TIPO_CTRTO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		

		--declarando variables auxiliares:
		ln_total_registros        	Number := 0;
		ln_registros_UPD	    	Number := 0;
		ln_registros_INS   			Number := 0;   
		v_Fecha_sys        			Varchar2(10);
		v_Hora_sys         			Varchar2(10);
		pn_cid 						Number(28) := 0;


	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--recorriendo:
		FOR record IN (
			Select pp.cod_tipo_ctrto, pp.desc_tipo_ctrto 
			From STG_ACTIVO_FIJO.TBL_TIPO_CNTR pp 
			where  pp.cod_tipo_ctrto
			not in (select cod_tipo_ctrto from DM_ACTIVO_FIJO.BIAF_TIPO_CNTR)
			)
		LOOP
			--insertando
			--max id
			select nvl(max(tipo_ctrto_id)+1,1) 
				into pn_cid  
			from DM_ACTIVO_FIJO.BIAF_TIPO_CNTR;
			
			insert into DM_ACTIVO_FIJO.BIAF_TIPO_CNTR(
				tipo_ctrto_id,
				cod_tipo_ctrto,
				desc_tipo_ctrto,
				fech_ulti_actu
			)values(
				pn_cid,
				record.cod_tipo_ctrto,
				record.desc_tipo_ctrto,
				sysdate
			); 
			
			ln_registros_INS := ln_registros_INS + 1; 
			commit;
			
		END LOOP;


		--Actualizando
		merge into DM_ACTIVO_FIJO.BIAF_TIPO_CNTR
		using (	Select pp.cod_tipo_ctrto, pp.desc_tipo_ctrto 
				From STG_ACTIVO_FIJO.TBL_TIPO_CNTR pp
				) b
		on 	(DM_ACTIVO_FIJO.BIAF_TIPO_CNTR.cod_tipo_ctrto = b.cod_tipo_ctrto)
		WHEN MATCHED THEN
			--update
			update set
				desc_tipo_ctrto = b.desc_tipo_ctrto,
				fech_ulti_actu = sysdate;
				
			ln_registros_UPD := ln_registros_UPD + 1;
			commit;

		--registro CERO:	
		insert into DM_ACTIVO_FIJO.BIAF_TIPO_CNTR(
			tipo_ctrto_id,
			cod_tipo_ctrto,
			desc_tipo_ctrto,
			fech_ulti_actu
		)select
			0,
			'ND',
			'ND',
			sysdate
			from  dual where  not exists(
			select cod_tipo_ctrto from DM_ACTIVO_FIJO.BIAF_TIPO_CNTR where tipo_ctrto_id = 0
			);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		commit;
		
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_TIPO_CTRTO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_TIPO_CTRTO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion familia activo   ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_DIM_FAMI_ACTIVO(
				pn_Erro          Out Number,
				pv_Mens          Out Varchar2)  Is
		
		

		--declarando variables auxiliares:
		ln_total_registros        	Number := 0;
		ln_registros_UPD	    	Number := 0;
		ln_registros_INS   			Number := 0;   
		v_Fecha_sys        			Varchar2(10);
		v_Hora_sys         			Varchar2(10);
		pn_cid 						Number(28) := 0;


	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--recorriendo
		FOR record IN (
			select codi_fam, nom_rubr, clas_acti_fijo
			from STG_ACTIVO_FIJO.TBL_FAMI_ACTI pp
			where trim(pp.codi_fam)
			not in (select codi_fam from DM_ACTIVO_FIJO.BIAF_FAMI_ACTI))
		LOOP
			
			--insertando
			--max id
			select nvl(max(fam_activo_id)+1,1) 
			into pn_cid  
			from DM_ACTIVO_FIJO.BIAF_FAMI_ACTI;
			
			insert into DM_ACTIVO_FIJO.BIAF_FAMI_ACTI(
				fam_activo_id,
				codi_fam,
				nom_rubr,
				clas_acti_fijo,
				fech_ulti_actu
			)values(
				pn_cid,
				record.codi_fam,
				record.nom_rubr,
				'TANGIBLE',
				sysdate
			); 
			
			ln_registros_INS := ln_registros_INS + 1; 
			commit;
			
		END LOOP;


		--Actualizando
		merge into DM_ACTIVO_FIJO.BIAF_FAMI_ACTI
		using (
				select codi_fam, clas_acti_fijo, trim(nom_rubr) nom_rubr
				from STG_ACTIVO_FIJO.TBL_FAMI_ACTI
			) b
		on (DM_ACTIVO_FIJO.BIAF_FAMI_ACTI.codi_fam = b.codi_fam)
		WHEN MATCHED THEN
			--actualizando
			update set
			nom_rubr = b.nom_rubr,
			fech_ulti_actu = sysdate;
			
			ln_registros_UPD := ln_registros_UPD + 1;
			commit;

		--registro CERO:	
		insert into DM_ACTIVO_FIJO.BIAF_FAMI_ACTI(
			fam_activo_id,
			codi_fam,
			nom_rubr,
			clas_acti_fijo,
			fech_ulti_actu
		)select
			0,
			'ND',
			'ND',
			'ND',
			sysdate
			from  dual where  not exists(
			select nom_rubr from DM_ACTIVO_FIJO.BIAF_FAMI_ACTI where fam_activo_id = 0
			);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		commit;
		
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_FAMI_ACTIVO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_FAMI_ACTIVO;
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion tipo  activo     ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_DIM_TIPO_ACTIVO(
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	select 
            a.cod_tipo_acti,
            a.des_tipo_acti,
            a.tip_indi_prcio,
            a.tip_depr_corp,
            a.tip_depr_fiscal,
            a.tip_reval_corp,
            a.tip_reval_cont,
            a.cnta_cont,
            b.cnta_id,
			a.cia	
	from STG_ACTIVO_FIJO.TBL_TIP_ACTIVO a left join  DM_ACTIVO_FIJO.BIAF_CNTA_CONTA b
	on (a.cnta_cont = b.cnta_cont);
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_UPD	    	Number := 0;
	ln_registros_INS   			Number := 0;   
	ln_existe             		Number := 0;
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);
	pn_cid 						Number(28) := 0;
	pn_cuenta_cont_id           Number(28) := 0;

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
		--iterando sobre los datos del cursor
		For y In d.FIRST .. d.LAST Loop
			
			if d(y).cnta_id is null then 
				pn_cuenta_cont_id := 0;
			else
				pn_cuenta_cont_id := d(y).cnta_id;
			end if;
			
			select count(cod_tipo_acti) 
				into ln_existe 
			from DM_ACTIVO_FIJO.BIAF_TIPO_ACTI
			where cod_tipo_acti = d(y).cod_tipo_acti;
			
			
			if ln_existe > 0 then
				--actualiza
				update DM_ACTIVO_FIJO.BIAF_TIPO_ACTI set
				des_tipo_acti = d(y).des_tipo_acti,
				tip_indi_prcio = d(y).tip_indi_prcio,
				tip_depr_corp = d(y).tip_depr_corp,
				tip_depr_fiscal = d(y).tip_depr_fiscal,
				tip_reval_corp = d(y).tip_reval_corp,
				tip_reval_cont = d(y).tip_reval_cont,
				cnta_id = pn_cuenta_cont_id,
				nom_cia = d(y).cia,
				fech_ulti_actu = sysdate
				where 
				cod_tipo_acti = d(y).cod_tipo_acti;
				
				ln_registros_UPD := ln_registros_UPD + 1;
			else
			--inserta
				--max id
				select nvl(max(tipo_activ_id)+1,1) 
				into pn_cid  
				from DM_ACTIVO_FIJO.BIAF_TIPO_ACTI;
				
				insert into DM_ACTIVO_FIJO.BIAF_TIPO_ACTI(
					tipo_activ_id,
					cod_tipo_acti,
					des_tipo_acti,
					tip_indi_prcio,
					tip_depr_corp,
					tip_depr_fiscal,
					tip_reval_corp,
					tip_reval_cont,
					cnta_id,
					nom_cia,
					fech_ulti_actu
				)values(
					pn_cid,
					d(y).cod_tipo_acti,
					d(y).des_tipo_acti,
					d(y).tip_indi_prcio,
					d(y).tip_depr_corp,
					d(y).tip_depr_fiscal,
					d(y).tip_reval_corp,
					d(y).tip_reval_cont,
					pn_cuenta_cont_id,
					d(y).cia,
					sysdate
				); 
				
				ln_registros_INS := ln_registros_INS + 1;
			
			end if;
			

			ln_total_registros := ln_total_registros + 1;
			
		End Loop;  
		--commit al 1000 registros
		Commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;
		
		
		--insertando caso especial: registro CERO                         
		insert into DM_ACTIVO_FIJO.BIAF_TIPO_ACTI(
			tipo_activ_id,
			cod_tipo_acti,
			des_tipo_acti,
			tip_indi_prcio,
			tip_depr_corp,
			tip_depr_fiscal,
			tip_reval_corp,
			tip_reval_cont,
			cnta_id,
			nom_cia,
			fech_ulti_actu
		)select
			0,
			'ND',
			'ND',
			'ND',
			'ND',
			'N',
			'N',
			'N',
			 0,
			'ND',
			sysdate
			from  dual where  not exists(
			select cod_tipo_acti from DM_ACTIVO_FIJO.BIAF_TIPO_ACTI where tipo_activ_id = 0
			);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		Commit;
		
		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_TIPO_ACTIVO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_TIPO_ACTIVO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion proveedor        ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_DIM_PROVEEDOR(
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	Select * from STG_ACTIVO_FIJO.TBL_PROV;
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_UPD	    	Number := 0;
	ln_registros_INS   			Number := 0;   
	ln_existe             		Number := 0;
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);
	pn_cid 						Number(28) := 0;

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
		--iterando sobre los datos del cursor
		For y In d.FIRST .. d.LAST Loop
				
			ln_total_registros := ln_total_registros + 1;
			
			Select Count(cntro_ruc)
				Into ln_existe
			From DM_ACTIVO_FIJO.BIAF_PROV
			Where cntro_ruc  = d(y).cntro_ruc;
			
			--actualizando
			If ln_existe > 0 Then
				
				Update DM_ACTIVO_FIJO.BIAF_PROV
					Set nom_prov  = d(y).nom_prov,
						nom_cia = d(y).cia,
						fech_ulti_actu = Sysdate
				Where cntro_ruc  = d(y).cntro_ruc;

				ln_registros_UPD := ln_registros_UPD + 1;
				 
			Else
				--insertando
				--max id
				select nvl(max(prov_id)+1,1) 
				into pn_cid  
				from DM_ACTIVO_FIJO.BIAF_PROV;
						 
				Insert Into DM_ACTIVO_FIJO.BIAF_PROV(
					prov_id ,
					cntro_ruc ,
					nom_prov ,
					nom_cia,
					fech_ulti_actu
				)Values(
					pn_cid,
					d(y).cntro_ruc,
					d(y).nom_prov,
					d(y).cia,
					Sysdate
				); 
		  
				ln_registros_INS := ln_registros_INS + 1;       
			End if;
			
		End Loop;  
		--commit al 1000 registros
		Commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;
		
		
		--insertando caso especial: registro CERO                         
		Insert Into DM_ACTIVO_FIJO.BIAF_PROV
		(
		prov_id ,
		cntro_ruc ,
		nom_prov ,
		nom_cia,
		fech_ulti_actu
		)
		Select
		0,
		'ND',
		'ND',
		'ND',
		sysdate
		From  dual Where  Not Exists(Select nom_prov From DM_ACTIVO_FIJO.BIAF_PROV Where prov_id = 0);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		Commit;
		
		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_PROVEEDOR' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_PROVEEDOR;
	
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimencion sucursal         ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_DIM_SUCU(
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	Select * FROM  STG_ACTIVO_FIJO.TBL_SUCU;
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_UPD	    	Number := 0;
	ln_registros_INS   			Number := 0;   
	ln_existe             		Number := 0;
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);
	pn_cid 						Number(28) := 0;

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
		--iterando sobre los datos del cursor
		For y In d.FIRST .. d.LAST Loop
				
			ln_total_registros := ln_total_registros + 1;
		
			Select Count(cod_sucu)
				Into ln_existe
			From DM_ACTIVO_FIJO.BIAF_SUCU
			Where cod_sucu  = d(y).cod_sucu;
	 
			If ln_existe > 0 Then
			
				--actualizando
				Update DM_ACTIVO_FIJO.BIAF_SUCU
				Set des_sucu   = d(y).des_sucu, 
					nom_cia = d(y).cia,
					fech_ulti_actu = Sysdate
				Where cod_sucu   = d(y).cod_sucu;

				ln_registros_UPD := ln_registros_UPD + 1;
			Else
				--insertando
				--max id
				select nvl(max(sucu_id)+1,1) 
					into pn_cid  
				from DM_ACTIVO_FIJO.BIAF_SUCU;
						 
				Insert Into DM_ACTIVO_FIJO.BIAF_SUCU(
				sucu_id ,
				cod_sucu,
				des_sucu,
				nom_cia,
				fech_ulti_actu
				)Values(
				pn_cid,
				d(y).cod_sucu,
				d(y).des_sucu,
				d(y).cia,
				Sysdate
				);
				
				ln_registros_INS := ln_registros_INS + 1;
			   
			End if;           
			
		End Loop;  
		--commit al 1000 registros
		Commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;
		
		
		--insertando caso especial: registro CERO                         
		Insert Into DM_ACTIVO_FIJO.BIAF_SUCU
		(
		sucu_id,        
		cod_sucu, 
		des_sucu,
		fech_ulti_actu
		)
		Select
		0,
		'ND',
		'ND',
		sysdate
		From  dual Where  Not Exists(Select des_sucu From DM_ACTIVO_FIJO.BIAF_SUCU Where sucu_id = 0);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		Commit;
		
		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_SUCU' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_SUCU;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimension centro de costo  ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_DIM_CTRO_CSTO(
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	Select * FROM STG_ACTIVO_FIJO.TBL_CNTR_CSTO;
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_UPD	    	Number := 0;
	ln_registros_INS   			Number := 0;   
	ln_existe             		Number := 0;
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);
	pn_cid 						Number(28) := 0;

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
		--iterando sobre los datos del cursor
		For y In d.FIRST .. d.LAST Loop
				
			ln_total_registros := ln_total_registros + 1;
			
			Select Count(cntr_csto)
				Into ln_existe
			From DM_ACTIVO_FIJO.BIAF_CNTR_CSTO
			Where cntr_csto  = d(y).cntr_csto;
         
			If ln_existe > 0 Then
				--actualiza
				Update DM_ACTIVO_FIJO.BIAF_CNTR_CSTO
				Set des_cntr_csto   = d(y).des_cntr_csto,
					fech_ulti_actu = Sysdate
				Where cntr_csto  = d(y).cntr_csto;

				ln_registros_UPD := ln_registros_UPD + 1;

			Else
				--inserta
				--max id
				select nvl(max(cent_costo_id)+1,1) 
					into pn_cid  
				from DM_ACTIVO_FIJO.BIAF_CNTR_CSTO;
							 
				Insert Into DM_ACTIVO_FIJO.BIAF_CNTR_CSTO(
					cent_costo_id  ,
					cntr_csto  ,
					des_cntr_csto  ,
					fech_ulti_actu
				)Values(
					pn_cid,
					d(y).cntr_csto ,
					d(y).des_cntr_csto ,
					Sysdate
				); 
				
				ln_registros_INS := ln_registros_INS + 1; 
			   
			End if;

		End Loop;  
		--commit al 1000 registros
		Commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;
		
		
		--insertando caso especial: registro CERO                         
		Insert Into DM_ACTIVO_FIJO.BIAF_CNTR_CSTO
	   (
		 cent_costo_id  ,
		 cntr_csto  ,
		 des_cntr_csto  ,
		 fech_ulti_actu
	   )
	   Select
	   0,
	   'ND',
	   'ND',
	   sysdate
	   From  dual Where  Not Exists(Select des_cntr_csto From 
	   DM_ACTIVO_FIJO.BIAF_CNTR_CSTO Where cent_costo_id  = 0);
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		Commit;
		
		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_CTRO_CSTO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_CTRO_CSTO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga dimension activo fijo      ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_DIM_ACTI_FIJO(
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is

	Select 
			ta.tipo_activ_id, fa.fam_activo_id,prv.prov_id,tc.tipo_ctrto_id,
			pp.* 
		FROM STG_ACTIVO_FIJO.TBL_ACTI_MEJO pp left join DM_ACTIVO_FIJO.BIAF_TIPO_ACTI ta 
		on (pp.cod_tipo_acti = ta.cod_tipo_acti ) left join DM_ACTIVO_FIJO.BIAF_FAMI_ACTI fa
		on (pp.familia = fa.codi_fam) left join DM_ACTIVO_FIJO.BIAF_PROV prv
		on (pp.proveedor = prv.cntro_ruc) left join DM_ACTIVO_FIJO.Biaf_Tipo_Cntr tc
		on (pp.cod_tipo_ctrto = tc.cod_tipo_ctrto);
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_UPD	    	Number := 0;
	ln_registros_INS   			Number := 0;   
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);
	pn_cid 						Number(28) := 0;
	pn_tip_acti_id Number(28) := 0;
	pn_fam_acti_id Number(28) := 0;
	pn_prov_id Number(28) := 0;
	pn_tip_ctrt_id Number(28) := 0;
	pn_origen_transfer Number(28) := 0;
	ln_regCA        Number := 0;
    ln_regCM        Number := 0;

	--limite de registros a tratar en bloque
	limite				        Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');

		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
		--iterando sobre los datos del cursor
		For y In d.FIRST .. d.LAST Loop
				
			ln_total_registros := ln_total_registros + 1;
			
			if d(y).tipo_activ_id is null then 
				pn_tip_acti_id := 0;
			else
				pn_tip_acti_id := d(y).tipo_activ_id;
			end if;
			
			if d(y).fam_activo_id is null then 
				pn_fam_acti_id := 0;
			else
				pn_fam_acti_id := d(y).fam_activo_id;
			end if;
			
			if d(y).prov_id is null then 
				pn_prov_id := 0;
			else
				pn_prov_id := d(y).prov_id;
			end if;
			
			if d(y).tipo_ctrto_id is null then 
				pn_tip_ctrt_id := 0;
			else
				pn_tip_ctrt_id := d(y).tipo_ctrto_id;
			end if;
			

			
			if d(y).tipo_origen_transfer is null then 
				pn_origen_transfer := 0;
			else
				--select BIAF_FAMI_ACTI por codi_fam
				pn_origen_transfer := d(y).tipo_origen_transfer;
			end if;
				
				
			--comprobando si el registro existe
			Select
				Count(cod_activo),
				count(cod_mejora)
			Into
				ln_regCA,
				ln_regCM
			From DM_ACTIVO_FIJO.BIAF_ACTI_MEJO
			Where
				cod_activo     = d(y).cod_activo
				and cod_mejora = d(y).cod_mejora;
                
			--si existe
			If  ln_regCA > 0  AND ln_regCM > 0 Then
                
				--si el registro existe actualizar
				Update
					DM_ACTIVO_FIJO.BIAF_ACTI_MEJO
				Set 
					ind_mejora = d(y).ind_mejora,
					nom_acti = d(y).nom_acti,
					ubic_acti = d(y).ubic_acti,
					nit =  d(y).nit,
					plaz_vida_util_cont = d(y).plaz_vida_util_cont,
					plaz_vida_util_corp = d(y).plaz_vida_util_corp,
					fech_adqi = d(y).fech_adqi,
					fech_retir_cont = d(y).fech_retir_cont,
					fech_retir_corp = d(y).fech_retir_corp,
					fech_activ = d(y).fech_activ,
					est_acti = d(y).est_acti,
					tipo_depr_cont = d(y).tipo_depr_cont,
					tipo_depr_corp = d(y).tipo_depr_corp,
					tipo_reval_cont = d(y).tipo_reval_cont,
					tipo_reval_corp = d(y).tipo_reval_corp,
					prov_id = pn_prov_id,--codigos no ID
					fam_activo_id = pn_fam_acti_id,
					tipo_activ_id = pn_tip_acti_id,
					tipo_ctrto_id = pn_tip_ctrt_id,
					asnt_ingr_cont = d(y).asnt_ingr_cont,
					asnt_ingr_corp = d(y).asnt_ingr_corp,
					asnt_retir_cont = d(y).asnt_retir_cont,
					asnt_retir_corp = d(y).asnt_retir_corp,
					nro_ord_comp = d(y).nro_ord_comp,
					empl_apro_ord_comp = d(y).empl_apro_ord_comp,
					resp_acti = d(y).resp_acti, 
					nro_fact = d(y).nro_fact, 
					mone_fact = d(y).mone_fact, 
					nro_ctrto_lsng = d(y).nro_ctrto_lsng, 
					nro_cuot_lsng = d(y).nro_cuot_lsng,
					
					mon_lsng_local = d(y).mon_lsng_local,
					mon_lsng_dolar = d(y).mon_lsng_dolar,
					
					fech_docu_fact =  d(y).fech_docu_fact,
					fech_inic_ctrto_lsng = d(y).fech_inic_ctrto_lsng, 
					fech_fin_ctrto_lsng = d(y).fech_fin_ctrto_lsng,
					esta_ctrto = d(y).esta_ctrto, 
					esta_docu_fact = d(y).esta_docu_fact, 
					cost_orig_local_cont =  d(y).cost_orig_local_cont,
					cost_orig_dolar_cont = d(y).cost_orig_dolar_cont,
					cost_orig_local_corp = d(y).cost_orig_local_corp,
					cost_orig_dolar_corp = d(y).cost_orig_dolar_corp,
					
					cost_orig_baja_local_cont = d(y).cost_orig_baja_local_cont, 
					cost_orig_baja_dolar_cont = d(y).cost_orig_baja_dolar_cont, 
					cost_orig_baja_local_corp = d(y).cost_orig_baja_local_corp, 
					cost_orig_baja_dolar_corp =  d(y).cost_orig_baja_dolar_corp,
					valo_resc_local_fiscal = d(y).valo_resc_local_fiscal, 
					valo_resc_dolar_fiscal =  d(y).valo_resc_dolar_fiscal,
					valo_resc_local_corp = d(y).valo_resc_local_corp,
					valo_resc_dolar_corp = d(y).valo_resc_dolar_corp, 
					mon_fact_sin_igv = d(y).mon_fact_sin_igv, 
					mon_tota_fact = d(y).mon_tota_fact,
					
					ind_transfer = d(y).ind_transfer, 
					tipo_origen_transfer = pn_origen_transfer,
					asnt_myor = d(y).asnt_myor,
					cent_costo_gestr = d(y).cntr_csto_gstor,
					nom_cia = d(y).cia
					
				Where
					cod_activo = d(y).cod_activo
				and cod_mejora = d(y).cod_mejora;
				
				--incrementado contador update
				ln_registros_UPD := ln_registros_UPD + 1;
            
			Else
				--si el registro no existe se inserta
				--max id
				select
					nvl(max(acti_mejor_id)+1,1)
				into
					pn_cid
				from DM_ACTIVO_FIJO.BIAF_ACTI_MEJO;
				
				--insertando
				Insert Into DM_ACTIVO_FIJO.BIAF_ACTI_MEJO
					( 
					acti_mejor_id,
					cod_activo,
					cod_mejora,
					ind_mejora,
					nom_acti,
					ubic_acti,
					nit,
					plaz_vida_util_cont,
					plaz_vida_util_corp,
					fech_adqi,
					fech_retir_cont,
					fech_retir_corp,
					fech_activ,
					est_acti,
					tipo_depr_cont,
					tipo_depr_corp,
					tipo_reval_cont, 
					tipo_reval_corp,
					prov_id,
					fam_activo_id,
					tipo_activ_id,
					tipo_ctrto_id,
					asnt_ingr_cont,
					asnt_ingr_corp,
					asnt_retir_cont,
					asnt_retir_corp,
					nro_ord_comp,
					empl_apro_ord_comp,
					resp_acti,
					nro_fact,
					mone_fact,
					nro_ctrto_lsng,
					nro_cuot_lsng,
					
					mon_lsng_local,
					mon_lsng_dolar,
					
					fech_docu_fact,
					fech_inic_ctrto_lsng,
					fech_fin_ctrto_lsng,
					esta_ctrto,
					esta_docu_fact,
					cost_orig_local_cont,
					cost_orig_dolar_cont,
					cost_orig_local_corp,
					cost_orig_dolar_corp,
					cost_orig_baja_local_cont,
					cost_orig_baja_dolar_cont,
					cost_orig_baja_local_corp,
					cost_orig_baja_dolar_corp,
					valo_resc_local_fiscal,
					valo_resc_dolar_fiscal,
					valo_resc_local_corp,
					valo_resc_dolar_corp,
					mon_fact_sin_igv,
					mon_tota_fact,
					ind_transfer,
					tipo_origen_transfer,
					asnt_myor,
					cent_costo_gestr,
					nom_cia,
					fech_ulti_actu
				) Values( 	
					pn_cid,
					d(y).cod_activo,
					d(y).cod_mejora,
					d(y).ind_mejora,
					d(y).nom_acti,
					d(y).ubic_acti,
					d(y).nit,
					d(y).plaz_vida_util_cont,
					d(y).plaz_vida_util_corp,
					d(y).fech_adqi,
					d(y).fech_retir_cont,
					d(y).fech_retir_corp,
					d(y).fech_activ,
					d(y).est_acti,
					d(y).tipo_depr_cont,
					d(y).tipo_depr_corp,
					d(y).tipo_reval_cont, 
					d(y).tipo_reval_corp,
					pn_prov_id,
					pn_fam_acti_id,
					pn_tip_acti_id,
					pn_tip_ctrt_id,
					d(y).asnt_ingr_cont,
					d(y).asnt_ingr_corp,
					d(y).asnt_retir_cont,
					d(y).asnt_retir_corp,
					d(y).nro_ord_comp,
					d(y).empl_apro_ord_comp,
					d(y).resp_acti,
					d(y).nro_fact,
					d(y).mone_fact,
					d(y).nro_ctrto_lsng,
					d(y).nro_cuot_lsng,
					
					d(y).mon_lsng_local,
					d(y).mon_lsng_dolar,

					
					d(y).fech_docu_fact,
					d(y).fech_inic_ctrto_lsng,
					d(y).fech_fin_ctrto_lsng,
					d(y).esta_ctrto,
					d(y).esta_docu_fact,
					d(y).cost_orig_local_cont,
					d(y).cost_orig_dolar_cont,
					d(y).cost_orig_local_corp,
					d(y).cost_orig_dolar_corp,
					d(y).cost_orig_baja_local_cont,
					d(y).cost_orig_baja_dolar_cont,
					d(y).cost_orig_baja_local_corp,
					d(y).cost_orig_baja_dolar_corp,
					d(y).valo_resc_local_fiscal,
					d(y).valo_resc_dolar_fiscal,
					d(y).valo_resc_local_corp,
					d(y).valo_resc_dolar_corp,
					d(y).mon_fact_sin_igv,
					d(y).mon_tota_fact,
					d(y).ind_transfer,
					pn_origen_transfer,
					d(y).asnt_myor,
					d(y).cntr_csto_gstor,
					d(y).cia,
					sysdate
					);
				
			ln_registros_INS := ln_registros_INS + 1;
			End if;
				
		End Loop;  
		
		--commit al 1000 registros
		Commit;
		EXIT WHEN D_DETALLE%NOTFOUND;
		END LOOP;
		
		
		--insertando caso especial: registro CERO                         
		Insert Into DM_ACTIVO_FIJO.BIAF_ACTI_MEJO
        ( 
		acti_mejor_id,
		cod_activo,
		cod_mejora,
		ind_mejora,
		nom_acti,
		ubic_acti,
		nit,
		plaz_vida_util_cont,
		plaz_vida_util_corp,
		fech_adqi,
		fech_retir_cont,
		fech_retir_corp,
		fech_activ,
		est_acti,
		tipo_depr_cont,
		tipo_depr_corp,
		tipo_reval_cont, 
		tipo_reval_corp,
		prov_id,
		fam_activo_id,
		tipo_activ_id,
		tipo_ctrto_id,
		asnt_ingr_cont,
		asnt_ingr_corp,
		asnt_retir_cont,
		asnt_retir_corp,
		nro_ord_comp,
		empl_apro_ord_comp,
		resp_acti,
		nro_fact,
		mone_fact,
		nro_ctrto_lsng,
		nro_cuot_lsng,
		
		mon_lsng_local,
		mon_lsng_dolar,
		
		fech_docu_fact,
		fech_inic_ctrto_lsng,
		fech_fin_ctrto_lsng,
		esta_ctrto,
		esta_docu_fact,
		cost_orig_local_cont,
		cost_orig_dolar_cont,
		cost_orig_local_corp,
		cost_orig_dolar_corp,
		
		cost_orig_baja_local_cont,
		cost_orig_baja_dolar_cont,
		cost_orig_baja_local_corp,
		cost_orig_baja_dolar_corp,
		valo_resc_local_fiscal,
		valo_resc_dolar_fiscal,
		valo_resc_local_corp,
		valo_resc_dolar_corp,
		mon_fact_sin_igv,
		mon_tota_fact,
		
		ind_transfer,
		tipo_origen_transfer,
		asnt_myor,
		cent_costo_gestr,
		nom_cia,
		fech_ulti_actu
        )
		Select
		0,
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		0,
		0,
		to_date('01/01/1821','dd/MM/yyyy'),
		to_date('01/01/1821','dd/MM/yyyy'),
		to_date('01/01/1821','dd/MM/yyyy'),
		to_date('01/01/1821','dd/MM/yyyy'),
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		0,
		0,
		0,
		0,
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		'ND',
		0,
		0,
		0,
		to_date('01/01/1821','dd/MM/yyyy'),
		to_date('01/01/1821','dd/MM/yyyy'),
		to_date('01/01/1821','dd/MM/yyyy'),
		'ND',
		'ND',
		0,
		0,
		0,
		0,
		
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		
		0,
		0,
		'ND',
		'ND',
		'ND',
		sysdate
		From
			dual
		Where
		Not Exists ( Select cod_activo From DM_ACTIVO_FIJO.BIAF_ACTI_MEJO Where acti_mejor_id = 0 );
		
		
		--incrementando contadores:
		ln_total_registros := ln_total_registros + 1;
		ln_registros_INS := ln_registros_INS + 1;
		
		Commit;
		
		--cerrando el cursor
		CLOSE D_DETALLE;
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. ACTUALIZADOS   -> : '||ln_registros_UPD);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_DIM_ACTI_FIJO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_DIM_ACTI_FIJO;

	--JM: dimencion asiento contable 
	
	
/*********************************************************************************************************
  NOMBRE:      DMPR_ASNTO_CNTBL
  PROPOSITO:   Procedimiento para poblar la DIM Asiento Contable
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_Erro --> 0=Conforme / 1=Error
/*********************************************************************************************************/
Procedure DMPR_ASNTO_CNTBL( pn_Erro  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is  
 
  Cursor D_DETALLE Is
      Select asnto_cont, fech_asnto, tipo_asnto, clase_asnto, cont_asnto, fnte_asnto, orgn_asnto  
        From STG_ACTIVO_FIJO.TBL_ASNTO_CNTBL;
 
  Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;
  d  TBL_D_DETALLE;  
  
 
  nTotReg_DET        Number := 0;
  nTotReg_DET_UPD    Number := 0;
  nTotReg_DET_INS    Number := 0;   
  
  ln_reg             Number := 0;
  v_Fecha_sys        Varchar2(10);
  v_Hora_sys         Varchar2(10); 
  ln_reg_asnto_id    Number := 0;
  
Begin
  
  Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line(' POBLAR TABLA BI ASIENTO CONTABLE ');
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('Activos Fijos');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : Actualizar ASIENTO CONTABLE');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('-------------------------------------------------------');
  dbms_output.put_line('.');
    
  Open D_DETALLE;
   Loop
    Fetch D_DETALLE Bulk Collect Into d Limit 1000;
    For y In 1 .. d.count Loop
        
        nTotReg_DET := nTotReg_DET + 1;

        Select Count(asnto_cont)
          Into ln_reg
          From DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL  
         Where asnto_cont  = d(y).asnto_cont ;
         
         If ln_reg > 0 Then
           Begin
             Update DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL
                Set fech_asnto  = d(y).fech_asnto, 
                    tipo_asnto  = d(y).tipo_asnto,
                    clase_asnto = d(y).clase_asnto,
                    cont_asnto  = d(y).cont_asnto,
                    fnte_asnto  = d(y).fnte_asnto,
                    orgn_asnto  = d(y).orgn_asnto,
                    fech_ulti_actu = sysdate
              Where asnto_cont  = d(y).asnto_cont;

              nTotReg_DET_UPD := nTotReg_DET_UPD + 1;
              pn_Erro:=0;
           Exception 
             When Others Then
                 dbms_output.put_line('Error Update del BIAF_ASNTO_CNTBL :'||sqlerrm||' - '||sqlcode||' - '||' Asiento: '||d(y).asnto_cont);
                 pn_Erro:=1;
           End;  
         Else
           nTotReg_DET_INS := nTotReg_DET_INS + 1;                         
           
           Select Max(asnto_id)+1
             Into ln_reg_asnto_id
             From DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL;
         
           Begin              
           Insert Into DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL(
                asnto_id ,
                asnto_cont ,
                fech_asnto ,
                tipo_asnto ,      
                clase_asnto,
                cont_asnto,
                fnte_asnto,
                orgn_asnto,
                fech_ulti_actu  
              )Values(
                ln_reg_asnto_id,
                d(y).asnto_cont,
                d(y).fech_asnto,
                d(y).tipo_asnto,
                d(y).clase_asnto, 
                d(y).cont_asnto,
                d(y).fnte_asnto,
                d(y).orgn_asnto,
                sysdate
                ); 
            
           pn_Erro:=0;
           
           Exception 
             When Others Then
                 dbms_output.put_line('Error Insert al BIAF_ASNTO_CNTBL :'||sqlerrm||' - '||sqlcode||' - '||' Asiento: '||ln_reg_asnto_id||'-'||d(y).asnto_cont);
                 pn_Erro:=1;
           End;      
         End if;      
    End Loop;    
    
   If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If; 
    
    Exit When D_DETALLE%NotFound;
   End Loop;
   
   
   nTotReg_DET := nTotReg_DET + 1;
   nTotReg_DET_INS := nTotReg_DET_INS + 1;   
   Begin                      
   Insert Into DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL
      (
        asnto_id,
        asnto_cont,
        fech_asnto,
        tipo_asnto,      
        clase_asnto,
        cont_asnto,
        fnte_asnto,
        orgn_asnto,
        fech_ulti_actu
      )
       Select
        0,
        'ND',
        sysdate,
        'ND',
        'ND',
        'ND',
        'ND',
        'ND',
        sysdate
        From  dual Where  Not Exists(Select asnto_cont From DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL Where asnto_id = 0);                                       
         pn_Erro:=0;
     Exception 
       When Others Then
           dbms_output.put_line('Error Insert al DM_ACTIVO_FIJO.BIAF_ASNTO_CNTBL: '||sqlerrm||' - '||sqlcode);
           pn_Erro:=1;
     End;
    If pn_Erro = 0 Then
       Commit;
    Else
       Rollback;
    End If; 
  Close D_DETALLE;    
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('            RESULTADO            ');
  dbms_output.put_line('---------------------------------');
  --
  Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG.              -> : '||nTotReg_DET);
  dbms_output.put_line('TOT.REG. ACTUALIZADOS -> : '||nTotReg_DET_UPD);
  dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||nTotReg_DET_INS);
End DMPR_ASNTO_CNTBL; 


/*********************************************************************************************************
  NOMBRE:      DMFU_ID_CIA
  PROPOSITO:   Función que determina el ID de la compañía
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciado revaluado en soles fiscal
/*********************************************************************************************************/
 Function DMFU_ID_CIA( pv_ruc_cia In DM_ACTIVO_FIJO.biaf_cia.nro_ruc_cia%Type )
  Return Number As
    ln_cia_id Number:=0;
  Begin               
      Select am.cia_id
        Into ln_cia_id
        From dm_activo_fijo.biaf_cia am
       Where am.nro_ruc_cia = pv_ruc_cia;
         If ln_cia_id is null or ln_cia_id = 0 Then
            ln_cia_id := 0;
         End If;
       Return ln_cia_id;
      Exception
        When Others Then
           Return ln_cia_id; 
 End DMFU_ID_CIA; 
 
 /*********************************************************************************************************
  NOMBRE:      DMFU_TEXT_CIA
  PROPOSITO:   Función que determina el nombre de la compañía
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               ls_cia_text --> el nombre de la compañia 
/*********************************************************************************************************/
 Function DMFU_TEXT_CIA( pv_ruc_cia In DM_ACTIVO_FIJO.biaf_cia.nro_ruc_cia%Type )
  Return Varchar As
    ls_cia_text Varchar2(20):=Null;
  Begin               
      Select am.nom_cjto 
        Into ls_cia_text
        From dm_activo_fijo.biaf_cia am
       Where am.nro_ruc_cia = pv_ruc_cia;
       Return ls_cia_text;
      Exception
        When Others Then
           Return ls_cia_text; 
 End DMFU_TEXT_CIA; 
 
/*********************************************************************************************************
  NOMBRE:      DMFU_ID_ASNTO
  PROPOSITO:   Función que determina el ID de la compañía
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_asnto_cont --> Código de Asiento Contable               
  RETORNO:     Retorna variable Number: 
               el codigo id del asiento contable del activo de la tabla STG_ACTIVO_FIJO.tbl_fact_estd_fina
               ln_asnto_id --> el id del asiento 
/*********************************************************************************************************/
 Function DMFU_ID_ASNTO( pv_asnto_cont In stg_activo_fijo.tbl_fact_estd_fina.asnto_cont%Type )
  Return Number As
    ln_asnto_id Number:=0;
  Begin               
      Select am.asnto_id
        Into ln_asnto_id
        From dm_activo_fijo.biaf_asnto_cntbl am
       Where am.asnto_cont = pv_asnto_cont;
         If ln_asnto_id is null or ln_asnto_id = 0 Then
            ln_asnto_id := 0;
         End If;
       Return ln_asnto_id;
      Exception
        When Others Then
           Return ln_asnto_id; 
 End DMFU_ID_ASNTO;  
 
 
/*********************************************************************************************************
  NOMBRE:      DMFU_ID_CENTRO_COSTO
  PROPOSITO:   Función que determina el ID del centro de costo
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cntr_cost --> Código del Centro de Costo
  RETORNO:     Retorna variable Number: 
/*********************************************************************************************************/
 Function DMFU_ID_CENTRO_COSTO( pv_cntr_csto In DM_ACTIVO_FIJO.biaf_cntr_csto.cntr_csto%Type )
  Return Number As
    ln_cent_costo_id Number:=0;
  Begin               
      Select am.cent_costo_id
        Into ln_cent_costo_id
        From dm_activo_fijo.biaf_cntr_csto am
       Where am.cntr_csto = pv_cntr_csto;
         If ln_cent_costo_id is null or ln_cent_costo_id = 0 Then
            ln_cent_costo_id := 0;
         End If;
       Return ln_cent_costo_id;
      Exception
        When Others Then
           Return ln_cent_costo_id; 
 End DMFU_ID_CENTRO_COSTO;

/*********************************************************************************************************
  NOMBRE:      DMFU_ID_ACTI_MEJORA
  PROPOSITO:   Función que determina el ID del centro de costo
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cntr_cost --> Código del Centro de Costo
  RETORNO:     Retorna variable Number: 
/*********************************************************************************************************/
 Function DMFU_ID_ACTI_MEJORA( pv_acti_fijo In DM_ACTIVO_FIJO.Biaf_Acti_Mejo.COD_ACTIVO%Type,
                               pv_acti_mejo In DM_ACTIVO_FIJO.Biaf_Acti_Mejo.COD_MEJORA%Type )
  Return Number As
    ln_acti_mejor_id Number:=0;
  Begin               
      Select am.acti_mejor_id
        Into ln_acti_mejor_id
        From dm_activo_fijo.biaf_acti_mejo am
       Where am.cod_activo = pv_acti_fijo
         And am.cod_mejora = pv_acti_mejo;
         If ln_acti_mejor_id is null or ln_acti_mejor_id = 0 Then
            ln_acti_mejor_id := 0;
         End If;
       Return ln_acti_mejor_id;
      Exception
        When Others Then
           Return ln_acti_mejor_id; 
 End DMFU_ID_ACTI_MEJORA;
 
/*********************************************************************************************************
  NOMBRE:      DMFU_ID_SUCURSAL
  PROPOSITO:   Función que determina el ID de la sucursal
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_codi_sucu --> Código de la sucursal
  RETORNO:     Retorna variable Number: 
/*********************************************************************************************************/
 Function DMFU_ID_SUCURSAL( pv_codi_sucu In DM_ACTIVO_FIJO.biaf_sucu.COD_SUCU%Type )
  Return Number As
    ln_sucu_id Number:=0;
  Begin               
      Select am.sucu_id
        Into ln_sucu_id
        From dm_activo_fijo.biaf_sucu am
       Where am.cod_sucu = pv_codi_sucu;
         If ln_sucu_id is null or ln_sucu_id = 0 Then
            ln_sucu_id := 0;
         End If;
       Return ln_sucu_id;
      Exception
        When Others Then
           Return ln_sucu_id; 
 End DMFU_ID_SUCURSAL; 


/*********************************************************************************************************
  NOMBRE:      DMFU_ID_FAMI
  PROPOSITO:   Función que determina el ID de la familia del activo
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciado revaluado en soles fiscal
/*********************************************************************************************************/
 Function DMFU_ID_FAMI( pv_cod_fami In dm_activo_fijo.biaf_fami_acti.codi_fam%Type )
  Return Number As
    ln_fam_activo_id  Number:=0;
  Begin               
      Select am.fam_activo_id
        Into ln_fam_activo_id
        From dm_activo_fijo.biaf_fami_acti am
       Where am.codi_fam = pv_cod_fami;
         If ln_fam_activo_id is null or ln_fam_activo_id = 0 Then
            ln_fam_activo_id := 0;
         End If;
       Return ln_fam_activo_id;
      Exception
        When Others Then
           Return ln_fam_activo_id; 
 End DMFU_ID_FAMI; 

/*********************************************************************************************************
  NOMBRE:      DMFU_ID_CTA_CTBLE
  PROPOSITO:   Función que determina el ID de la Cuenta Contable del activo
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_cod_acfi --> Código de AF               
  RETORNO:     Retorna variable Number: 
               el monto revaluado en dolares del activo de la tabla STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION
               pn_dolar_f --> el monto depreciado revaluado en soles fiscal
/*********************************************************************************************************/
 Function DMFU_ID_CTA_CTBLE( pv_cta_ctble In dm_activo_fijo.biaf_cnta_conta.cnta_cont%Type )
  Return Number As
    ln_cnta_id  Number:=0;
  Begin               
      Select am.cnta_id
        Into ln_cnta_id
        From dm_activo_fijo.biaf_cnta_conta am
       Where am.cnta_cont = pv_cta_ctble;
         If ln_cnta_id is null or ln_cnta_id = 0 Then
            ln_cnta_id := 0;
         End If;
       Return ln_cnta_id;
      Exception
        When Others Then
           Return ln_cnta_id; 
 End DMFU_ID_CTA_CTBLE; 


	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Fact  activo               ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_FACT_ACTIVO(
			ld_periodo       In  Varchar2,
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	Select
	nvl(peri.peri_mes_id,0) peri_mes_id,
	nvl(cia.cia_id,0) cia_id,
	nvl(af.acti_mejor_id,0) acti_mejor_id,
	nvl(cc.cent_costo_id,0) cent_costo_id,
	nvl(sucu.sucu_id,0) sucu_id,
	faf.cost_orig_local_cont,
	faf.cost_orig_local_corp,
	faf.cost_orig_dolar_cont,
	faf.cost_orig_dolar_corp,
	faf.cost_orig_baja_local_cont,
	faf.cost_orig_baja_dolar_cont,
	faf.cost_orig_baja_local_corp,
	faf.cost_orig_baja_dolar_corp,
	faf.depr_local_cont,
	faf.depr_local_corp,
	faf.depr_dolar_cont,
	faf.depr_dolar_corp,
	faf.mon_fact_sin_igv,
	faf.mon_tota_fact,
	faf.fech_depre_acti,
	faf.pctj_cntr_cost

	FROM  STG_ACTIVO_FIJO.TBL_FACT_ACTI faf left join DM_ACTIVO_FIJO.BIAF_CIA cia
	on (faf.nro_ruc_cia = cia.nro_ruc_cia) left join DM_ACTIVO_FIJO.BIAF_ACTI_MEJO af
	on (faf.cod_activo = af.cod_activo and faf.cod_mejora = af.cod_mejora) left join DM_ACTIVO_FIJO.BIAF_CNTR_CSTO cc
	on (faf.cntr_csto = cc.cntr_csto) left join DM_ACTIVO_FIJO.BIAF_SUCU sucu
	on (faf.cod_sucu = sucu.cod_sucu) left join DM_ACTIVO_FIJO.BIAF_TMPO_PERI peri
	on (faf.peri_mes_id = peri.peri_mes_id)
	where faf.peri_mes_id = ld_periodo;
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_INS   			Number := 0;   
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');
	
	
		--eliminando los registros del periodo para repoblar la fact
		delete from DM_ACTIVO_FIJO.BIAF_FACT_ACTI  
		where  peri_mes_id = ld_periodo;
		
		commit;
	
	
		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
						 
				Insert Into DM_ACTIVO_FIJO.BIAF_FACT_ACTI (
				
				peri_mes_id,
				cia_id,
				acti_mejor_id,
				cent_costo_id,
				
				cost_orig_local_cont,
				cost_orig_local_corp,
				cost_orig_dolar_cont,
				
				cost_orig_dolar_corp,

				cost_orig_baja_local_cont,
				cost_orig_baja_dolar_cont,
				
				cost_orig_baja_local_corp,
				cost_orig_baja_dolar_corp,
				
				depr_local_cont,
				depr_local_corp,
				depr_dolar_cont,
				depr_dolar_corp,
				
				mon_fact_sin_igv,
				mon_tota_fact,
				sucu_id,

				fech_depre_acti,
				pctj_cntr_cost,
				fech_ulti_actu				
				
				)Values(

				d(y).peri_mes_id,
				d(y).cia_id,
				d(y).acti_mejor_id,
				d(y).cent_costo_id,
				d(y).cost_orig_local_cont,
				d(y).cost_orig_local_corp,
				d(y).cost_orig_dolar_cont,
				d(y).cost_orig_dolar_corp,
				d(y).cost_orig_baja_local_cont,
				d(y).cost_orig_baja_dolar_cont,
				d(y).cost_orig_baja_local_corp,
				d(y).cost_orig_baja_dolar_corp,
				d(y).depr_local_cont,
				d(y).depr_local_corp,
				d(y).depr_dolar_cont,
				d(y).depr_dolar_corp,
				d(y).mon_fact_sin_igv,
				d(y).mon_tota_fact,
				d(y).sucu_id,
				d(y).fech_depre_acti,
				d(y).pctj_cntr_cost,
				Sysdate
				);
				
				ln_registros_INS :=  ln_registros_INS + 1;
				ln_total_registros := ln_total_registros + 1;
				
			End Loop;  
		
			Commit;
		
		Exit When D_DETALLE%NotFound;
		End Loop;
  
		
		Close D_DETALLE;  
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_FACT_ACTIVO' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_FACT_ACTIVO;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Fact historico del activo  ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/

	PROCEDURE BIAF_CRGA_FACT_ACTIVO_HIST(
			ld_periodo       In  Varchar2,
			pn_Erro          Out Number,
			pv_Mens          Out Varchar2)  Is
	
	
	--declarando el cursor:
	Cursor D_DETALLE Is
	
	select

	fa.peri_mes_id as peri_mes_id,
	fa.cia_id as cia_id,
	fa.acti_mejor_id as acti_mejor_id,
	fa.cent_costo_id as cent_costo_id,
	
	--costos  originales copia
	(am.cost_orig_local_cont*fa.pctj_cntr_cost)/100  as cost_orig_hist_local_cont,

	(am.cost_orig_dolar_cont*fa.pctj_cntr_cost)/100 as cost_orig_hist_dolar_cont,

	(am.cost_orig_local_corp*fa.pctj_cntr_cost)/100 as cost_orig_hist_local_corp,
	(am.cost_orig_dolar_corp*fa.pctj_cntr_cost)/100 as cost_orig_hist_dolar_corp,
	
	

	--costos orig acumulados a la fecha de corte desde el primer dia del año
	(
		case when  extract(year from am.fech_activ)  =  extract(year from to_date(ld_periodo, 'YYYYMM'))  then 
			(am.cost_orig_local_cont*fa.pctj_cntr_cost)/100
		else
			null
		end
	) as cost_orig_acum_local_cont,
	
	
	(
		case when  extract(year from am.fech_activ)  =  extract(year from to_date(ld_periodo, 'YYYYMM'))  then 
			(am.cost_orig_dolar_cont*fa.pctj_cntr_cost)/100
		else
			null
		end
	) as cost_orig_acum_dolar_cont,
	
	
	(
		case when  extract(year from am.fech_activ)  =  extract(year from to_date(ld_periodo, 'YYYYMM'))  then 
			(am.cost_orig_local_corp*fa.pctj_cntr_cost)/100
		else
			null
		end
	) as cost_orig_acum_local_corp,
	
	(
		case when  extract(year from am.fech_activ)  =  extract(year from to_date(ld_periodo, 'YYYYMM'))  then 
			(am.cost_orig_dolar_corp*fa.pctj_cntr_cost)/100
		else
			null
		end
	) as cost_orig_acum_dolar_corp,
	
	--depreciaciones acumuladas desde el inicio al perido de consulta, si salne null es pq no tienen alfun tipo de depreciacion(fiscal o contable)
	(
	select 
	sum(pp.depr_local) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') <= to_date(ld_periodo,'YYYYMM')
	and pp.depr_local is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'F'
	) as depr_acum_local_cont,
	
	
	(
	select 
	sum(pp.depr_dolar) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') <= to_date(ld_periodo,'YYYYMM')
	and pp.depr_dolar is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'F'
	) as depr_acum_dolar_cont,
	
	
	(
	select 
	sum(pp.depr_local) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') <= to_date(ld_periodo,'YYYYMM')
	and pp.depr_local is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'C'
	) as depr_acum_local_corp,
	
	
	(
	select 
	sum(pp.depr_dolar) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') <= to_date(ld_periodo,'YYYYMM')
	and pp.depr_dolar is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'C'
	) as depr_acum_dolar_corp,
	
	--depreciaciones acumuladas desde enero del periodo a la fecha de corte del periodo
	(
	select 
	sum(pp.depr_local) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM')  
	between  TRUNC(to_date(ld_periodo,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(ld_periodo,'YYYYMM'))  
	and pp.depr_local is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'F'
	) as depr_acum_peri_local_cont,

	
	(
	select 
	sum(pp.depr_dolar) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM')  
	between  TRUNC(to_date(ld_periodo,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(ld_periodo,'YYYYMM'))  
	and pp.depr_dolar is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'F'
	) as depr_acum_peri_dolar_cont,
	
	
	(
	select 
	sum(pp.depr_local) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM')  
	between  TRUNC(to_date(ld_periodo,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(ld_periodo,'YYYYMM'))  
	and pp.depr_local is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'C'
	) as depr_acum_peri_local_corp,
	
	
	(
	select 
	sum(pp.depr_dolar) 
	from
	STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION pp, 
	DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
	where 
	pp.activo_fijo = qq.cod_activo
	and pp.mejora = qq.cod_mejora
	and qq.acti_mejor_id = fa.acti_mejor_id 
	and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM')  
	between  TRUNC(to_date(ld_periodo,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(ld_periodo,'YYYYMM'))  
	and pp.depr_dolar is not null
	and pp.referencia  = 'H' 
	and pp.contabilidad = 'C'
	) as depr_acum_peri_dolar_corp,
	

	--monto facturados
	am.mon_fact_sin_igv as mon_fact_hist_sin_igv, 
	am.mon_tota_fact as mon_tota_hist_fact,
	
	/*
	(
	select distinct fam.mon_fact_sin_igv 
	from  DM_ACTIVO_FIJO.BIAF_FACT_ACTI fam
	where fam.acti_mejor_id =  fa.acti_mejor_id
	) as mon_fact_hist_sin_igv,

	(
	select distinct fam.mon_tota_fact 
	from  DM_ACTIVO_FIJO.BIAF_FACT_ACTI fam
	where fam.acti_mejor_id =  fa.acti_mejor_id
	) as mon_tota_hist_fact,
	*/
	fa.pctj_cntr_cost as pctj_cntr_cost_hist,
	fa.sucu_id as sucu_id
	
	
	from 
	DM_ACTIVO_FIJO.BIAF_FACT_ACTI fa,  
	BIAF_ACTI_MEJO am, 
	BIAF_CNTR_CSTO cc 
	where 
	fa.acti_mejor_id = am.acti_mejor_id
	and fa.cent_costo_id = cc.cent_costo_id
	--solo activos vigentes hasta el periodo, se da prioridad a la baja contable o fiscal
    and to_date(to_char(nvl(trunc(am.fech_retir_cont),to_date('12/12/2050','dd/MM/YYYY')),'YYYYMM')  ,'YYYYMM')  >  to_date(ld_periodo, 'YYYYMM')
	and  fa.peri_mes_id = ld_periodo
	---
	--and fa.peri_mes_id = '201403'
	--and fa.acti_mejor_id = '58515'  --51312
	;
	
	
	--declarando un tipo rowtype 
	Type TBL_D_DETALLE Is Table Of D_DETALLE%RowType;

	--declarando una variable del tipo anterior.
	d  TBL_D_DETALLE;  

	--declarando variables auxiliares:
	ln_total_registros        	Number := 0;
	ln_registros_INS   			Number := 0;   
	v_Fecha_sys        			Varchar2(10);
	v_Hora_sys         			Varchar2(10);

	--limite de registros a tratar en bloque
	limite				Number := 1000;

	Begin
		
		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;

		--fecha y hora ctual
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
		Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		
		--pintando el inicio
		dbms_output.enable(2000000);
		dbms_output.put_line('FECHA         : '||v_Fecha_sys);
		dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
		dbms_output.put_line('--------------------------------------');
		
		--eliminando los registros del periodo para repoblar la fact
		delete from DM_ACTIVO_FIJO.BIAF_FACT_ACTI_HIST  
		where  peri_mes_id = ld_periodo;
		
		commit;
	
		--abriendo el cursor:
		Open D_DETALLE;
		Loop
		Fetch D_DETALLE Bulk Collect Into d Limit limite;
	
			--iterando sobre los datos del cursor
			For y In d.FIRST .. d.LAST Loop
			
				Insert Into DM_ACTIVO_FIJO.BIAF_FACT_ACTI_HIST (
					--PK
					peri_mes_id,
					cia_id,
					acti_mejor_id,
					cent_costo_id,
					
					--costos historicos
					cost_orig_hist_local_cont,
					cost_orig_hist_dolar_cont,
					cost_orig_hist_local_corp,
					cost_orig_hist_dolar_corp,
					
					--costos acumulados a la fecha de corte desde el primer dia del año
					cost_orig_acum_local_cont,
					cost_orig_acum_dolar_cont,
					cost_orig_acum_local_corp,
					cost_orig_acum_dolar_corp,
					
					--depreciacion acumulada hasta el periodo de consulta
					depr_acum_local_cont,
					depr_acum_dolar_cont,
					depr_acum_local_corp,
					depr_acum_dolar_corp,
					
					--depreciaciones acumuladas desde enero del periodo a la fecha de corte del periodo
					depr_acum_peri_local_cont,
					depr_acum_peri_dolar_cont,
					depr_acum_peri_local_corp,
					depr_acum_peri_dolar_corp,
					
					--adicionales
					mon_fact_hist_sin_igv,
					mon_tota_hist_fact,
					pctj_cntr_cost_hist,
					sucu_id,    
					fech_ulti_actu
					
				) values(
					d(y).peri_mes_id,
					d(y).cia_id,
					d(y).acti_mejor_id,
					d(y).cent_costo_id,
					
					--costos historicos
					d(y).cost_orig_hist_local_cont,
					d(y).cost_orig_hist_dolar_cont,
					d(y).cost_orig_hist_local_corp,
					d(y).cost_orig_hist_dolar_corp,
					
					--costos acumulados a la fecha de corte desde el primer dia del año
					d(y).cost_orig_acum_local_cont,
					d(y).cost_orig_acum_dolar_cont,
					d(y).cost_orig_acum_local_corp,
					d(y).cost_orig_acum_dolar_corp,
					
					--depreciacion acumulada hasta el periodo de consulta
					d(y).depr_acum_local_cont,
					d(y).depr_acum_dolar_cont,
					d(y).depr_acum_local_corp,
					d(y).depr_acum_dolar_corp,
					
					--depreciaciones acumuladas desde enero del periodo a la fecha de corte del periodo
					d(y).depr_acum_peri_local_cont,
					d(y).depr_acum_peri_dolar_cont,
					d(y).depr_acum_peri_local_corp,
					d(y).depr_acum_peri_dolar_corp,
					--adicionales
					d(y).mon_fact_hist_sin_igv,
					d(y).mon_tota_hist_fact,
					d(y).pctj_cntr_cost_hist,
					d(y).sucu_id,    
					sysdate
				);
				
				ln_registros_INS :=  ln_registros_INS + 1;
				ln_total_registros := ln_total_registros + 1;
				
			End Loop;  
		
			Commit;
		
		Exit When D_DETALLE%NotFound;
		End Loop;
  
		
		Close D_DETALLE;  
		
		--mostrando resultados:
		Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
		dbms_output.put_line('HORA FIN : '||v_Hora_sys);
		dbms_output.put_line('TOT.REG. INSERTADOS   -> : '||ln_registros_INS);
		dbms_output.put_line('TOT.REG. AFECTADOS   -> : '||ln_total_registros);
		
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_FACT_ACTIVO_HIST' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_FACT_ACTIVO_HIST;
	
	
	--JM: fact
	
/*********************************************************************************************************
  NOMBRE:      DMPR_FACT_REVA
  PROPOSITO:   Procedimiento para poblar la FACT de Revaluaciones de los Activos Fijos de HTB y SMART
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_retorno --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_FACT_REVA( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                           pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  ld_periodo         Varchar2(10) := null;
  
  Cursor C_TBLON_BIAF_FACT_REVAL Is
  select x.peri_mes_id, 
       x.cia_id, 
       x.cent_costo_id, 
       x.acti_mejor_id,       
       x.cost_reval_local_cont, 
       x.cost_reval_dolar_cont,
       x.cost_reval_local_corp,
       x.cost_reval_dolar_corp,
       x.cost_retir_reval,                
       ( select distinct nvl(ww.depr_reval_loc ,0)
          from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION_REVAL ww
         where ww.activo_fijo = x.cod_activo
           and ww.mejora = x.cod_mejora
           and ltrim(TO_CHAR(trunc(ww.fecha),'YYYYMM'),'0')  = pv_per_mens
       )  as depr_reval_local_cont,
       x.depr_reval_dolar_cont,
       x.depr_reval_local_corp,
       x.depr_reval_dolar_corp,
       x.depr_retir_reval,
       x.fech_depre_reval,
       x.sucu_id,        
       x.pctj_cntr_cost_reval        
   from ( Select distinct a.cod_activo, a.cod_mejora, pv_per_mens as peri_mes_id,
                 DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_CIA( a.nro_ruc_cia ) as cia_id,
                 DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_CENTRO_COSTO( a.cntr_csto ) as cent_costo_id, 
                 DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_ACTI_MEJORA( a.cod_activo, a.cod_mejora) as acti_mejor_id,  
                 a.cost_reval_dolar_cont,
                 a.cost_reval_local_corp,
                 a.cost_reval_dolar_corp,
                 a.cost_retir_reval,                
                 a.depr_reval_dolar_cont,
                 a.depr_reval_local_corp,
                 a.depr_reval_dolar_corp,
                 a.depr_reval_retiro as depr_retir_reval,
                 a.fech_depre_reval,
                 DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_SUCURSAL( a.cod_sucu ) as sucu_id,  
                 
                 ( case when
                          ( select distinct ltrim(TO_CHAR(trunc(pp.fecha),'YYYYMM'),'0')   
                              from STG_ACTIVO_FIJO.TBL_HIST_REVALUACION pp
                             where pp.activo_fijo = a.cod_activo
                               and pp.mejora = a.cod_mejora
                               and pp.cia = DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_TEXT_CIA( a.nro_ruc_cia ) ) = pv_per_mens ---'201602' 
                       then ( select sum(pp.reval_valor) as reval_valor
                                from STG_ACTIVO_FIJO.TBL_HIST_REVALUACION pp
                               where pp.activo_fijo = a.cod_activo
                                 and pp.mejora = a.cod_mejora
                                 and pp.cia = DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_TEXT_CIA( a.nro_ruc_cia ) )
                       else
                           0
                                 
                   end ) as cost_reval_local_cont, 
                                    
                 (select avg(sum(b.porcentaje)) as porcentaje
                    from STG_ACTIVO_FIJO.TBL_ACTIVO_CENTRO b
                   where a.cod_activo = b.activo_fijo and a.cntr_csto = b.centro_costo
                   group by b.porcentaje
                  ) as pctj_cntr_cost_reval
                  
            From STG_ACTIVO_FIJO.TBL_FACT_REVAL a) x;
       

   Type TBL_C_TBLON_BIAF_FACT_REVAL Is Table Of C_TBLON_BIAF_FACT_REVAL%RowType;
    d TBL_C_TBLON_BIAF_FACT_REVAL;  
   
   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_dif_sol         Number := 0;
   ln_dif_index       Number := 0;
   ln_periodo         Number := 0;
   ld_fecha_act       Date;
   ln_existe          Number := 0;
   pn_cid             Number := 0;  
   
   v_Fecha_sys        Varchar2(10):=null;
   v_Hora_sys         Varchar2(10):=null; 
  
Begin

	--inicializando el periodo
	ld_periodo := pv_per_mens;
    
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA FACT REVALUACION');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  --eliminando los registros del periodo para repoblar la fact
	Delete From DM_ACTIVO_FIJO.BIAF_FACT_REVAL Where peri_mes_id = pv_per_mens;
  
  Open C_TBLON_BIAF_FACT_REVAL;
  Loop 
    Fetch C_TBLON_BIAF_FACT_REVAL Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop
        
        nTotReg_DPO := nTotReg_DPO + 1;        
        Begin
        Insert Into DM_ACTIVO_FIJO.BIAF_FACT_REVAL (
                peri_mes_id,  
                cia_id,
                cent_costo_id,
                acti_mejor_id,
                cost_reval_local_cont ,
                cost_reval_dolar_cont ,
                cost_reval_local_corp ,
                cost_reval_dolar_corp ,
                cost_retir_reval, 
                depr_reval_local_cont , 
                depr_reval_dolar_cont , 
                depr_reval_local_corp , 
                depr_reval_dolar_corp ,                     
                depr_retir_reval ,
                fech_depre_reval,                                                                                                          
                sucu_id,              
                fech_ulti_actu,
                pctj_cntr_cost_reval  
          ) Values (
                d(y).peri_mes_id,  
                d(y).cia_id,
                d(y).cent_costo_id,
                d(y).acti_mejor_id,
                d(y).cost_reval_local_cont ,
                d(y).cost_reval_dolar_cont ,
                d(y).cost_reval_local_corp ,
                d(y).cost_reval_dolar_corp ,
                d(y).cost_retir_reval, 
                nvl(d(y).depr_reval_local_cont,0) , 
                d(y).depr_reval_dolar_cont , 
                d(y).depr_reval_local_corp , 
                d(y).depr_reval_dolar_corp ,                     
                d(y).depr_retir_reval ,
                d(y).fech_depre_reval,                                                                                      
                d(y).sucu_id,
                sysdate,
                nvl(d(y).pctj_cntr_cost_reval,0) 
           );
          
           ln_registros_INS := ln_registros_INS + 1;
           pn_retorno:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert DM_ACTIVO_FIJO.BIAF_FACT_REVAL: '||sqlerrm||' - '||sqlcode||' Activo id: '||d(y).acti_mejor_id || ' Periodo: '||d(y).peri_mes_id);
            pn_retorno:=1;
        End;  
    End Loop;
    If pn_retorno = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_TBLON_BIAF_FACT_REVAL%NotFound;
  End Loop;
  Close C_TBLON_BIAF_FACT_REVAL;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales   : '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Insertados: '||ln_registros_INS);

End DMPR_FACT_REVA;

/*********************************************************************************************************
  NOMBRE:      DMPR_FACT_REVA_HIST
  PROPOSITO:   Procedimiento para poblar la FACT de Revaluaciones Historicas de los Activos Fijos de HTB y SMART
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_retorno --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_FACT_REVA_HIST( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                                pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  Cursor C_BIAF_FACT_REVAL_HIST Is
      Select a.peri_mes_id,
             a.cia_id,
             a.cent_costo_id, 
             a.acti_mejor_id, 
             a.cost_reval_local_cont as cost_reval_hist_local_cont,
             a.cost_reval_dolar_cont as cost_reval_hist_dolar_cont,
             a.cost_reval_local_corp as cost_reval_hist_local_corp, 
             a.cost_reval_dolar_corp as cost_reval_hist_dolar_corp,
             a.depr_reval_local_cont as depr_acum_reval_local_cont, 
             a.depr_reval_dolar_cont as depr_acum_reval_dolar_cont, 
             a.depr_reval_local_corp as depr_acum_reval_local_corp,
             a.depr_reval_dolar_corp as depr_acum_reval_dolar_corp,
             a.fech_depre_reval, 
             a.cost_retir_reval,
             a.depr_retir_reval,
             a.pctj_cntr_cost_reval, 
             a.sucu_id,
             a.fech_ulti_actu,
             sum(a.cost_reval_local_cont) as cost_acum_reval_local_cont,
             sum(a.cost_reval_dolar_cont) as cost_acum_reval_dolar_cont, 
             sum(a.cost_reval_local_corp) as cost_acum_reval_local_corp,
             sum(a.cost_reval_dolar_corp) as cost_acum_reval_dolar_corp
        From DM_ACTIVO_FIJO.BIAF_FACT_REVAL a
       Where a.peri_mes_id = pv_per_mens
       group by a.peri_mes_id,
             a.cia_id,
             a.cent_costo_id, 
             a.acti_mejor_id, 
             a.cost_reval_local_cont,
             a.cost_reval_dolar_cont,
             a.cost_reval_local_corp, 
             a.cost_reval_dolar_corp,
             a.depr_reval_local_cont, 
             a.depr_reval_dolar_cont, 
             a.depr_reval_local_corp,
             a.depr_reval_dolar_corp,
             a.fech_depre_reval, 
             a.cost_retir_reval,
             a.depr_retir_reval,
             a.pctj_cntr_cost_reval, 
             a.sucu_id,
             a.fech_ulti_actu;   

   Type BIAF_FACT_REVAL_HIST Is Table Of C_BIAF_FACT_REVAL_HIST%RowType;
    d BIAF_FACT_REVAL_HIST;  
   
   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_dif_sol         Number := 0;
   ln_dif_index       Number := 0;
   ln_periodo         Number := 0;
   ld_fecha_act       Date;
   ln_existe          Number := 0;
   pn_cid             Number := 0;
   
   ln_depr_acum_reval_local_cont Number := 0;
   ln_depr_acum_reval_dolar_cont Number := 0; 
   ln_depr_acum_reval_local_corp Number := 0; 
   ln_depr_acum_reval_dolar_corp Number := 0;
   ln_depr_acum_retir_reval      Number := 0; 
   ln_depr_acum_rev_per_loc_cont Number := 0; 
   ln_depr_acum_rev_per_dol_cont Number := 0; 
   ln_depr_acum_rev_per_loc_corp Number := 0; 
   ln_depr_acum_rev_per_dol_corp Number := 0;   
   ln_cost_reval_hist_local_cont Number := 0;   
   ln_cost_reval_hist_dolar_cont Number := 0;   
   ln_cost_reval_hist_local_corp Number := 0;   
   ln_cost_reval_hist_dolar_corp Number := 0;   
   ln_cost_acum_reval_local_cont Number := 0;   
   ln_cost_acum_reval_local_corp Number := 0;   
   ln_cost_acum_reval_dolar_cont Number := 0;   
   ln_cost_acum_reval_dolar_corp Number := 0;
   

   ln_depr_acum_reva_loca_peri_f Number := 0;
   ln_depr_acum_reva_dola_peri_f Number := 0;
   ln_depr_acum_reva_loca_peri_c Number := 0;
   ln_depr_acum_reva_dola_peri_c Number := 0;
   
   v_Fecha_sys        Varchar2(10):=null;
   v_Hora_sys         Varchar2(10):=null;   
Begin

   
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA FACT REVALUACION HIST');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  --eliminando los registros del periodo para repoblar la fact
	Delete From DM_ACTIVO_FIJO.BIAF_FACT_REVA_HIST Where peri_mes_id = pv_per_mens;
  
  Open C_BIAF_FACT_REVAL_HIST;
  Loop 
    Fetch C_BIAF_FACT_REVAL_HIST Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop
        
        nTotReg_DPO := nTotReg_DPO + 1;  
        Begin
          Select sum(cost_reval_hist_local_cont), sum(cost_reval_hist_dolar_cont),
                 sum(cost_reval_hist_local_corp), sum(cost_reval_hist_dolar_corp),
                 sum(depr_acum_reval_local_cont), sum(depr_acum_reval_dolar_cont), 
                 sum(depr_acum_reval_local_corp), sum(depr_acum_reval_dolar_corp),  
                 depr_acum_retir_reval,
                 sum(depr_acum_reval_peri_loca_cont), sum(depr_acum_reval_peri_dola_cont), 
                 sum(depr_acum_reval_peri_loca_corp), sum(depr_acum_reval_peri_dola_corp)/*,
                 cost_acum_reval_local_cont, cost_acum_reval_local_corp,
                 cost_acum_reval_dolar_cont, cost_acum_reval_dolar_corp  */
            Into ln_cost_reval_hist_local_cont, ln_cost_reval_hist_dolar_cont,
                 ln_cost_reval_hist_local_corp, ln_cost_reval_hist_dolar_corp,
                 ln_depr_acum_reval_local_cont, ln_depr_acum_reval_dolar_cont, 
                 ln_depr_acum_reval_local_corp, ln_depr_acum_reval_dolar_corp, 
                 ln_depr_acum_retir_reval,
                 ln_depr_acum_rev_per_loc_cont, ln_depr_acum_rev_per_dol_cont, 
                 ln_depr_acum_rev_per_loc_corp, ln_depr_acum_rev_per_dol_corp/*,
                 ln_cost_acum_reval_local_cont, ln_cost_acum_reval_local_corp,
                 ln_cost_acum_reval_dolar_cont, ln_cost_acum_reval_dolar_corp */
            From DM_ACTIVO_FIJO.BIAF_FACT_REVA_HIST x
           Where x.cia_id        = d(y).cia_id
             And x.cent_costo_id = d(y).cent_costo_id
             And x.acti_mejor_id = d(y).acti_mejor_id   
            -- And x.peri_mes_id   = d(y).peri_mes_id
            Group by depr_acum_retir_reval/*,
                     cost_acum_reval_local_cont, cost_acum_reval_local_corp,
                     cost_acum_reval_dolar_cont, cost_acum_reval_dolar_corp*/;
        Exception
          When Others Then                    
             ln_cost_reval_hist_local_cont := 0;
             ln_cost_reval_hist_dolar_cont := 0;
             ln_cost_reval_hist_local_corp := 0;
             ln_cost_reval_hist_dolar_corp := 0;
             ln_depr_acum_reval_local_cont := 0;
             ln_depr_acum_reval_dolar_cont := 0; 
             ln_depr_acum_reval_local_corp := 0; 
             ln_depr_acum_reval_dolar_corp := 0;
             ln_depr_acum_retir_reval      := 0; 
             ln_depr_acum_rev_per_loc_cont := 0; 
             ln_depr_acum_rev_per_dol_cont := 0; 
             ln_depr_acum_rev_per_loc_corp := 0; 
             ln_depr_acum_rev_per_dol_corp := 0; 
             ln_cost_acum_reval_local_cont := 0;
             ln_cost_acum_reval_local_corp := 0; 
             ln_cost_acum_reval_dolar_cont := 0; 
             ln_cost_acum_reval_dolar_corp := 0; 
        End;
        Begin
            select sum(pp.depr_reval_loc ), sum(pp.depr_reval_dol) 
            into ln_depr_acum_reva_loca_peri_f,  ln_depr_acum_reva_dola_peri_f
            from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION_REVAL pp, DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
            where pp.activo_fijo = qq.cod_activo
              and pp.mejora = qq.cod_mejora
              and qq.acti_mejor_id = d(y).acti_mejor_id 
              and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') between  TRUNC(to_date(pv_per_mens,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(pv_per_mens,'YYYYMM'))  
              and pp.depr_reval_loc is not null
              and pp.contabilidad = 'F';    
        Exception
          When Others Then 
            ln_depr_acum_reva_loca_peri_f:=0;
            ln_depr_acum_reva_dola_peri_f:=0;         
        End;      
        Begin
            select sum(pp.depr_reval_loc ), sum(pp.depr_reval_dol) 
            into ln_depr_acum_reva_loca_peri_c,  ln_depr_acum_reva_dola_peri_c
            from STG_ACTIVO_FIJO.TBL_HIST_DEPRECIACION_REVAL pp, DM_ACTIVO_FIJO.BIAF_ACTI_MEJO qq 
            where pp.activo_fijo = qq.cod_activo
              and pp.mejora = qq.cod_mejora
              and qq.acti_mejor_id = d(y).acti_mejor_id
              and to_date(TO_CHAR( pp.fecha, 'YYYYMM'), 'YYYYMM') between  TRUNC(to_date(pv_per_mens,'YYYYMM'),'YEAR')  and LAST_DAY(to_date(pv_per_mens,'YYYYMM'))  
              and pp.depr_reval_loc is not null
              and pp.contabilidad = 'C';            
        Exception
          When Others Then 
            ln_depr_acum_reva_loca_peri_c:=0;
            ln_depr_acum_reva_dola_peri_c:=0;                       
        End;  
        
        Begin
          Insert Into DM_ACTIVO_FIJO.BIAF_FACT_REVA_HIST (
                  peri_mes_id,  
                  cia_id,
                  cent_costo_id,
                  acti_mejor_id,
                  cost_reval_hist_local_cont ,
                  cost_reval_hist_dolar_cont ,
                  cost_reval_hist_local_corp ,
                  cost_reval_hist_dolar_corp ,                     
                  depr_acum_reval_local_cont , 
                  depr_acum_reval_dolar_cont , 
                  depr_acum_reval_local_corp , 
                  depr_acum_reval_dolar_corp ,   
                  depr_acum_reval_peri_loca_cont,
                  depr_acum_reval_peri_dola_cont,
                  depr_acum_reval_peri_loca_corp,
                  depr_acum_reval_peri_dola_corp,  
                  cost_acum_retir_reval,                     
                  depr_acum_retir_reval,   
                  pctj_cntr_cost_hist_reva,                                                                                   
                  sucu_id,                            
                  fech_ulti_actu,
                  cost_acum_reval_local_cont,                
                  cost_acum_reval_local_corp,
                  cost_acum_reval_dolar_cont,
                  cost_acum_reval_dolar_corp 
            ) Values (
                  d(y).peri_mes_id,  
                  d(y).cia_id,
                  d(y).cent_costo_id,
                  d(y).acti_mejor_id,
                  d(y).cost_reval_hist_local_cont , --+ ln_cost_reval_hist_local_cont, 
                  d(y).cost_reval_hist_dolar_cont , --+ ln_cost_reval_hist_dolar_cont,
                  d(y).cost_reval_hist_local_corp , --+ ln_cost_reval_hist_local_corp,
                  d(y).cost_reval_hist_dolar_corp , --+ ln_cost_reval_hist_dolar_corp,
                  d(y).depr_acum_reval_local_cont + ln_depr_acum_reval_local_cont, 
                  d(y).depr_acum_reval_dolar_cont + ln_depr_acum_reval_dolar_cont, 
                  d(y).depr_acum_reval_local_corp + ln_depr_acum_reval_local_corp, 
                  d(y).depr_acum_reval_dolar_corp + ln_depr_acum_reval_dolar_corp,       
                  nvl(ln_depr_acum_reva_loca_peri_f,0), --ln_depr_acum_rev_per_loc_cont,
                  nvl(ln_depr_acum_reva_dola_peri_f,0), --ln_depr_acum_rev_per_dol_cont,
                  nvl(ln_depr_acum_reva_loca_peri_c,0), --ln_depr_acum_rev_per_loc_corp,
                  nvl(ln_depr_acum_reva_dola_peri_c,0), --ln_depr_acum_rev_per_dol_corp,  
                  d(y).cost_retir_reval,  
                  d(y).depr_retir_reval + ln_depr_acum_retir_reval,
                  d(y).pctj_cntr_cost_reval,
                  d(y).sucu_id,
                  sysdate,
                  d(y).cost_acum_reval_local_cont + ln_cost_reval_hist_local_cont, --ln_cost_acum_reval_local_cont, 
                  d(y).cost_acum_reval_local_corp + ln_cost_reval_hist_dolar_cont, --ln_cost_acum_reval_local_corp,
                  d(y).cost_acum_reval_dolar_cont + ln_cost_reval_hist_local_corp, --ln_cost_acum_reval_dolar_cont, 
                  d(y).cost_acum_reval_dolar_corp + ln_cost_reval_hist_dolar_corp  --ln_cost_acum_reval_dolar_corp 
             );
          
           ln_registros_INS := ln_registros_INS + 1;
           pn_retorno:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert DM_ACTIVO_FIJO.BIAF_FACT_REVA_HIST: '||sqlerrm||' - '||sqlcode||' Activo_Id: '||d(y).acti_mejor_id);
            pn_retorno:=1;
        End;  
    End Loop;
    If pn_retorno = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_BIAF_FACT_REVAL_HIST%NotFound;
  End Loop;
  Close C_BIAF_FACT_REVAL_HIST;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales   : '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Insertados: '||ln_registros_INS);

End DMPR_FACT_REVA_HIST;
	
	
	
	

/*********************************************************************************************************
  NOMBRE:      DMPR_FACT_FINA
  PROPOSITO:   Procedimiento para poblar la FACT de Estados Financieros de los Activos Fijos de HTB y SMART
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_retorno --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_FACT_FINA( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                           pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  Cursor C_BIAF_FACT_FINA Is
      Select distinct DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_CTA_CTBLE ( a.cnta_cont ) as cnta_id,
             a.codi_fam,DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_FAMI( a.codi_fam ) as fam_activo_id,
             a.peri_mes_id,
             DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_CIA( a.nro_ruc_cia ) as cia_id,
             DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.DMFU_ID_ASNTO( a.asnto_cont ) as asnto_id,
             a.cost_adicn,
             a.cost_transfer
        From stg_activo_fijo.tbl_fact_estd_fina a
       Where a.peri_mes_id = pv_per_mens;

   Type BIAF_FACT_FINA Is Table Of C_BIAF_FACT_FINA%RowType;
    d BIAF_FACT_FINA;  
   
   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_dif_sol         Number := 0;
   ln_dif_index       Number := 0;
   ln_periodo         Number := 0;
   ld_fecha_act       Date;
   ln_existe          Number := 0;
   pn_cid             Number := 0;
   
   v_Fecha_sys        Varchar2(10):=null;
   v_Hora_sys         Varchar2(10):=null;   
Begin

    
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA FACT FINANCIERO');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  --eliminando los registros del periodo para repoblar la fact
	Delete From DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA Where peri_mes_id = pv_per_mens;
  
  Open C_BIAF_FACT_FINA;
  Loop 
    Fetch C_BIAF_FACT_FINA Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop
        
        nTotReg_DPO := nTotReg_DPO + 1;  
        Begin
        Insert Into DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA (
                cnta_id,  
                fam_activo_id,
                peri_mes_id,
                cia_id, 
                asnto_id,
                cost_adicn,
                cost_transfer,
                fech_ulti_actu 
          ) Values (
                d(y).cnta_id,  
                d(y).fam_activo_id,
                d(y).peri_mes_id,
                d(y).cia_id,   
                d(y).asnto_id,
                d(y).cost_adicn ,
                d(y).cost_transfer ,
                sysdate 
           );
          
           ln_registros_INS := ln_registros_INS + 1;
           pn_retorno:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA: '||sqlerrm||' - '||sqlcode||' CtaId: '||d(y).cnta_id);
            pn_retorno:=1;
        End;  
    End Loop;
    If pn_retorno = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_BIAF_FACT_FINA%NotFound;
  End Loop;
  Close C_BIAF_FACT_FINA;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales   : '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Insertados: '||ln_registros_INS);

End DMPR_FACT_FINA;


/*********************************************************************************************************
  NOMBRE:      DMPR_FACT_FINA_HIST
  PROPOSITO:   Procedimiento para poblar la FACT de Estados Financieros Historicos de los Activos Fijos de HTB y SMART
  AUTOR:       José Manuel Magán Vásquez C-116167
  PARAMETROS:  pv_peri_mes --> Periodo del mes
  RETORNO:     Retorna variable Number: 
               pn_retorno --> 0=Conforme / 1=Error
/*********************************************************************************************************/
 Procedure DMPR_FACT_FINA_HIST( pv_per_mens In DM_ACTIVO_FIJO.BIAF_FACT_REVAL.peri_mes_id%Type,
                                pn_retorno  In Out DM_ACTIVO_FIJO.BIAF_ACTI_MEJO.ACTI_MEJOR_ID%Type )
 Is 
 
  Cursor C_BIAF_FACT_FINA_HIST Is
      Select a.cnta_id,  
             a.fam_activo_id,
             a.peri_mes_id,
             a.cia_id, 
             a.asnto_id,
             a.cost_adicn,
             a.cost_transfer
        From DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA a
       Where a.peri_mes_id = pv_per_mens;   

   Type BIAF_FACT_FINA_HIST Is Table Of C_BIAF_FACT_FINA_HIST%RowType;
    d BIAF_FACT_FINA_HIST;  
   
   nTotReg_DPO        Number := 0;
   ln_registros_INS   Number := 0; 
   ln_dif_sol         Number := 0;
   ln_dif_index       Number := 0;
   ln_periodo         Number := 0;
   ld_fecha_act       Date;
   ln_existe          Number := 0;
   pn_cid             Number := 0;
   ln_cost_orig_peri  Number := 0;
   
   v_Fecha_sys        Varchar2(10):=null;
   v_Hora_sys         Varchar2(10):=null;   
Begin

   
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
  Select To_Char(New_Time(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys  From DUAL;
--
  dbms_output.enable(2000000);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line(' POBLAMIENTO TABLAS BI DE ACTIVO FIJO  ');
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('ACTUALIZA TABLON AF');
  dbms_output.put_line('FECHA         : '||v_Fecha_sys);
  dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
  dbms_output.put_line('PROGRAM       : ACTUALIZA FACT FINANZAS HIST');
  dbms_output.put_line('EJECUTADO POR : '||User);
  dbms_output.put_line('---------------------------------------');
  dbms_output.put_line('.');

  --eliminando los registros del periodo para repoblar la fact
	Delete From DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA_HIST Where peri_mes_id = pv_per_mens;
  
  Open C_BIAF_FACT_FINA_HIST;
  Loop 
    Fetch C_BIAF_FACT_FINA_HIST Bulk Collect Into d Limit 1000;
    For y In 1 .. d.COUNT Loop
        
        nTotReg_DPO := nTotReg_DPO + 1;   
         Begin
            Select cost_orig_peri 
              Into ln_cost_orig_peri
              From DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA_HIST x
             Where x.peri_mes_id   = d(y).peri_mes_id
               And x.cia_id        = d(y).cia_id
               And x.cnta_id       = d(y).cnta_id
               And x.fam_activo_id = d(y).fam_activo_id  
               And x.asnto_id      = d(y).asnto_id;
        Exception
          When Others Then                  
             ln_cost_orig_peri := 0; 
        End;              
        
        Begin
          Insert Into DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA_HIST (
                  cnta_id ,  
                  fam_activo_id ,
                  peri_mes_id ,
                  cia_id ,
                  asnto_id ,
                  cost_orig_peri  ,
                  fech_ulti_actu 
            ) Values (
                  d(y).cnta_id,  
                  d(y).fam_activo_id,
                  d(y).peri_mes_id,
                  d(y).cia_id,                    
                  d(y).asnto_id,
                  d(y).cost_transfer + ln_cost_orig_peri,
                  sysdate 
             );
          
           ln_registros_INS := ln_registros_INS + 1;
           pn_retorno:=0;
        Exception
          When Others then
            dbms_output.put_line('Error Insert DM_ACTIVO_FIJO.BIAF_FACT_ESTD_FINA_HIST: '||sqlerrm||' - '||sqlcode||' Activo: '||d(y).cnta_id);
            pn_retorno:=1;
        End;  
    End Loop;
    If pn_retorno = 0 Then
       Commit;
    Else
       Rollback;
    End If;
    Exit When C_BIAF_FACT_FINA_HIST%NotFound;
  End Loop;
  Close C_BIAF_FACT_FINA_HIST;
  --
  dbms_output.put_line('---------------------------------');
  dbms_output.put_line('        RESULTADO FINAL          ');
  dbms_output.put_line('---------------------------------');
  --
  Select TO_CHAR(NEW_TIME(SYSDATE, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
  dbms_output.put_line('HORA FIN : '||v_Hora_sys);
  dbms_output.put_line('TOT.REG. Totales   : '||nTotReg_DPO);
  dbms_output.put_line('TOT.REG. Insertados: '||ln_registros_INS);

End DMPR_FACT_FINA_HIST;        
 
	
	
	
	
	--########### Carga inicial		
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Inicial                    ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_INIC(
				pn_Erro      Out Number,
				pv_Mens      Out Varchar2)  Is
		
		
	v_Fecha_sys        	Varchar2(10);
	v_Hora_sys         	Varchar2(10);
	Pd_fecha_inicio     Date;
	Pd_fecha_fin        Date;
	--pd_fech_ini_temp    Date;
	--pd_fech_fin_temp    Date;
	ld_periodo        	Varchar2(10);
	contadorAnio        Number(6) := 0;
	contadorMes         Number(6) := 0;
	mes                 Varchar2(10);
	anio                Varchar2(10);
				
	Begin
		
	--inicializacion:
	Pn_Erro := Kn_Ejec_Ok;
	Pv_Mens := Kv_Ejec_Mens_Ok;

	--fechas de inicio y fin para la dimension tiempo:
	--pd_fech_ini_temp   := '01/01/1985';
	--pd_fech_fin_temp   := '30/12/2050';
	
	
	--fecha y hora ctual
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	
	--pintando el inicio
	dbms_output.enable(2000000);
	dbms_output.put_line('FECHA         : '||v_Fecha_sys);
	dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
	dbms_output.put_line('--------------------------------------');
	
	
    --#######Proceso de carga de los tablones y las dimenciones:
	
	--ejecutando los SP  para cargar los tablones: se truncan en cada carga
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CIA(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CTA_CTBL(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_TIP_CRTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_FAMI_ACTI(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_SUCU(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_PROV(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CTRO_CSTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_TIP_ACTI(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_TIP_ACTI(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_FIJO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_MEJO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_DEPR(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_REVA(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_DEPR_REVA(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_DEPR_CENT_CSTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_CNTRO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_ACTI_MEJO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_USU_APROC(Pn_Erro,Pv_Mens);

	--ejecutando los SP para cargar las dimenciones del DM: se actualizan valores anteriores y se insertan nuevos valores
	--DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIEMPO(pd_fech_ini_temp,pd_fech_fin_temp,Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CIA(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CTA_CTBL(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIPO_CTRTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_FAMI_ACTIVO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIPO_ACTIVO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_PROVEEDOR(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_SUCU(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CTRO_CSTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_ACTI_FIJO(Pn_Erro,Pv_Mens);
	
	--#### Proceso de carga de la FACT:
	
	--inicializacion de fechas:
	--activo mas antiguo:
	select 
		min(x.FECHA_ACTIVACION) 
		into Pd_fecha_inicio
	from 
	(select pp.FECHA_ACTIVACION from  admin.activo_mejora@EXACTUS pp
	union all
	select ww.FECHA_ACTIVACION from smart.activo_mejora@EXACTUS ww) x;
	
	--periodo anterior a la fecha actual:		
	SELECT TRUNC(SYSDATE, 'MM')-1 
	into Pd_fecha_fin
	FROM DUAL;

	dbms_output.put_line('fecha inicial :' || Pd_fecha_inicio);
	dbms_output.put_line('fecha final :'|| Pd_fecha_fin);
	
	--iterando sobre los años
	FOR contadorAnio IN  to_number(TO_CHAR(Pd_fecha_inicio,'YYYY')) .. to_number(TO_CHAR(Pd_fecha_fin,'YYYY'))
	LOOP 

		--iterando sobre los meses
		FOR contadorMes IN  to_number(extract(month from Pd_fecha_inicio)) .. to_number(extract(month from Pd_fecha_fin))
		LOOP
			
			if contadorMes < 10 then 
				mes := '0'||contadorMes;
			else
				mes := to_char(contadorMes);
			end if; 

			anio := to_char(contadorAnio);
			
			ld_periodo := anio || mes;
			
			DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_FACT_ACTI(ld_periodo,pn_Erro,pv_Mens);
			DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_FACT_ACTIVO(ld_periodo,pn_Erro,pv_Mens);
			DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_FACT_ACTIVO_HIST(ld_periodo,pn_Erro,pv_Mens);
		
		END LOOP;
	END LOOP;
	
	
	dbms_output.put_line('############## fin de la  carga inicial');
    --mostrando resultados:
	Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	dbms_output.put_line('HORA FIN : '||v_Hora_sys);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_INIC' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_INIC;
	
		
	/**
		->>- @param IN:
			-ld_periodo : none
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │ Carga Periodica de los tablones  ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_PERI_TBL(
				pn_Erro      Out Number,
				pv_Mens      Out Varchar2)  Is
		
	v_Fecha_sys        	Varchar2(10);
	v_Hora_sys         	Varchar2(10);
				
	Begin
		
	--inicializacion:
	Pn_Erro := Kn_Ejec_Ok;
	Pv_Mens := Kv_Ejec_Mens_Ok;
	
	
	--fecha y hora ctual
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	
	--pintando el inicio
	dbms_output.enable(2000000);
	dbms_output.put_line('FECHA         : '||v_Fecha_sys);
	dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
	dbms_output.put_line('--------------------------------------');
	
	
    --#######Proceso de carga de los tablones y las dimenciones:
	dbms_output.put_line('inicio de carga de tablones');
	
	
	--ejecutando los SP  para cargar los tablones: se truncan en cada carga
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CIA(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CTA_CTBL(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_TIP_CRTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_FAMI_ACTI(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_SUCU(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_PROV(Pn_Erro,Pv_Mens);  
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_CTRO_CSTO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_TIP_ACTI(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_TIP_ACTI(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_FIJO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_MEJO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_DEPR(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_REVA(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_HIST_DEPR_REVA(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_DEPR_CENT_CSTO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_ACTI_CNTRO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_ACTI_MEJO(Pn_Erro,Pv_Mens); 
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TRNS_USU_APROC(Pn_Erro,Pv_Mens); 
	
	
    --mostrando resultados:
	dbms_output.put_line('############## fin de la  carga Periodica de tablones');
	Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	dbms_output.put_line('HORA FIN : '||v_Hora_sys);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_PERI_TBL' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_PERI_TBL;
	
	
	/**
		->>- @param IN:
			-None
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │Carga Periodica de las dimenciones║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_PERI_DIM(
				pn_Erro      Out Number,
				pv_Mens      Out Varchar2)  Is
		
	v_Fecha_sys        	Varchar2(10);
	v_Hora_sys         	Varchar2(10);
				
	Begin
		
	--inicializacion:
	Pn_Erro := Kn_Ejec_Ok;
	Pv_Mens := Kv_Ejec_Mens_Ok;
	
	--fechas de inicio y fin para la dimension tiempo:
	--pd_fech_ini_temp   := '01/01/1985';
	--pd_fech_fin_temp   := '30/12/2050';
	
	--fecha y hora ctual
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	
	--pintando el inicio
	dbms_output.enable(2000000);
	dbms_output.put_line('FECHA         : '||v_Fecha_sys);
	dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
	dbms_output.put_line('--------------------------------------');
	
	
    --#######Proceso de carga de los tablones y las dimenciones:
	dbms_output.put_line('inicio de carga/actualizacion de dimenciones');
	

	--ejecutando los SP para cargar las dimenciones del DM: se actualizan valores anteriores y se insertan nuevos valores

	--DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIEMPO(pd_fech_ini_temp,pd_fech_fin_temp,Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CIA(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CTA_CTBL(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIPO_CTRTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_FAMI_ACTIVO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_TIPO_ACTIVO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_PROVEEDOR(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_SUCU(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_CTRO_CSTO(Pn_Erro,Pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_DIM_ACTI_FIJO(Pn_Erro,Pv_Mens);
	dbms_output.put_line('fin carga de dimenciones datamart');
	
	
    --mostrando resultados:
	dbms_output.put_line('############## fin de la  carga Periodica de dimenciones');
	Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	dbms_output.put_line('HORA FIN : '||v_Hora_sys);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_PERI_DIM' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_PERI_DIM;
	
	
	/**
		->>- @param IN:
			-ld_periodo :periodo a procesar
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │Carga Periodica de las tablas Fact║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
	*/
	PROCEDURE BIAF_CRGA_PERI_FACT(
				ld_periodo   Varchar2,
				pn_Erro      Out Number,
				pv_Mens      Out Varchar2)  Is
		
	v_Fecha_sys        	Varchar2(10);
	v_Hora_sys         	Varchar2(10);
				
	Begin
		
	--inicializacion:
	Pn_Erro := Kn_Ejec_Ok;
	Pv_Mens := Kv_Ejec_Mens_Ok;
	
	
	--fecha y hora ctual
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'dd-mm-yyyy') Into v_Fecha_sys From DUAL;
	Select TO_CHAR(NEW_TIME(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	
	--pintando el inicio
	dbms_output.enable(2000000);
	dbms_output.put_line('FECHA         : '||v_Fecha_sys);
	dbms_output.put_line('HORA INICIO   : '||v_Hora_sys);
	dbms_output.put_line('--------------------------------------');
	
	
    --#### Proceso de carga de la FACT:
	dbms_output.put_line('inicio de carga de la fact y fact historica');
		
	--se corre para un periodo en particular
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_TBLN_FACT_ACTI(ld_periodo,pn_Erro,pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_FACT_ACTIVO(ld_periodo,pn_Erro,pv_Mens);
	DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_FACT_ACTIVO_HIST(ld_periodo,pn_Erro,pv_Mens);
	
	
    --mostrando resultados:
	dbms_output.put_line('############## fin de la  carga Periodica de las FACT y FACT Historica');
	Select to_char(new_time(Sysdate, 'PDT','PST'),'hh24:mi:ss') Into v_Hora_sys From DUAL;
	dbms_output.put_line('HORA FIN : '||v_Hora_sys);
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_PERI_FACT' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_PERI_FACT;
	
	
		
	/**
		->>- @param IN:
			-ld_periodo :periodo a procesar
				
		->>- @return OUT:
			-pn_Erro : codigo de error
			-pv_Mens : mensaje de error
		╔═══════════════════════╦══════════════════════════════════════════════════════════════════════╗
		║ Version │ Fecha       │   Autor            │ N° Cambio    │            Descripcion           ║
		║─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────║
		║ 1.0.0   │ 10/03/2020  │ Wilber Huamani     │ C-116167     │   ETL de Carga Periodica         ║
		║         │             │                    │              │                                  ║
		║         │             │                    │              │                                  ║
		╚─────────┴─────────────┴────────────────────┴──────────────┴──────────────────────────────────╝
		
		NOTAS:
		
		Proceso ETL BI AF
		=================
		Esta tabla mantiene las solicitudes de procesamiento del BI de AF para un periodo en particular.
			
		Estructura de la tabla: DM_ACTIVO_FIJO.DMAF_SOLI_ETL
		 
				+-----------------------------+
				| DMAF_SOLI_ETL               │ 
				+=============================+
				| ide_proc       NUMBER       │ 
				| tip_proc       NUMBER       │  
				| per_proc       VARCHAR2(30) │
				| est_soli       NUMBER(2)    │  
				| nom_comp_crea  VARCHAR2(40) │  
				| fec_crea       DATE         |
				| cod_usua_crea  VARCHAR2(30) │   
				| nom_comp_modi  VARCHAR2(40) │   
				| cod_usua_modi  VARCHAR2(30) │   
				| fec_modi       DATE         │   
				+-----------------------------+

			* ide_proc: incremental
			* est_soli=0 solicitud Pendiente 
			* est_soli=1 solicitud Ejecutandose
			* est_soli=2 solicitud Ejecutado Con Error
			* est_soli=3 solicitud Culminada
			
			* tip_proc = 1 Carga de tablones
			* tip_proc = 2 Carga/Actualizacion de Dimenciones
			* tip_proc = 3 Carga de FACT y FACT-HIST
			
		Ejecucion del Script:  ejecutar en el sgte orden:
		=====================

		Procesar Carga de tablones:
		==========================
		1.  En la tabla: DM_ACTIVO_FIJO.DMAF_SOLI_ETL
			Ingresar el registro que se desea procesar con:
			tip_proc=1 y est_soli=0; 
				
				+=========+==========+============+==========+===============+============+===============+===============+===============+===========+
				|ide_proc │ tip_proc │ per_proc   │ est_soli | nom_comp_crea | fec_crea   | cod_usua_crea | nom_comp_modi | cod_usua_modi | fec_modi  |
				+=========+==========+============+==========+===============+============+===============+===============+===============+===========+
				|   9     │     1    │ 201902     │    0     |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				+---------+----------+------------+----------+---------------+------------+---------------+---------------+---------------+-----------+
			
		2 Ejecutar este script


		Procesar Actualizacion de Dimenciones: 
		=====================================
		3   Luego que termino de procesar el primer proceso, validar que se haya insertando en la tabla DM_ACTIVO_FIJO.DMAF_SOLI_ETL
			un nuevo el registro con:
			tip_proc=2 y est_soli=0; 
		  
				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|ide_proc │ tip_proc │ per_proc   │ est_soli  | nom_comp_crea | fec_crea   | cod_usua_crea | nom_comp_modi | cod_usua_modi | fec_modi  |
				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|   9     │     1    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				|   10    │     2    │ 201902     │    0      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				+---------+----------+------------+-----------+---------------+------------+---------------+---------------+---------------+-----------+

		4 Ejecutar este script


		Procesar Carga FACT y FACT-HIST
		===============================
			
		5   Luego que termino de procesar el segundo proceso, validar que se haya insertando en la tabla DM_ACTIVO_FIJO.DMAF_SOLI_ETL
			un nuevo registro con: 
			tip_proc=3 y est_soli=0;
		  
				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|ide_proc │ tip_proc │ per_proc   │ est_soli  | nom_comp_crea | fec_crea   | cod_usua_crea | nom_comp_modi | cod_usua_modi | fec_modi  |
				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|   9     │     1    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				|   10    │     2    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				|   11    │     3    │ 201902     │    0      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				+---------+----------+------------+-----------+---------------+------------+---------------+---------------+---------------+-----------+		

		6   Ejecutar este script


		Validando Resultados
		====================

		7   Validar que todos los registros para el perido procesado esten con est_soli = 3

				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|ide_proc │ tip_proc │ per_proc   │ est_soli  | nom_comp_crea | fec_crea   | cod_usua_crea | nom_comp_modi | cod_usua_modi | fec_modi  |
				+=========+==========+============+===========+===============+============+===============+===============+===============+===========+
				|   9     │     1    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				|   10    │     2    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				|   11    │     3    │ 201902     │    3      |   whuamani    | 01/06/2020 |    whuamani   |  whuamani     |    12230      | 31/05/2020|
				+---------+----------+------------+-----------+---------------+------------+---------------+---------------+---------------+-----------+	
			
	*/
	
	PROCEDURE BIAF_CRGA_PERI_ETL Is
	
	ln_cont          Number(6);
	ln_ide_proc      Number(6);
	ln_ide_proc_new  Number(6);
	ln_tip_proc      Number(6);
	salir            Exception;
	lv_mens_erro     Varchar2(2000);
	ld_periodo       Varchar2(20) := null;
	pn_Erro      	 Number(6);
	pv_Mens      	 Varchar2(500);
				
	Begin
	

		--inicializacion:
		Pn_Erro := Kn_Ejec_Ok;
		Pv_Mens := Kv_Ejec_Mens_Ok;
		
		
		--buscando solicitudes pendientes: est_soli = 0
		Select Count(1) 
		Into ln_cont 
		From DM_ACTIVO_FIJO.DMAF_SOLI_ETL Where est_soli = 0;
	
	
		--si hay solicitud pendiente:
		If ln_cont = 1 Then
			
			--capturando el id_proc y el tip_proc 
			Select ide_proc, tip_proc 
			Into ln_ide_proc, ln_tip_proc 
			From DM_ACTIVO_FIJO.DMAF_SOLI_ETL Where est_soli = 0;
			
			Begin
				
				--########### tip_proc = 1; PROCESO 1 : carga de tablones  #########---
				If ln_tip_proc = 1 Then
					
					--obteniendo nuevo id_proc
					Select Nvl(Max(ide_proc), 0) 
					Into ln_ide_proc_new 
					From DM_ACTIVO_FIJO.DMAF_SOLI_ETL;

					ln_ide_proc_new := ln_ide_proc_new + 1;

					
					dbms_output.put_line('Proceso 01 del ETL, carga de tablones Stage, fecha y hora inicio '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss')||Chr(10));
					dbms_output.put_line('');
					
					--Actualizamos estado de solicitud del proceso tip_proc = 1 a "ejecutandose" (est_soli = 1)
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Set est_soli = 1 
					Where ide_proc = ln_ide_proc 
					And est_soli = 0 
					And tip_proc = ln_tip_proc;

					--creamos uns copia del registro 1 (tip_proc=1) para la ejecucion del segundo proceso
					Insert Into DM_ACTIVO_FIJO.DMAF_SOLI_ETL
					(ide_proc, tip_proc, 
					per_proc, 
					est_soli,nom_comp_crea,
					fec_crea,cod_usua_crea,
					nom_comp_modi,cod_usua_modi,
					fec_modi)
					Select ln_ide_proc_new, 2, --<< proceso 2
					per_proc,
					0, --<< estado solicitud 
					SYS_CONTEXT('USERENV','TERMINAL'),
					sysdate,user,
					SYS_CONTEXT('USERENV','TERMINAL'),user,
					sysdate
					From DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Where ide_proc = ln_ide_proc 
					And est_soli = 1 
					And tip_proc = ln_tip_proc;
				
					
					Commit;
					
					
					--carga de tablones:
					Begin
					
						--DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_PERI_TBL(pn_Erro, pv_Mens);	
						dbms_output.put_line('##############  ejecutando carga tablones');
						dbms_output.put_line(Chr(10)||Chr(13));
					Exception
					When Others Then
						--haciendo rollback:
						Delete DM_ACTIVO_FIJO.DMAF_SOLI_ETL  Where ide_proc = ln_ide_proc_new And est_soli = 0 And tip_proc = 2;
						--actualizando el  estado de solicitud del registro 1 a "Error", est_soli = 2
						Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL  Set est_soli = 2 Where ide_proc = ln_ide_proc And est_soli = 1 And tip_proc = ln_tip_proc;
						Commit;
						lv_mens_erro := 'Problema al ejecutar el Proceso 01 del ETL. carga de tablones stage :'||pv_Mens;
					Raise salir;
					End;
			

					dbms_output.put_line('Hora de culminacion: '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss'));
					
					--Actualizamos el estado de solicitud a "finalizado" (est_soli = 3) del registro 1 (tip_proc=1)
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Set est_soli = 3 
					Where ide_proc = ln_ide_proc 
					And est_soli = 1 
					And tip_proc = ln_tip_proc;
					Commit;
				
				--############ tip_proc = 2; PROCESO 2 Actualizacion de dimenciones ###########---
				Elsif ln_tip_proc = 2 Then
					
					--obteniendo nuevo id_proc
					Select Nvl(Max(ide_proc), 0) 
					Into ln_ide_proc_new 
					From DM_ACTIVO_FIJO.DMAF_SOLI_ETL;

					ln_ide_proc_new := ln_ide_proc_new + 1;
					
					dbms_output.put_line('Proceso 02 del ETL, actualizacion de dimenciones, fecha y hora inicio '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss')||Chr(10));
					dbms_output.put_line('');
					
					--Actualizamos estado de solicitud del proceso tip_proc = 2 a "ejecutandose" (est_soli = 1)
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Set est_soli = 1 
					Where ide_proc = ln_ide_proc 
					And est_soli = 0 
					And tip_proc = ln_tip_proc;
					Commit;
					
					--creamos uns copia del registro 2 (tip_proc=2) para la ejecucion del tercer proceso
					Insert Into DM_ACTIVO_FIJO.DMAF_SOLI_ETL
					(ide_proc, tip_proc, 
					per_proc, 
					est_soli,nom_comp_crea,
					fec_crea,cod_usua_crea,
					nom_comp_modi,cod_usua_modi,
					fec_modi)
					Select ln_ide_proc_new, 3, --<< proceso 3
					per_proc,
					0, --<< estado de solicitud
					SYS_CONTEXT('USERENV','TERMINAL'),
					sysdate,user,
					SYS_CONTEXT('USERENV','TERMINAL'),user,
					sysdate
					From DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Where ide_proc = ln_ide_proc 
					And est_soli = 1 
					And tip_proc = ln_tip_proc;
					
					Commit;
					
					--Actualizacion de dimenciones
					Begin
						
						--DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_PERI_DIM(pn_Erro,pv_Mens);
						dbms_output.put_line('##############  ejecutando actualizacion de dimenciones');
						dbms_output.put_line(Chr(10)||Chr(13));
					Exception
					When Others Then
						
						--haciendo rollback:
						Delete DM_ACTIVO_FIJO.DMAF_SOLI_ETL  Where ide_proc = ln_ide_proc_new And est_soli = 0 And tip_proc = 3;
						--actualizando el  estado de solicitud del registro 2 a "Error", est_soli = 2
						Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL  Set est_soli = 2 Where ide_proc = ln_ide_proc And est_soli = 1 And tip_proc = ln_tip_proc;
						
						Commit;
						lv_mens_erro:='Problema al ejecutar el Proceso 02 del ETL, actualizando dimenciones :' ||pv_Mens;
						Raise salir;
					End;
					

					dbms_output.put_line('Hora de culminacion: '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss'));
					
					--Actualizamos el estado de solicitud a "finalizado" (est_soli = 3) del registro 2 (tip_proc=1)
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL Set est_soli = 3 
					Where ide_proc = ln_ide_proc 
					And est_soli = 1 
					And tip_proc = ln_tip_proc;
					Commit;
				
				--############ tip_proc = 3; PROCESO 3 carga de las fact y fact-hist ###########---
				Elsif ln_tip_proc = 3 Then
					
					dbms_output.put_line('Proceso 03 del ETL, carga de tablas FACT y FACT-HIST, fecha y hora inicio '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss')||Chr(10));
					dbms_output.put_line('');
					
					--Actualizamos estado de solicitud del proceso tip_proc = 3 a "ejecutandose" (est_soli = 1)
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Set est_soli = 1 
					Where ide_proc = ln_ide_proc 
					And est_soli = 0 
					And tip_proc = ln_tip_proc;
					Commit;
					
					Begin
						
						--DM_ACTIVO_FIJO.DMPG_BIAF_LOAD_DTMR.BIAF_CRGA_PERI_FACT(ld_periodo, pn_Erro,pv_Mens);
						dbms_output.put_line('##############  ejecutando carga fact y fact-hist');
						dbms_output.put_line(Chr(10)||Chr(13));
					Exception
					When Others Then
						Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL Set est_soli = 2 Where ide_proc = ln_ide_proc And est_soli = 1 	And tip_proc = ln_tip_proc;
						Commit;
						lv_mens_erro:='Problema al ejecutar el Proceso 03 del ETL, carga de fact y fact-hist' || pn_Erro;
						Raise salir;
					End;
					

					dbms_output.put_line('Hora de culminacion: '||To_Char(Sysdate, 'dd/mm/rrrr hh24:mi:ss'));
					
					--Actualizamos estado de proceso a "finalizado"
					Update DM_ACTIVO_FIJO.DMAF_SOLI_ETL 
					Set est_soli = 3 
					Where ide_proc = ln_ide_proc 
					And est_soli = 1 
					And tip_proc = ln_tip_proc;
					Commit;
				
				
				End If;
				
			Exception
			When Others Then
				lv_mens_erro:='Se encontraron errores durante el proceso ETL.' ||pv_Mens;
				Raise salir;
			End;
		Elsif ln_cont > 1 Then
			dbms_output.put_line('PROBLEMA: Se encontro mas de una solicitud de proceso ETL');
		Elsif ln_cont = 0 Then
			dbms_output.put_line('ADVERTENCIA: No se encontraron solicitudes del ETL para procesar');
		End If;
	
	
	Exception
	When No_Data_Found Then
		Null;
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'No hay Data';
	When Others Then
		Pn_Erro := Kn_Ejec_Erro;
		Pv_Mens := 'Procedimiento:'||'BIAF_CRGA_PERI_ETL' || '-' || Sqlerrm;
		DBMS_OUTPUT.PUT_LINE(Pv_Mens);
	End BIAF_CRGA_PERI_ETL;

End DMPG_BIAF_LOAD_DTMR;
/